﻿using Gateway.ProjectHorn.Systems.Entities.Components.Combat;
using Gateway.ProjectHorn.Systems.UI;
using Gateway.ProjectHorn.Util;
using TMPro;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

public class CombatUnitHUD : EntityUIElement
{
    private const string RankStarTemplateName = "<sprite name=rank_star>";

    [SerializeField] private TextMeshProUGUI _rankLabel;
    [SerializeField] private TextMeshProUGUI _numberLabel;
    [SerializeField] private Image _hitPointsFillImage;

    private uint m_hitPoints;
    private uint m_hitPointsCap;

    public void Init(uint currentHP, uint maxHP, uint unitNumber, uint unitRank)
    {
        SetCurrentHitPoints(currentHP);
        SetMaxHitPoints(maxHP);
        SetUnitNumber(unitNumber);
        SetRank(unitRank);
    }

    public void SetUnitNumber(uint number)
    {
        string numeral = number.ToRomanNumeral();

        _numberLabel.SetText(numeral);
    }

    public void SetRank(uint rank)
    {
        string spriteSequence = string.Empty;

        for (int i = 0; i < rank; i++)
        {
            spriteSequence += RankStarTemplateName;
        }

        _rankLabel.SetText(spriteSequence);
    }

    public void SetCurrentHitPoints(uint hitPoints, bool updateFillAmount = true)
    {
        m_hitPoints = hitPoints;

        if (updateFillAmount)
        {
            _hitPointsFillImage.fillAmount = m_hitPoints / (float)m_hitPointsCap;
        }
    }

    public void SetMaxHitPoints(uint maxHitPoints, bool updateFillAmount = true)
    {
        m_hitPointsCap = maxHitPoints;

        if (updateFillAmount)
        {
            SetCurrentHitPoints(m_hitPoints);
        }
    }

    public override float3 GetEntityWorldPosition(Entity entity)
    {
        return entityManager.GetComponentData<CombatUnitData>(entity).CurrentTileTarget
            .AddFloat2(new float2(.5f, .5f)).ToFloat3();
    }

    protected override void OnStart()
    {
    }

    protected override void OnUpdate()
    {
    }

    protected override void OnDispose()
    {
    }
}