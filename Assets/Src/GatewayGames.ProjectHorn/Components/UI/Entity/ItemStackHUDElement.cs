﻿using System;
using DG.Tweening;
using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Services.Serialization;
using Gateway.ProjectHorn.Services.Serialization.Definitions.Item;
using Gateway.ProjectHorn.Systems.DOTS.Item;
using Gateway.ProjectHorn.Systems.UI.Elements;
using Gateway.ProjectHorn.Util.DOTween;
using Unity.Entities;
using UnityEngine;
using UnityEngine.UI;

namespace Gateway.ProjectHorn.Components.UI.EntityHUD
{
    public class ItemStackHUDElement : DraggableEntityElement
    {
        [SerializeField] private Image s_fillImage;
        [SerializeField] private RawImage s_itemImage;
        [SerializeField] private TweenSettings s_fillTweenSettings;

        [NonSerialized] private ItemDef m_currentItem;

        public ItemStackListHUD Container;
        public Entity ItemStackEntity;

        private Transform m_cachedContainer;
        private int m_itemCount;
        private int m_itemCountLimit;

        public void SetItem(uint itemUID, int stackLimit, int itemCount)
        {
            var itemDef = ServiceInjector.GetService<DefinitionService>().GetDefinitionByUID<ItemDef>(itemUID);
            SetItem(itemDef, stackLimit, itemCount);
        }

        public void SetItem(ItemDef item, int stackLimit, int itemCount)
        {
            m_currentItem = item;

            if (item == null)
            {
                Hide();
            }
            else if (item.UID == m_currentItem.UID)
            {
                s_itemImage.texture = item.Graphics.Texture;
                SetItemCount(itemCount, stackLimit);
            }
        }

        public void SetItemCount(int itemCount, int stackLimit)
        {
            m_itemCount = itemCount;
            m_itemCountLimit = stackLimit;

            s_fillImage.DOFillAmount(itemCount / (float)stackLimit, s_fillTweenSettings.Duration)
                .SetEase(s_fillTweenSettings.Ease);

            if (itemCount > 0)
            {
                Show();
            }
            else
            {
                Hide();
            }
        }

        public void ChangeItemCountBy(int change)
        {
            SetItemCount(m_itemCount + change, m_itemCountLimit);
        }

        protected override void OnStart()
        {
            s_fillImage.fillAmount = 0;
            DragBeginEvent += OnDragStart;
            DragEndEvent += OnDragEng;
        }

        protected override void OnUpdate()
        {
        }

        protected override void OnDispose()
        {
            DragBeginEvent -= OnDragStart;
            DragEndEvent -= OnDragEng;
        }

        private void OnDragStart()
        {
            m_cachedContainer = transform.parent;
        }

        private void OnDragEng(GameObject[] targets)
        {
            for (int i = 0; i < targets.Length; i++)
            {
                var container = targets[i].GetComponent<ItemStackListHUD>();
                if (container != null && container != Container)
                {
                    Container.RemoveElement(this);
                    Container = container;
                    LocalItemSystem.SetItemStackOwnerDistrict(ItemStackEntity, container.District);
                    Container.AddElement(this);
                    return;
                }
            }

            transform.SetParent(m_cachedContainer);
            Container.RebuildLayout();
        }
    }
}