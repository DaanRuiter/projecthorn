using System.Collections.Generic;
using Gateway.ProjectHorn.Engine;
using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Systems.Terrain;
using Gateway.ProjectHorn.Systems.UI;
using Gateway.ProjectHorn.Systems.UI.Elements;
using Unity.Entities;

/**
 * 09/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Components.UI.EntityHUD
{
    public class EntityHUDContainer : SimpleUIElement
    {
        [InjectService] protected TerrainSystem i_terrainSystem;
        [InjectService] protected UISystem i_uiSystem;

        private Dictionary<Entity, IEntityUIElement> m_entityHUDElements = new Dictionary<Entity, IEntityUIElement>();

        public void SetEntityHUD(Entity entity, IEntityUIElement HUD)
        {
            if (!m_entityHUDElements.ContainsKey(entity))
            {
                m_entityHUDElements.Add(entity, HUD);
            }
        }

        public bool ContainsEntity(Entity entity) => m_entityHUDElements.ContainsKey(entity);

        public bool ContainsOfTypeEntity<T>(Entity entity) where T : IEntityUIElement =>
            ContainsEntity(entity) && m_entityHUDElements[entity].GetType() == typeof(T);

        public T GetEntityHUD<T>(Entity entity) where T : IEntityUIElement
        {
            if (m_entityHUDElements.ContainsKey(entity))
            {
                return (T)m_entityHUDElements[entity];
            }

            return default;
        }

        protected override void OnStart()
        {
        }

        protected override void OnUpdate()
        {
            foreach (var hud in m_entityHUDElements)
            {
                var entityScreenPos = Main.Camera.WorldToScreenPoint(hud.Value.GetEntityWorldPosition(hud.Key));
                hud.Value.Transform.position = entityScreenPos;
            }
        }

        protected override void OnDispose()
        {
        }

        public ItemStackHUDElement CreateItemStackHUDElement(Entity itemStackEntity)
        {
            var prefab = PrefabCollection.StandardGameAssets.GetPrefab("ItemStackHUD");
            var display = Instantiate(prefab, transform).GetComponent<ItemStackHUDElement>();
            display.ItemStackEntity = itemStackEntity;
            display.transform.SetSiblingIndex(0);
            return display;
        }
    }
}