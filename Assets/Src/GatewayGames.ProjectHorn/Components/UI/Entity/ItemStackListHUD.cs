

/**
 * 13/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

using System;
using System.Collections.Generic;
using Gateway.ProjectHorn.Engine;
using Gateway.ProjectHorn.Systems.Building;
using Gateway.ProjectHorn.Systems.UI;
using Gateway.ProjectHorn.Systems.UI.Elements;
using Unity.Entities;
using UnityEngine;
using UnityEngine.UI;

namespace Gateway.ProjectHorn.Components.UI.EntityHUD
{
    public class ItemStackListHUD : EntityUIElement, IListUIElement<ItemStackHUDElement>
    {
        public event Action<ItemStackHUDElement> ElementAddedEvent;

        public event Action<ItemStackHUDElement, int> ElementInsertedEvent;

        public event Action<ItemStackHUDElement> ElementRemovedEvent;

        public District District;

        [SerializeField] private GridLayoutGroup s_contentRoot;

        private List<ItemStackHUDElement> m_elements;

        protected override void OnStart()
        {
            m_elements = new List<ItemStackHUDElement>();
        }

        protected override void OnUpdate()
        {
        }

        protected override void OnDispose()
        {
        }

        public static ItemStackListHUD CreateItemStackListHUD(Transform parent)
        {
            var prefab = PrefabCollection.StandardGameAssets.GetPrefab("ItemStackContainer");
            var container = Instantiate(prefab, parent).GetComponent<ItemStackListHUD>();
            return container;
        }

        public void AddElement(ItemStackHUDElement element)
        {
            m_elements.Add(element);
            ElementAddedEvent?.Invoke(element);
            OnElementAdded(element);
        }

        public void InsertElement(ItemStackHUDElement element, int index)
        {
            m_elements.Insert(index, element);
            ElementInsertedEvent?.Invoke(element, index);
            OnElementAdded(element);
        }

        public bool ContainsElement(ItemStackHUDElement element) => m_elements.Contains(element);

        public void RebuildLayout()
        {
            s_contentRoot.enabled = false;
            s_contentRoot.enabled = true;
        }

        public ItemStackHUDElement GetElement(int index)
        {
            return m_elements[index];
        }

        public ItemStackHUDElement GetElement(Entity entity)
        {
            for (int i = 0; i < m_elements.Count; i++)
            {
                if (entity == m_elements[i].ItemStackEntity)
                {
                    return m_elements[i];
                }
            }

            return null;
        }

        public void RemoveElement(ItemStackHUDElement element)
        {
            m_elements.Remove(element);
            element.transform.SetParent(null);
            ElementRemovedEvent?.Invoke(element);
        }

        public void RemoveElement(int index) => RemoveElement(m_elements[index]);

        private void OnElementAdded(ItemStackHUDElement element)
        {
            element.transform.SetParent(s_contentRoot.transform);
            element.Container = this;
        }
    }
}