

using TMPro;
using UnityEngine;
using UnityEngine.UI;

/**
 * 25/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Src.GatewayGames.ProjectHorn.Components.UI.Debug
{
    public class DefintionEntryView : MonoBehaviour
    {
        public TextMeshProUGUI s_nameLabel;
        public Image s_sprite;
        public RawImage s_texture;
    }
}