﻿using Gateway.ProjectHorn.Services.Serialization.Definitions;
using UnityEngine;

namespace Gateway.Components.UI.Debug
{
    public class DefinitionEntry : MonoBehaviour
    {
        public BaseDef Entry
        {
            get { return m_entry; }
            set
            {
                m_entry = value;
                OnEntryChanged();
            }
        }

        private BaseDef m_entry;

        private void OnEntryChanged()
        {
        }
    }
}