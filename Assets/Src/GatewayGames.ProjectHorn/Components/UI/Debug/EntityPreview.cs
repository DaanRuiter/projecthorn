﻿using System.Collections.Generic;
using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Services.Graphics;
using Gateway.ProjectHorn.Services.Serialization.Definitions;
using Gateway.ProjectHorn.Services.Serialization.Definitions.Building;
using Gateway.ProjectHorn.Systems.Terrain;
using Gateway.ProjectHorn.Systems.UI;
using Gateway.ProjectHorn.Util;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/**
 * 25/01/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Components.UI.Debug
{
    public class EntityPreview : SimpleUIElement
    {
        [SerializeField] private DefinitionListUI s_list;
        [SerializeField] private TextMeshProUGUI s_propertiesLabel;
        [SerializeField] private Image s_previewImageSprite;
        [SerializeField] private RawImage s_previewImageTexture;
        [SerializeField] private MeshFilter s_tileRangePreviewMeshFilter;

        [InjectService] private TerrainSystem _terrainSystem;
        [InjectService] private GraphicsService _graphicsService;

        private Sprite m_defaultSprite;
        private bool m_entitySelected;
        private Dictionary<string, string> m_properties;

        protected override void OnStart()
        {
            m_defaultSprite = s_previewImageSprite.sprite;
            m_properties = new Dictionary<string, string>();

            s_list.SelectedDefinitionChangedEvent += UpdateSelectedDefinition;

            UpdateSelectedDefinition(null);
        }

        protected override void OnUpdate()
        {
            ToggleGameObject(s_tileRangePreviewMeshFilter.gameObject, !IsCursorOnUI);

            if (m_entitySelected && s_tileRangePreviewMeshFilter.gameObject.activeInHierarchy)
            {
                var tileUnderCursor = _terrainSystem.GetTileUnderCursor();

                if (tileUnderCursor != null)
                {
                    s_tileRangePreviewMeshFilter.transform.position = tileUnderCursor.GetPosition().ToVector3();
                }
            }
        }

        protected override void OnDispose()
        {
            s_list.SelectedDefinitionChangedEvent -= UpdateSelectedDefinition;
        }

        private void UpdateSelectedDefinition(BaseDef definition)
        {
            SetImage(m_defaultSprite);
            SetTexture(null);
            m_properties.Clear();
            Destroy(s_tileRangePreviewMeshFilter.mesh);

            if (definition != null)
            {
                definition.InspectWithPreview(this);

                if (definition is BuildingDef)
                {
                    var selectedBuildingDef = (BuildingDef)definition;
                    var mesh = s_tileRangePreviewMeshFilter.mesh;
                    if (mesh != null)
                    {
                        Destroy(mesh);
                    }

                    s_tileRangePreviewMeshFilter.mesh =
                        _graphicsService.CreateTileRangeMesh(selectedBuildingDef.SpawnerDef);
                }
            }

            s_tileRangePreviewMeshFilter.gameObject
                .SetActive(definition != null); //todo: move range preview logic to own script

            s_propertiesLabel.text = GetPropertiesText();
            m_entitySelected = definition != null;
        }

        public void SetImage(Sprite sprite = null)
        {
            s_previewImageSprite.sprite = sprite;
            s_previewImageSprite.enabled = sprite != null;
        }

        public void SetTexture(Texture texture)
        {
            s_previewImageTexture.texture = texture;
            s_previewImageTexture.enabled = texture != null;
        }

        public void AddProperty(string propertyName, object value)
        {
            if (!m_properties.ContainsKey(propertyName))
            {
                m_properties.Add(propertyName, value.ToString());
            }
        }

        public string GetPropertiesText()
        {
            string text = string.Empty;

            foreach (var property in m_properties)
            {
                string key = property.Key;
                string value = property.Value;
                text += string.Format("{0} <b>{1}</b>\n", key == string.Empty ? key : key + ":", value);
            }

            return text;
        }
    }
}