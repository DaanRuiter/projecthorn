﻿using System;
using System.Collections.Generic;
using Gateway.ProjectHorn.Services.Serialization.Definitions.District;
using Gateway.ProjectHorn.Services.Serialization.Definitions.DistrictNode;
using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Services.Serialization;
using Gateway.ProjectHorn.Services.Serialization.Definitions;
using Gateway.ProjectHorn.Services.Serialization.Definitions.Building;
using Gateway.ProjectHorn.Systems.DOTS.Combat;
using Gateway.ProjectHorn.Systems.Entities;
using Gateway.ProjectHorn.Systems.Terrain;
using Gateway.ProjectHorn.Systems.Terrain.Tile;
using Gateway.ProjectHorn.Systems.UI;
using Gateway.ProjectHorn.Util;
using Src.GatewayGames.ProjectHorn.Components.UI.Debug;
using UnityEngine;
using UnityEngine.UI;

namespace Gateway.ProjectHorn.Components.UI.Debug
{
    public class DefinitionListUI : SimpleUIElement
    {
        public event Action<BaseDef> SelectedDefinitionChangedEvent;

        public GameObject DefinitionEntryPrefab;
        public Transform ListRoot;

        private BaseDef m_selectedDefinition;

        [InjectService] private DefinitionService _definitions;
        [InjectService] private TerrainSystem _terrainSystem;
        [InjectService] private EntitySystem _entitySystem;
        [InjectService] private UISystem i_uiSystem;

        private List<Button> m_entryButtons = new List<Button>();

        protected void UpdateItems()
        {
            DisposeItems();

            var definitions = _definitions.GetAllDefinitions();

            foreach (KeyValuePair<Type, List<BaseDef>> definitionList in definitions)
            {
                string type = definitionList.Key.Name.Substring(0, definitionList.Key.Name.Length - 3); // strip Def

                foreach (BaseDef def in definitionList.Value)
                {
                    if (def.HasFlag("placeable"))
                    {
                        var entry = Instantiate(DefinitionEntryPrefab, ListRoot).GetComponent<DefintionEntryView>();
                        entry.s_nameLabel.SetText(string.Format("<b>[{0}]</b> {1}", type.Substring(0, 1), def.Name));
                        if (def is TileDef)
                        {
                            entry.s_texture.gameObject.SetActive(false);
                            entry.s_sprite.gameObject.SetActive(true);
                            entry.s_sprite.sprite = ((TileDef)def).Graphics.Sprite;
                        }

                        if (def is EntityDef)
                        {
                            entry.s_sprite.gameObject.SetActive(false);
                            entry.s_texture.gameObject.SetActive(true);
                            entry.s_texture.texture = ((EntityDef)def).Graphics.Texture;
                        }

                        var button = entry.GetComponent<Button>();
                        button.onClick.AddListener(() => SelectDefinition(def));
                        m_entryButtons.Add(button);
                    }
                }
            }
        }

        protected override void OnStart()
        {
            UpdateItems();
        }

        protected override void OnUpdate()
        {
            if (Input.GetMouseButton(0) && m_selectedDefinition != null)
            {
                TerrainTile tile = _terrainSystem.GetTileUnderCursor();

                if (tile != null)
                {
                    PlaceSelectedDefinition(tile.GetPosition());
                }
            }
            else if (Input.GetMouseButtonUp(1))
            {
                SelectDefinition(null);
                UpdateItems();
            }

            if (Input.GetKeyUp(KeyCode.Space))
            {
                var pos = _terrainSystem.GetTileUnderCursor().GetPosition().ToInt2();
                LocalCombatSystem.SetMoveCommand(LocalCombatSystem.TestCombatUnit, pos);
            }
        }

        protected override void OnDispose()
        {
            DisposeItems();
        }

        protected void DisposeItems()
        {
            for (int i = 0; i < m_entryButtons.Count; i++)
            {
                Destroy(m_entryButtons[i].gameObject);
            }

            m_entryButtons.Clear();
        }

        private void SelectDefinition(BaseDef definition)
        {
            m_selectedDefinition = definition;

            i_uiSystem.IsPlacingEntity = definition != null;

            SelectedDefinitionChangedEvent?.Invoke(definition);
        }

        private void PlaceSelectedDefinition(Vector2Int tilePosition)
        {
            if (!_terrainSystem.Tiles.IsInRange(tilePosition))
            {
                return;
            }

            if (Main.PlayerRealm.Tier.HasReachedLimit(m_selectedDefinition))
            {
                return;
            }

            var type = m_selectedDefinition.GetType();

            if (type == typeof(BuildingDef) || type.IsSubclassOf(typeof(BuildingDef)))
            {
                _entitySystem.PlaceBuildingInDistrict((BuildingDef)m_selectedDefinition,
                    tilePosition,
                    _terrainSystem.GetNodePosUnderCursor());
            }
            else if (type == typeof(DistrictNodeDef) || type.IsSubclassOf(typeof(DistrictNodeDef)))
            {
                _entitySystem.PlaceNodeInDistrict((DistrictNodeDef)m_selectedDefinition,
                    tilePosition,
                    _terrainSystem.GetNodePosUnderCursor());
            }
            else if (type == typeof(TileDef) || type.IsSubclassOf(typeof(TileDef)))
            {
                _terrainSystem.Tiles.SetTile(tilePosition, (TileDef)m_selectedDefinition);
            }
            else if (type == typeof(DistrictDef) || type.IsSubclassOf(typeof(DistrictDef)))
            {
                _entitySystem.BuildDistrict((DistrictDef)m_selectedDefinition, tilePosition);
            }
        }
    }
}