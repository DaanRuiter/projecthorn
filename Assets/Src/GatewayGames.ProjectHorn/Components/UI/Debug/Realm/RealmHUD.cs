using Gateway.ProjectHorn.Systems.Realm;
using Gateway.ProjectHorn.Systems.UI;
using TMPro;
using UnityEngine;

/**
 * 22/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Src.GatewayGames.ProjectHorn.Components.UI.Debug.Realms
{
    public class RealmHUD : SimpleUIElement
    {
        [SerializeField] private TextMeshProUGUI s_tierText;
        [SerializeField] private TextMeshProUGUI s_houseLimitText;
        [SerializeField] private TextMeshProUGUI s_districtLimitText;

        private string m_tierTemplate;
        private string m_houseTemplate;
        private string m_districtTemplate;

        private float m_updateInterval = .1f;
        private float m_nextUpdateTimestamp;

        private Realm m_realm;

        public void SetRealm(Realm realm)
        {
            m_realm = realm;
            UpdateHUD();
        }

        protected override void OnStart()
        {
            m_tierTemplate = s_tierText.text;
            m_houseTemplate = s_houseLimitText.text;
            m_districtTemplate = s_districtLimitText.text;
        }

        protected override void OnUpdate()
        {
            float realTime = Time.realtimeSinceStartup;

            if (realTime >= m_nextUpdateTimestamp)
            {
                m_nextUpdateTimestamp = realTime + m_updateInterval;
                UpdateHUD();
            }

            if (Input.GetKeyUp(KeyCode.T))
            {
                m_realm.UpgradeTier();
            }
        }

        protected override void OnDispose()
        {
        }

        private void UpdateHUD()
        {
            s_tierText.SetText(string.Format(m_tierTemplate, m_realm.Tier.TierIndex));
            s_houseLimitText.SetText(string.Format(m_houseTemplate,
                m_realm.Tier.HouseLimit.Current,
                m_realm.Tier.HouseLimit.Limit));
            s_districtLimitText.SetText(string.Format(m_districtTemplate,
                m_realm.Tier.DistrictLimit.Current,
                m_realm.Tier.DistrictLimit.Limit));
        }
    }
}