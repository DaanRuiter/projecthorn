

using Gateway.ProjectHorn.Components.UI.EntityHUD;
using Gateway.ProjectHorn.Systems.Building;
using UnityEngine;

/**
 * 05/06/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Components.UI.Item
{
    public class DistrictItemHUDContainer : EntityHUDContainer
    {
        private District m_currentOpenDistrict;

        protected override void OnUpdate()
        {
            base.OnUpdate();

            if (Input.GetMouseButtonDown(0) && !i_uiSystem.IsPlacingEntity)
            {
                ToggleDistrictInventory();
            }
        }

        private void ToggleDistrictInventory()
        {
            var tile = i_terrainSystem.GetTileUnderCursor();

            if (tile != null)
            {
                if (m_currentOpenDistrict != tile.District)
                {
                    if (m_currentOpenDistrict != null)
                    {
                        GetEntityHUD<ItemStackListHUD>(m_currentOpenDistrict.Entity).Hide();
                        m_currentOpenDistrict = null;
                    }

                    if (tile.District != null)
                    {
                        m_currentOpenDistrict = tile.District;
                        GetEntityHUD<ItemStackListHUD>(m_currentOpenDistrict.Entity).Show();
                    }
                }
            }
        }
    }
}