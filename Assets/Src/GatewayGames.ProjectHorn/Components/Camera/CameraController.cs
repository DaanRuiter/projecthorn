﻿using Gateway.ProjectHorn.Util;

namespace Gateway.ProjectHorn.Components.CameraController
{
    public abstract class CameraController : SharedMonoBehaviour
    {
        public static CameraController ActiveController { get; private set; }

        public static UnityEngine.Camera Camera => UnityEngine.Camera.main;

        private static void SetActiveController(CameraController controller)
        {
            if (controller == ActiveController)
            {
                return;
            }

            ActiveController?.OnControllerDisable();
            ActiveController = controller;
            ActiveController.OnControllerEnable();
        }

        protected abstract void OnControllerEnable();

        protected abstract void OnControllerDisable();

        public void SetAsActiveController()
        {
            SetActiveController(this);
        }
    }
}