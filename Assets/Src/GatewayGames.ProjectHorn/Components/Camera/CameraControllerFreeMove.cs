﻿using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Systems.Terrain;
using UnityEngine;

namespace Gateway.ProjectHorn.Components.CameraController
{
    [RequireComponent(typeof(Camera))]
    public class CameraControllerFreeMove : CameraController
    {
        public float m_movementSpeed = 500;
        public float m_zoomSpeed = 1500;

        private Camera m_camera;

        [InjectService] private TerrainSystem m_terrainSystem;

        private void Start()
        {
            m_camera = GetComponent<Camera>();

            SetAsActiveController();
        }

        private void Update()
        {
            // Fetch input
            var input = new Vector3(Input.GetAxis("Horizontal"),
                Input.GetAxis("Vertical"));

            // Scale input with speed
            var translation = input;
            translation.x *= (m_movementSpeed * m_camera.orthographicSize);
            translation.y *= (m_movementSpeed * m_camera.orthographicSize);
            translation *= Time.deltaTime;

            // Clamp
            var targetPos = transform.position + translation;
            targetPos.x = Mathf.Clamp(targetPos.x, 0, m_terrainSystem.Tiles.Dimensions.x);
            targetPos.y = Mathf.Clamp(targetPos.y, 0, m_terrainSystem.Tiles.Dimensions.y);

            // Apply
            transform.position = targetPos;

            float zoomInput = -Input.GetAxis("Mouse ScrollWheel") * m_zoomSpeed;
            float zoom = Mathf.Clamp(m_camera.orthographicSize + zoomInput, 1, m_terrainSystem.Tiles.Dimensions.x);
            m_camera.orthographicSize = zoom;
        }

        protected override void OnControllerDisable()
        {
            enabled = false;
        }

        protected override void OnControllerEnable()
        {
            enabled = true;
        }
    }
}