﻿using UnityEngine;

namespace Gateway.ProjectHorn.Components.CameraController
{
    public class CameraControllerFollow : CameraController
    {
        [SerializeField] private Transform s_target;
        [SerializeField] private Vector3 s_followOffset = new Vector3(0, 0, -7.5f);
        [SerializeField] private float s_followSpeed = 5f;
        [SerializeField] private float s_followDamping = .15f;

        protected override void Awake()
        {
            SetAsActiveController();
        }

        private void Update()
        {
            if (!s_target)
            {
                return;
            }

            transform.position = Vector3.Lerp(transform.position,
                s_target.position + s_followOffset,
                s_followSpeed * Time.deltaTime * s_followDamping);
        }

        protected override void OnControllerEnable()
        {
            enabled = true;
        }

        protected override void OnControllerDisable()
        {
            enabled = false;
        }
    }
}