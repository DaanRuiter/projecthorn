using System;
using System.Collections.Generic;
using System.IO;
using Gateway.ProjectHorn.Services.Serialization;
using Gateway.ProjectHorn.Services.Serialization.Definitions;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using UnityEditor;
using UnityEngine;

namespace Gateway.ProjectHorn.Editor.JSON
{
    public class JsonEditorWindow : OdinMenuEditorWindow
    {
        private static string rootFolder => SerializationService.dataFolder;

        [MenuItem("Gateway/JSON Editor Window")]
        private static void ShowWindow()
        {
            var window = GetWindow<JsonEditorWindow>();
            window.titleContent = new GUIContent("JSON Editor");
            window.Show();
        }

        private bool m_initialized;
        private string[] m_folders;

        // private void Init()
        // {
        //     m_initialized = true;
        //
        //     m_folders = Directory.GetDirectories(rootFolder);
        // }

        [Serializable]
        class DefinitionList : SerializedScriptableObject
        {
            public string TypeLabel;

            [ListDrawerSettings(ListElementLabelName = "DefinitionName")]
            public List<Entry> Defs;

            public DefinitionList Init(List<BaseDef> defs)
            {
                Defs = new List<Entry>();
                defs.ForEach(def =>
                {
                    Defs.Add(new Entry()
                    {
                        Definition = def
                    });
                });
                return this;
            }
        }

        public class Entry
        {
            public string DefinitionName => Definition.Name;

            public BaseDef Definition;

            [Button]
            public void Save()
            {
                string json = JsonUtility.ToJson(Definition, true);
                StreamWriter writer = new StreamWriter(Definition.DataPath);
                writer.WriteAsync(json);
                writer.Close();
            }
        }

        protected override OdinMenuTree BuildMenuTree()
        {
            m_folders = Directory.GetDirectories(rootFolder);

            var serialization = new SerializationService(false);
            var definitions = new DefinitionService(false);
            serialization.LoadAllEntityDefinitions(definitions);
            var defs = definitions.GetAllDefinitions();

            var tree = new OdinMenuTree();
            tree.Add("Game data", null);
            tree.Selection.SupportsMultiSelect = false;

            // m_folders.ForEach(f =>
            // {
            //     string[] split = f.Split('/');
            //     string folder = split[split.Length - 1];
            //
            //     tree.Add(folder, "");
            // });

            // tree.AddAllAssetsAtPath("Some Menu Item", rootFolder, typeof(BaseDef), true)
            //     .AddThumbnailIcons();

            defs.ForEach(defList =>
            {
                var list = CreateInstance<DefinitionList>().Init(defList.Value);
                list.TypeLabel = defList.Key.Name.Substring(0, defList.Key.Name.Length - 3);
                tree.Add(defList.Key.Name, list);
            });

            // tree.Add("Settings", GeneralDrawerConfig.Instance);
            // tree.AddAllAssetsAtPath("Odin Settings", "Assets/Plugins/Sirenix", typeof(ScriptableObject), true, true);
            return tree;
        }

        // private void OnGUI()
        // {
        //     if (!m_initialized)
        //     {
        //         Init();
        //     }
        //
        //     Odin
        // }
    }
}