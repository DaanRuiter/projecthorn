﻿using Gateway.ProjectHorn.Engine;
using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Systems.Actor;
using Gateway.ProjectHorn.Systems.Actor.TaskMaster;
using Gateway.ProjectHorn.Systems.DOTS;
using Gateway.ProjectHorn.Systems.DOTS.Common;
using Gateway.ProjectHorn.Systems.Entities;
using Gateway.ProjectHorn.Systems.Realm;
using Gateway.ProjectHorn.Systems.Terrain;
using Gateway.ProjectHorn.Systems.UI;
using Gateway.ProjectHorn.Util;
using Src.GatewayGames.ProjectHorn.Components.UI.Debug.Realms;
using TMPro;
using Unity.Entities;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Gateway.ProjectHorn
{
    public class Main : SharedMonoBehaviour
    {
        public static float TimeScale { get; private set; }

        public static bool IsAwake { get; private set; }

        public static bool Ready { get; private set; }

        public static Camera Camera { get; private set; }

        public static EventSystem EventSystem { get; private set; }

        public static Realm PlayerRealm => m_playerRealm;

        public static EntityManager EntityManager => World.DefaultGameObjectInjectionWorld.EntityManager;
        
        private static Realm m_playerRealm;

        [SerializeField] private PrefabCollection s_standardGameAssets;
        [SerializeField] private EventSystem s_eventSystem;

        // Temp
        [SerializeField] private TextMeshProUGUI s_speedText;

        protected override void Awake()
        {
            TimeScale = 1f;
            IsAwake = true;
            Camera = Camera.main;
            EventSystem = s_eventSystem;

            base.Awake();

            CreateMainSystems();

            if (Camera)
            {
                var terrain = ServiceInjector.GetService<TerrainSystem>().Definition;
                var cameraPosition = Camera.transform.position;
                cameraPosition.x = terrain.WorldSize.x / 2f;
                cameraPosition.y = terrain.WorldSize.y / 2f;
                Camera.transform.position = cameraPosition;
            }

            Ready = true;

            CreateMainTaskMasters();

            s_speedText.SetText(GetGameSpeedLabel());
        }

        private void Start() // Game start, systems ready
        {
            m_playerRealm = new Realm(FactionIndex.PlayerFactionUID);
            ServiceInjector.GetService<UISystem>().GetElement<RealmHUD>().SetRealm(m_playerRealm);

            var entitySystem = ServiceInjector.GetService<EntitySystem>();
            entitySystem.DistrictPlacedEvent += (district) => m_playerRealm.Tier.DistrictLimit.Increment();
            entitySystem.NodePlacedEvent += (node, district) =>
            {
                if (node.Name == "House")
                {
                    m_playerRealm.Tier.HouseLimit.Increment();
                }
            };
        }

        private void CreateMainSystems()
        {
            s_standardGameAssets.Load();

            ServiceInjector.LoadService<TerrainSystem>();
            ServiceInjector.LoadService<EntitySystem>();
        }

        private void CreateMainTaskMasters()
        {
            var taskSystem = GetOrCreateSystem<ActorTaskSystem>();
            taskSystem.AddTaskMaster(new ActorMoveTaskMaster());
            taskSystem.AddTaskMaster(new ActorConstructionTaskMaster());
        }

        private void Update()
        {
            ServiceInjector.UpdateServices();

            if (Input.GetKeyUp(KeyCode.Period))
            {
                TimeScale++;

                s_speedText.SetText(GetGameSpeedLabel());
            }

            if (Input.GetKeyUp(KeyCode.Comma))
            {
                TimeScale--;

                s_speedText.SetText(GetGameSpeedLabel());
            }
        }

        private void OnDestroy()
        {
            ServiceInjector.DisposeServices();
        }

        private string GetGameSpeedLabel() => $"Speed: {TimeScale}x";

        //Helpers
        public static T GetOrCreateSystem<T>() where T : BaseDOTSSystem
        {
            var system = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<T>();

            system.InternalOnCreate();

            return system;
        }
    }
}