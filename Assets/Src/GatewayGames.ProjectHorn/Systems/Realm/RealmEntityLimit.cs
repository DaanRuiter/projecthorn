

using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Services.Serialization;
using Gateway.ProjectHorn.Services.Serialization.Definitions;

/**
 * 22/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Realm
{
    public class RealmEntityLimit<T> : RealmLimit where T : EntityDef
    {
        public bool TargetsSpecificDef { get; private set; }

        public uint DefinitionUID { get; private set; }

        // private T targetDef;

        public RealmEntityLimit(int current, int limit, string entityDefName = null) : base(current, limit)
        {
            if (entityDefName != null)
            {
                SetSpecificDefinition(entityDefName);
            }
        }

        public void SetSpecificDefinition(string entityDefName)
        {
            SetSpecificDefinition(ServiceInjector.GetService<DefinitionService>().GetDefinition<T>(entityDefName));
        }

        public void SetSpecificDefinition(T targetDefinition)
        {
            TargetsSpecificDef = true;
            DefinitionUID = targetDefinition.UID;
        }

        public bool EqualsSpecificDefinition(BaseDef definition)
        {
            if (TargetsSpecificDef)
            {
                return DefinitionUID == definition.UID;
            }

            return true;
        }
    }
}