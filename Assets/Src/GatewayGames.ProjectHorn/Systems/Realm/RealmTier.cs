

using System;
using Gateway.ProjectHorn.Services.Serialization.Definitions;
using Gateway.ProjectHorn.Services.Serialization.Definitions.District;
using Gateway.ProjectHorn.Services.Serialization.Definitions.Building;

/**
 * 22/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Realm
{
    public class RealmTier : ICloneable
    {
        public readonly uint TierIndex;

        //todo: make dynamic
        public RealmEntityLimit<BuildingDef> HouseLimit { get; private set; }

        public RealmEntityLimit<DistrictDef> DistrictLimit { get; private set; }

        public RealmTier(uint tier, int houseLimit, int districtLimit)
        {
            TierIndex = tier;
            HouseLimit = new RealmEntityLimit<BuildingDef>(0, houseLimit, "House");
            DistrictLimit = new RealmEntityLimit<DistrictDef>(0, districtLimit);
        }

        public bool HasReachedLimit(BaseDef definition)
        {
            var defType = definition.GetType();

            if (defType == typeof(BuildingDef) && HouseLimit.EqualsSpecificDefinition(definition))
            {
                return HouseLimit.LimitReached;
            }

            if (defType == typeof(DistrictDef) && DistrictLimit.EqualsSpecificDefinition(definition))
            {
                return DistrictLimit.LimitReached;
            }

            return false;
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}