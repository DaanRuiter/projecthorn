

using Unity.Mathematics;

/**
 * 22/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Realm
{
    public class RealmLimit
    {
        public int Current { get; private set; }

        public int Limit { get; private set; }

        public RealmLimit(int current, int limit)
        {
            Current = current;
            Limit = limit;
        }

        public bool LimitReached => Current >= Limit;

        public void Increment(int amount = 1)
        {
            Current = math.clamp(Current + amount, Current, Limit);
        }

        internal void SetCurrent(int current)
        {
            Current = math.clamp(current, Current, Limit);
        }
    }
}