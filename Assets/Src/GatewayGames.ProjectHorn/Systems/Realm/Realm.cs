

using Gateway.ProjectHorn.Systems.Terrain.Tile;
using UnityEngine;

/**
 * 22/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Realm
{
    public class Realm
    {
        private static readonly RealmTier[] RealmTiers = new[]
        {
            new RealmTier(0, 5, 2),
            new RealmTier(1, 10, 4),
            new RealmTier(2, 20, 6),
            new RealmTier(3, 50, 10),
            new RealmTier(4, 99999, 99999),
        };

        public RealmTier Tier => m_tier;

        public TileGroup Tiles => m_tiles;

        private TileGroup m_tiles;
        private ushort m_factionUID;
        private RealmTier m_tier;

        public Realm(ushort factionUID)
        {
            m_factionUID = factionUID;
            m_tiles = new TileGroup(m_factionUID);
            SetTier();
        }

        public void SetTier(uint tier = 0)
        {
            if (!IsTierSupported(tier))
            {
                Debug.LogError("Tier not supported: " + tier);
                return;
            }

            RealmTier currentTier = m_tier;

            m_tier = (RealmTier)RealmTiers[tier].Clone();

            if (currentTier != null)
            {
                m_tier.DistrictLimit.SetCurrent(currentTier.DistrictLimit.Current);
                m_tier.HouseLimit.SetCurrent(currentTier.HouseLimit.Current);
            }
        }

        public void UpgradeTier()
        {
            uint nextTier = m_tier.TierIndex + 1;

            if (IsTierSupported(nextTier))
            {
                SetTier(nextTier);
            }
        }

        private bool IsTierSupported(uint tier)
        {
            if (tier >= RealmTiers.Length)
            {
                return false;
            }

            return true;
        }
    }
}