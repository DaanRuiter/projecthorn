using System.Collections.Generic;
using Gateway.ProjectHorn.Services.Serialization.Definitions.Prop;
using Unity.Entities;
using Unity.Mathematics;

/**
 * 20/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Building
{
    public class DistrictNode
    {
        public float2 WorldPosition { get; internal set; }

        public Entity Entity { get; internal set; }

        public PropNodeDef Props { get; internal set; }

        public Dictionary<string, Entity> ChildEntities { get; }

        public DistrictNode()
        {
            ChildEntities = new Dictionary<string, Entity>();
        }

        public DistrictNode(Entity entity) : this()
        {
            Entity = entity;
        }
    }
}