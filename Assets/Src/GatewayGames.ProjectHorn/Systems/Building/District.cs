using System.Collections.Generic;
using Gateway.ProjectHorn.Services.Serialization.Definitions.Item;
using Unity.Entities;
using Unity.Mathematics;

/**
 * 11/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Building
{
    public class District
    {
        public const int NodeGridSize = 3;
        public const float NodeLocalSize = 1f / NodeGridSize;
        public const float HalfNodeLocalSize = NodeLocalSize / 2f;

        public Entity Entity { get; internal set; }

        public DistrictNode this[int x, int y] => GetNode(x, y);

        public DistrictNode this[int2 pos] => GetNode(pos);

        public DistrictNode[,] NodeMap { get; }

        // public ItemYieldDef[] CachedItemYields => m_cachedItemYields;
        //
        // private ItemYieldDef[] m_cachedItemYields;

        private Dictionary<uint, int> m_inventory;

        public District()
        {
            NodeMap = new DistrictNode[NodeGridSize, NodeGridSize];
            m_inventory = new Dictionary<uint, int>();
        }

        public District(Entity entity) : this()
        {
            Entity = entity;
        }

        public bool IsNodeFree(int2 localPosition)
        {
            return NodeMap[localPosition.x, localPosition.y] == null;
        }

        public void SetNode(int2 localPosition, DistrictNode node)
        {
            NodeMap[localPosition.x, localPosition.y] = node;
            UpdateCachedItemYields();
        }

        public void UpdateInventory(uint item, int count)
        {
            if (!m_inventory.ContainsKey(item))
            {
                m_inventory.Add(item, count);
            }
            else
            {
                int current = m_inventory[item];
                if (current + count >= 0)
                {
                    m_inventory[item] += count;
                }
            }
        }

        public bool HasItem(uint item, int count = 1)
        {
            return m_inventory.ContainsKey(item) && m_inventory[item] >= count;
        }

        public Dictionary<uint, int> GetInventoryData()
        {
            // var inventory = new NativeHashMap<uint, int>(m_inventory.Count(), allocator);
            // var itemUIDS = m_inventory.Keys.ToArray();
            //
            // for (int i = 0; i < itemUIDS.Length; i++)
            // {
            //     inventory.Add(itemUIDS[i], m_inventory[itemUIDS[i]]);
            // }

            return m_inventory;
        }

        public DistrictNode GetNode(int x, int y) => NodeMap[x, y];

        public DistrictNode GetNode(int2 nodePosition) => GetNode(nodePosition.x, nodePosition.y);

        public DistrictNode GetNodeClosestTo(float2 worldPosition)
        {
            var nodePos = WorldToNodePosition(worldPosition);

            return NodeMap[nodePos.x, nodePos.y];
        }

        private void UpdateCachedItemYields() //Optimize by using list to add remove updated
        {
            // var yields = new List<ItemYieldDef>();
            //
            // for (int x = 0; x < NodeGridSize; x++)
            // {
            //     for (int y = 0; y < NodeGridSize; y++)
            //     {
            //         var node = GetNode(x, y);
            //
            //         if (node != null && node.Props != null)
            //         {
            //             yields.Add(node.Props.Prop.ItemYield);
            //         }
            //     }
            // }
            //
            // m_cachedItemYields = yields.ToArray();
        }

        public NodeItemYield[] GetNodeYieldData()
        {
            var result = new List<NodeItemYield>();

            for (int x = 0; x < NodeGridSize; x++)
            {
                for (int y = 0; y < NodeGridSize; y++)
                {
                    var node = GetNode(x, y);

                    if (node != null && node.Props != null)
                    {
                        result.Add(new NodeItemYield()
                        {
                            WorldPosition = node.WorldPosition,
                            YieldData = node.Props.Prop.ItemYield.AsData()
                        });
                    }
                }
            }

            return result.ToArray();
        }

        public static int2 WorldToNodePosition(float2 worldPosition)
        {
            worldPosition -= math.floor(worldPosition);
            worldPosition += NodeLocalSize / 2f;

            int nodeX = (int)math.floor(worldPosition.x / NodeLocalSize);
            int nodeY = (int)math.floor(worldPosition.y / NodeLocalSize);

            return new int2(nodeX, nodeY);
        }

        public struct NodeItemYield
        {
            public float2 WorldPosition;
            public ItemYieldData YieldData;
        }
    }
}