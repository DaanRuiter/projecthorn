﻿using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Systems.Entities;
using Gateway.ProjectHorn.Systems.Entities.Components.Building;
using Gateway.ProjectHorn.Util;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

/**
 * 25/01/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.DOTS.Building
{
    [BurstCompile]
    public class ActorSpawnerSystem : BaseDOTSSystem
    {
        public struct SpawnActorCommand
        {
            public bool IsNotNull;
            public float2 Pos;
            public uint UID;
            public int SpawnRange;
        }

        protected override float updateInterval => .1f;

        [InjectService] private EntitySystem _entitySystem;

        private EntityQuery _spawnerQuery;

        protected override void StartSystem()
        {
            _spawnerQuery = GetEntityQuery(typeof(ActorSpawner));
        }

        protected override JobHandle UpdateSystem(JobHandle inputDeps, SystemTimeInfo timeInfo)
        {
            NativeArray<SpawnActorCommand> spawnCommands =
                new NativeArray<SpawnActorCommand>(_spawnerQuery.CalculateEntityCount(), Allocator.TempJob);

            var checkSpawners = Entities
                .WithStoreEntityQueryInField(ref _spawnerQuery)
                .ForEach((int entityInQueryIndex, ref ActorSpawner spawner, in Translation translation) =>
                {
                    float targetTime = spawner.LastSpawnTimestamp + spawner.SpawnInterval;
                    bool limitReached = spawner.SpawnCount >= spawner.SpawnLimit;

                    if (!limitReached && timeInfo.CurrentTime > targetTime)
                    {
                        spawner.LastSpawnTimestamp = timeInfo.CurrentTime;
                        spawner.SpawnCount++;

                        spawnCommands[entityInQueryIndex] =
                            new SpawnActorCommand()
                            {
                                UID = spawner.ActorUID,
                                Pos = translation.Value.ToFloat2(),
                                IsNotNull = true,
                                SpawnRange = spawner.SpawnRange
                            };
                    }
                }).Schedule(inputDeps);

            checkSpawners.Complete();

            for (int i = 0; i < spawnCommands.Length; i++)
            {
                if (spawnCommands[i].IsNotNull)
                {
                    _entitySystem.SpawnActor(spawnCommands[i]);
                }
            }

            spawnCommands.Dispose();

            return checkSpawners;
        }

        protected override void StopSystem()
        {
        }

        // [BurstCompile]
        // public struct CheckSpawnerJob : IJobForEach<ActorSpawner, Translation>
        // {
        //     public NativeList<float3> Positions;
        //     public NativeList<uint> ActorUIDs;
        //
        //     public void Execute(ref ActorSpawner spawner, [ReadOnly] ref Translation translation)
        //     {
        //         float currentTime = UnityEngine.Time.realtimeSinceStartup;
        //         float targetTime = spawner.LastSpawnTimestamp + spawner.SpawnInterval;
        //
        //         if (targetTime >= currentTime)
        //         {
        //             spawner.LastSpawnTimestamp = currentTime;
        //
        //             Positions.Add(translation.Value);
        //             ActorUIDs.Add(spawner.ActorUID);
        //         }
        //     }
        // }

// job.Complete();

// foreach (var spawnsJob in spawnsJobs)
// {
//     var actorSpawnerDef = _definitionService.GetDefinitionByUID<SpawnerDef>(spawnsJob.ActorUID);
//
//     Main.Instance.EntitySystem.SpawnActor(actorSpawnerDef.Spawnable, spawnsJob.Position);
// }
//
// spawnsJobs.Dispose();
// inputDeps.Complete();

// return inputDeps;

// CurrentJob = new CheckSpawnerJob()
// {
//     Positions = new NativeList<float3>(Allocator.TempJob),
//     ActorUIDs = new NativeList<uint>(Allocator.TempJob)
// };
//
// return job.Schedule(this, inputDeps);
    }
}