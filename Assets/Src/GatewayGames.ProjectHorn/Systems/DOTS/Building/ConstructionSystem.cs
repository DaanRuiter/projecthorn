
using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Services.Serialization.Definitions.Building;
using Gateway.ProjectHorn.Systems.Actor;
using Gateway.ProjectHorn.Systems.Actor.TaskMaster;
using Gateway.ProjectHorn.Systems.Building;
using Gateway.ProjectHorn.Systems.Entities;
using Gateway.ProjectHorn.Systems.Entities.Components.Actor;
using Gateway.ProjectHorn.Systems.Entities.Components.Building;
using Gateway.ProjectHorn.Systems.Terrain;
using Gateway.ProjectHorn.Util;
using Gateway.ProjectHorn.Util.HelperTypes;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

/**
 * 03/07/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.DOTS.Building
{
    public struct ConstructionResolveData
    {
        public Entity Entity;
        public float2 WorldPosition;
    }

    public class ConstructionSystem : BaseDOTSSystem
    {
        public const string ScaffoldName = "Scaffolding"; //todo: make dynamic
        public const int MaxBuildersPerBuilding = 1;

        protected override float updateInterval => .2f;

        [InjectService] private static EntitySystem i_entitySystem;
        [InjectService] private static TerrainSystem i_terrainSystem;

        private EntityQuery m_builderQuery;
        private EntityQuery m_buildingQuery;

        private ActorTaskSystem _actorTaskSystem;
        private ActorConstructionTaskMaster _constructionTaskMaster;

        protected override void StartSystem()
        {
            m_builderQuery = GetEntityQuery(typeof(ActorLocalBuilderData),
                typeof(ActorBaseData));
            m_buildingQuery = GetEntityQuery(typeof(BuildingConstructionData));

            _actorTaskSystem = Main.GetOrCreateSystem<ActorTaskSystem>();
            _constructionTaskMaster = _actorTaskSystem.GetTaskMaster<ActorConstructionTaskMaster>();
        }

        protected override JobHandle UpdateSystem(JobHandle inputDeps, SystemTimeInfo timeInfo)
        {
            var finishedBuildings =
                new NativeHashMap<Entity, ConstructionResolveData>(m_buildingQuery.CalculateEntityCount(),
                    Allocator.TempJob);
            var builderActorData = m_builderQuery.ToComponentDataArray<ActorBaseData>(Allocator.TempJob);
            var builders = m_builderQuery.ToEntityArray(Allocator.TempJob);
            var taskBuffer = new NativeList<ActorTaskExecutionInfo>(Allocator.TempJob);
            int cachedUnassignedBuilderIndex = 0;

            Entities.ForEach((ref BuildingConstructionData constructionData,
                in Translation translation,
                in Entity entity) =>
            {
                //Assign workers
                if (!constructionData.BuildersAssigned.CapReached)
                {
                    var unassignedBuilder =
                        GetUnassignedBuilder(builderActorData, builders, ref cachedUnassignedBuilderIndex);

                    if (unassignedBuilder.Item1 != Entity.Null)
                    {
                        var builder = unassignedBuilder.Item1;

                        constructionData.BuildersAssigned.Add(1);

                        taskBuffer.Add(new ActorTaskExecutionInfo()
                        {
                            Actor = builder,
                            TargetEntity = entity,
                            TargetPosition = translation.Value.ToFloat2().Add(District.HalfNodeLocalSize),
                            TaskExecuteInterval = 1f,
                            TaskExecuteStepCount = constructionData.Progress
                        });
                    }
                }
            }).Run();

            for (int i = 0; i < taskBuffer.Length; i++)
            {
                var taskInfo = taskBuffer[i];

                // var overlay = i_entitySystem.
                var task = _constructionTaskMaster.CreateTask(taskInfo);

                _actorTaskSystem.EnqueueTask(task);
            }

            // Entities.WithStructuralChanges().ForEach((int entityInQueryIndex,
            //     ref ActorLocalBuilderData builderData,
            //     in ActorLocalMoveData moveData,
            //     in Entity entity) =>
            // {
            //     var targetBuilding = builderData.TargetBuilding;
            //     if (targetBuilding == Entity.Null)
            //     {
            //         return;
            //     }
            //
            //     if (!entityManager.HasComponent<BuildingConstructionData>(targetBuilding))
            //     {
            //         //todo: fix this
            //         LogWarning($"{entity} has broken target building {targetBuilding}");
            //         builderData.TargetBuilding = Entity.Null;
            //         return;
            //     }
            //
            //     var constructionData = entityManager.GetComponentData<BuildingConstructionData>(targetBuilding);
            //
            //     if (moveData.ReachedTarget && !constructionData.Progress.CapReached)
            //     {
            //         constructionData.Progress.Add(builderData.BuildPower);
            //     }
            //
            //     if (constructionData.Progress.CapReached)
            //     {
            //         finishedBuildings.Add(targetBuilding,
            //             new ConstructionResolveData
            //             {
            //                 Entity = targetBuilding,
            //                 WorldPosition = targetBuilding.GetPosition().Add(.05f)
            //             });
            //
            //         constructionData.BuildersAssigned.SetCount(0);
            //         builderData.TargetBuilding = Entity.Null;
            //     }
            //
            //     entityManager.SetComponentData(targetBuilding, constructionData);
            //     builderDatas[entityInQueryIndex] = builderData;
            // }).Run();

            var finishedBuildingEntities = finishedBuildings.GetKeyArray(Allocator.TempJob);
            for (int i = 0; i < finishedBuildings.Count(); i++)
            {
                var resolveData = finishedBuildings[finishedBuildingEntities[i]];
                var buildingDef = i_entitySystem.GetDefinitionForEntity<BuildingDef>(resolveData.Entity);

                i_entitySystem.FinishBuildingInDistrict(buildingDef, resolveData.WorldPosition);

                entityManager.RemoveComponent<BuildingConstructionData>(resolveData.Entity);

                // for (int j = 0; j < builderDatas.Length; j++)
                // {
                //     var builderData = builderDatas[i];
                //     if (builderData.TargetBuilding == resolveData.Entity)
                //     {
                //         builderData.TargetBuilding = Entity.Null;
                //         entityManager.SetComponentData(builders[i], builderData);
                //     }
                // }
            }

            finishedBuildingEntities.Dispose();
            finishedBuildings.Dispose();
            builderActorData.Dispose();
            builders.Dispose();
            taskBuffer.Dispose();

            return inputDeps;
        }

        protected override void StopSystem()
        {
        }

        private static DataTuple<Entity, ActorBaseData> GetUnassignedBuilder(NativeArray<ActorBaseData> builderData,
            NativeArray<Entity> builders,
            ref int cachedStartIndex)
        {
            for (int i = cachedStartIndex; i < builders.Length; i++)
            {
                if (builderData[i].TaskState == ActorTaskState.None)
                {
                    cachedStartIndex = i;
                    return new DataTuple<Entity, ActorBaseData>()
                    {
                        Item1 = builders[i],
                        Item2 = builderData[i]
                    };
                }
            }

            return new DataTuple<Entity, ActorBaseData>()
            {
                Item1 = Entity.Null
            };
        }

        public static Entity GetScaffoldOverlayEntity(float2 worldPosition)
        {
            var tile = i_terrainSystem.Tiles.GetTile(worldPosition);
            return tile.District[worldPosition.ToNodePos()].ChildEntities[ScaffoldName];
        }
    }
}