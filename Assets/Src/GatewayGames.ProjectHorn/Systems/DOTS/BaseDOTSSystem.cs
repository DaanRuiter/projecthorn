using Gateway.ProjectHorn.Services;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

/**
 * 17/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */
namespace Gateway.ProjectHorn.Systems.DOTS
{
    public abstract class BaseDOTSSystem : JobComponentSystem
    {
        protected struct SystemTimeInfo
        {
            public float CurrentTime;
            public float TimeScale;
            public float DeltaTime;
        }

        protected virtual float updateInterval => 0f;

        protected virtual bool waitForMain => true;

        protected static float cachedRealTime { get; private set; }

        protected static float realTime => UnityEngine.Time.realtimeSinceStartup;

        protected static EntityManager entityManager => World.DefaultGameObjectInjectionWorld.EntityManager;

        private static float m_scaledTime;

        private float m_nextUpdateTimestamp;
        private bool m_created;

        internal void InternalOnCreate()
        {
            OnCreate();
        }

        protected sealed override void OnCreate()
        {
            if (Main.Ready && !m_created)
            {
                Debug.Log($"System {GetType().Name} Created");
                m_created = true;
                ServiceInjector.InjectServices(this);
                StartSystem();
            }
        }

        protected sealed override JobHandle OnUpdate(JobHandle inputDeps)
        {
            if (!waitForMain || Main.Ready)
            {
                if (!m_created)
                {
                    OnCreate();
                }
            }
            else
            {
                return inputDeps;
            }

            cachedRealTime = realTime;

            if (cachedRealTime >= m_nextUpdateTimestamp)
            {
                m_nextUpdateTimestamp = cachedRealTime + (updateInterval / Main.TimeScale);

                var timeInfo = new SystemTimeInfo()
                {
                    TimeScale = Main.TimeScale,
                    CurrentTime = UnityEngine.Time.realtimeSinceStartup,
                    DeltaTime = Time.DeltaTime
                };

                return UpdateSystem(inputDeps, timeInfo);
            }

            return inputDeps;
        }

        protected sealed override void OnDestroy() => StopSystem();

        protected abstract void StartSystem();

        protected abstract JobHandle UpdateSystem(JobHandle inputDeps, SystemTimeInfo timeInfo);

        protected abstract void StopSystem();

        protected static float3 GetEntityPosition(Entity entity)
        {
            return entityManager.GetComponentData<Translation>(entity).Value;
        }

        protected static void Log(object message) => Debug.Log(string.Format("{0}", message));

        protected static void LogWarning(object message) => Debug.LogWarning(string.Format("{0}", message));

        protected static void LogError(object message) => Debug.LogError(string.Format("{0}", message));
    }
}