

using Unity.Entities;

/**
 * 26/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.DOTS.Item
{
    public struct ItemStackUpdate : IComponentData
    {
        public Entity Entity;
        public uint ItemUID;
        public int ItemCountChange;

        public bool IncreasesItemCount => ItemCountChange > 0;
    }
}