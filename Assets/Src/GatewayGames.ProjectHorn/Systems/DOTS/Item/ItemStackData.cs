

using Gateway.ProjectHorn.Systems.Entities.Components.Actor;
using Gateway.ProjectHorn.Systems.Entities.Components.Common;
using Unity.Entities;

/**
 * 08/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.DOTS.Item
{
    public struct ItemStackData : IComponentData
    {
        public const int MaxCarryItemCount = 10;
        public const int MaxStackItemCount = 10;

        public uint ItemUID;
        public URange ItemCount;

        public bool IsEmpty => ItemCount.Count == 0 && ItemCount.Cap > 0;

        public bool IsFull => ItemCount.CapReached;

        public bool ContainsItem(uint itemUID) => ItemUID == itemUID;

        public ItemStackCarryData ToCarryData(Entity itemStackEntity)
        {
            return new ItemStackCarryData()
            {
                ItemCount = ItemCount,
                ItemUID = ItemUID,
            };
        }
    }
}