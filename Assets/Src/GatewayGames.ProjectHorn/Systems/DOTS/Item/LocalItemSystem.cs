

using Gateway.ProjectHorn.Components.UI.EntityHUD;
using Gateway.ProjectHorn.Components.UI.Item;
using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Systems.Building;
using Gateway.ProjectHorn.Systems.DOTS.Common;
using Gateway.ProjectHorn.Systems.Entities.Components.Actor;
using Gateway.ProjectHorn.Systems.Entities.Components.Common;
using Gateway.ProjectHorn.Systems.Terrain;
using Gateway.ProjectHorn.Systems.UI;
using Gateway.ProjectHorn.Util;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

/**
 * 08/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.DOTS.Item
{
    // [BurstCompile]
    [AlwaysUpdateSystem]
    public class LocalItemSystem : BaseDOTSSystem
    {
        private struct ItemStackUpdateResult
        {
            public Entity Entity;
            public Entity ItemStackEntity;
            public uint ItemUID;
            public int ItemCountChange;
        }

        private struct ItemStackUpdateInfo
        {
            public Range ItemCountResult;
            public int ItemCountSpace;
            public int ItemCountResidue;
            public int ItemCountChange;
        }

        public static bool Ready { get; private set; }

        private static NativeList<ItemStackUpdateResult> _itemStackUpdateResults;

        private static EntityQuery m_itemStackQuery;

        protected override float updateInterval => .1f;

        protected override bool waitForMain => false;

        [InjectService] private UISystem i_uiSystem;
        [InjectService] private TerrainSystem i_terrainSystem;

        protected override void StartSystem()
        {
            m_itemStackQuery = GetEntityQuery(typeof(ItemStackData), typeof(EntityIndex));
            Ready = true;
        }

        protected override JobHandle UpdateSystem(JobHandle inputDeps, SystemTimeInfo timeInfo)
        {
            _itemStackUpdateResults = new NativeList<ItemStackUpdateResult>(Allocator.TempJob);

            Entities
                .WithStructuralChanges()
                .WithoutBurst()
                .ForEach((int entityInQueryIndex, ref ItemStackUpdate stackUpdate, in Entity stackUpdateEntity) =>
                {
                    m_itemStackQuery.SetSharedComponentFilter(new EntityIndex()
                    {
                        Entity = stackUpdate.Entity
                    });
                    var itemStacks = m_itemStackQuery.ToEntityArray(Allocator.TempJob);

                    if (stackUpdate.IncreasesItemCount)
                    {
                        var updateInfo = new ItemStackUpdateInfo()
                        {
                            ItemCountResidue = stackUpdate.ItemCountChange
                        };

                        // Check existing ItemStacks to unload items into
                        for (int i = 0; i < itemStacks.Length; i++)
                        {
                            var itemStackData = entityManager.GetComponentData<ItemStackData>(itemStacks[i]);

                            if (!itemStackData.IsFull && itemStackData.ContainsItem(stackUpdate.ItemUID))
                            {
                                updateInfo = PerformItemStackUpdate(itemStacks[i], ref itemStackData, stackUpdate);
                            }
                        }

                        // Create new ItemStack if there was no stack with room left
                        if (updateInfo.ItemCountResidue > 0)
                        {
                            CreateItemStack(entityManager,
                                stackUpdate.ItemUID,
                                (uint)updateInfo.ItemCountResidue,
                                ItemStackData.MaxStackItemCount,
                                stackUpdate.Entity);
                        }
                    }
                    else
                    {
                        int removeCount = math.abs(stackUpdate.ItemCountChange);

                        for (int i = 0; i < itemStacks.Length; i++)
                        {
                            if (removeCount > 0)
                            {
                                var itemStackData = entityManager.GetComponentData<ItemStackData>(itemStacks[i]);
                                if (itemStackData.ItemUID == stackUpdate.ItemUID)
                                {
                                    uint newItemCount =
                                        (uint)math.clamp(itemStackData.ItemCount.Count - removeCount,
                                            0,
                                            itemStackData.ItemCount.Cap);
                                    uint taken = itemStackData.ItemCount.Count - newItemCount;
                                    removeCount -= (int)taken;

                                    if (newItemCount > 0)
                                    {
                                        itemStackData.ItemCount.SetCount(newItemCount);
                                        entityManager.SetComponentData(itemStacks[i], itemStackData);
                                    }
                                    else
                                    {
                                        DestroyItemStack(itemStacks[i]);
                                    }

                                    _itemStackUpdateResults.Add(new ItemStackUpdateResult()
                                    {
                                        Entity = stackUpdate.Entity,
                                        ItemStackEntity = itemStacks[i],
                                        ItemCountChange = -(int)taken,
                                        ItemUID = itemStackData.ItemUID
                                    });
                                }
                            }
                        }
                    }

                    entityManager.DestroyEntity(stackUpdateEntity);
                    itemStacks.Dispose();
                }).Run();

            //Update itemstack HUDs
            var entityHUDContainer = i_uiSystem.GetElement<DistrictItemHUDContainer>();
            if (entityHUDContainer != null)
            {
                foreach (var result in _itemStackUpdateResults)
                {
                    if (entityHUDContainer.ContainsOfTypeEntity<ItemStackListHUD>(result.Entity))
                    {
                        var itemStackEntity = result.ItemStackEntity;
                        var stackContainer = entityHUDContainer.GetEntityHUD<ItemStackListHUD>(result.Entity);
                        var stackElement = stackContainer.GetElement(itemStackEntity);
                        var districtTilePos = entityManager.GetComponentData<Translation>(result.Entity).Value
                            .ToFloat2()
                            .ToTilePos();

                        //Update district inventory
                        i_terrainSystem.Tiles.GetTile(districtTilePos).District
                            .UpdateInventory(result.ItemUID, result.ItemCountChange);

                        if (stackElement == null)
                        {
                            //Create new itemStack HUD element
                            stackElement = entityHUDContainer.CreateItemStackHUDElement(itemStackEntity);
                            stackElement.SetItem(result.ItemUID, ItemStackData.MaxStackItemCount, 0);
                            stackContainer.AddElement(stackElement);
                        }

                        stackElement.ChangeItemCountBy(result.ItemCountChange);
                    }
                }
            }

            // workJob.Complete();

            _itemStackUpdateResults.Dispose();

            return inputDeps;
        }

        protected override void StopSystem()
        {
        }

        //Helpers
        public static void DestroyItemStack(Entity itemstack)
        {
            entityManager.DestroyEntity(itemstack);
        }

        public static NativeMultiHashMap<Entity, ItemStackCarryData> GetActorCarryData(NativeArray<Entity> actors,
            Allocator allocator)
        {
            var carryData = new NativeMultiHashMap<Entity, ItemStackCarryData>(actors.Length, allocator);

            for (int i = 0; i < actors.Length; i++)
            {
                m_itemStackQuery.SetSharedComponentFilter(new EntityIndex()
                {
                    Entity = actors[i]
                });

                var itemStackEntities = m_itemStackQuery.ToEntityArray(Allocator.TempJob);
                var itemStacks = m_itemStackQuery.ToComponentDataArray<ItemStackData>(itemStackEntities,
                    Allocator.TempJob);

                for (int j = 0; j < itemStacks.Length; j++)
                {
                    var data = itemStacks[j].ToCarryData(itemStackEntities[j]);
                    carryData.Add(actors[i], data);
                }

                itemStackEntities.Dispose();
                itemStacks.Dispose();
            }

            return carryData;
        }

        public static NativeArray<ItemStackData> GetActorCarryData(Entity actor)
        {
            m_itemStackQuery.SetSharedComponentFilter(new EntityIndex()
            {
                Entity = actor
            });

            return m_itemStackQuery.ToComponentDataArray<ItemStackData>(
                m_itemStackQuery.ToEntityArray(Allocator.TempJob),
                Allocator.TempJob);
        }

        public static void SetItemStackOwnerDistrict(Entity itemStackEntity, District targetDistrict)
        {
            SetItemStackOwner(itemStackEntity, targetDistrict.Entity);
        }

        public static void SetItemStackOwner(Entity itemStackEntity, Entity ownerEntity)
        {
            var entityIndexData = entityManager.GetSharedComponentData<EntityIndex>(itemStackEntity);

            if (entityIndexData.Entity != ownerEntity)
            {
                entityManager.SetSharedComponentData(itemStackEntity,
                    new EntityIndex()
                    {
                        Entity = ownerEntity
                    });
            }
        }

        public static void TransferItems(Entity sourceEntity, Entity targetEntity, uint itemUID, int itemCount)
        {
            CreateItemStackUpdate(itemUID, -itemCount, sourceEntity);
            CreateItemStackUpdate(itemUID, itemCount, targetEntity);
        }

        public static void GiveItemsTo(Entity targetEntity, uint itemUID, int itemCount)
        {
            CreateItemStackUpdate(itemUID, Mathf.Abs(itemCount), targetEntity);
        }

        public static void TakeItemsFrom(Entity targetEntity, uint itemUID, int itemCount)
        {
            CreateItemStackUpdate(itemUID, -Mathf.Abs(itemCount), targetEntity);
        }

        public static void CreateItemStackUpdate(uint itemUID, int itemCountChange, Entity entity)
        {
            if (itemCountChange != 0)
            {
                var itemStackEntity = entityManager.CreateEntity();
                entityManager.AddComponentData(itemStackEntity,
                    new ItemStackUpdate()
                    {
                        Entity = entity,
                        ItemCountChange = itemCountChange,
                        ItemUID = itemUID
                    });
            }
        }

        private static Entity CreateItemStack(EntityManager manager,
            uint itemUID,
            uint itemCount,
            uint itemLimit,
            Entity entity)
        {
            var newItemStack = manager.CreateEntity(typeof(EntityIndex), typeof(ItemStackData));

            manager.AddComponentData(newItemStack,
                new ItemStackData()
                {
                    ItemUID = itemUID,
                    ItemCount = new URange(itemLimit, itemCount) //todo: clamp again
                });
            manager.AddComponentData(newItemStack, //todo: allow multiple itemstack huds per entity and target building
                new Translation() { Value = manager.GetComponentData<Translation>(entity).Value });
            manager.AddSharedComponentData(newItemStack, new EntityIndex() { Entity = entity });

            _itemStackUpdateResults.Add(new ItemStackUpdateResult()
            {
                Entity = entity,
                ItemStackEntity = newItemStack,
                ItemCountChange = (int)itemCount,
                ItemUID = itemUID,
            });

            return newItemStack;
        }

        private static ItemStackUpdateInfo PerformItemStackUpdate(Entity itemStack,
            ref ItemStackData itemStackData,
            ItemStackUpdate stackUpdate)
        {
            var updateInfo = GetItemStackUpdateInfo(itemStackData, stackUpdate);

            entityManager.SetComponentData(itemStack,
                new ItemStackData()
                {
                    ItemCount = updateInfo.ItemCountResult.ToURange(),
                    ItemUID = itemStackData.ItemUID
                });

            _itemStackUpdateResults.Add(new ItemStackUpdateResult()
            {
                Entity = stackUpdate.Entity,
                ItemStackEntity = itemStack,
                ItemCountChange = updateInfo.ItemCountChange,
                ItemUID = itemStackData.ItemUID
            });

            return updateInfo;
        }

        private static ItemStackUpdateInfo GetItemStackUpdateInfo(ItemStackData itemStackData,
            ItemStackUpdate stackUpdate)
        {
            int newItemCount = (int)math.clamp(itemStackData.ItemCount.Count + stackUpdate.ItemCountChange,
                itemStackData.ItemCount.Count,
                itemStackData.ItemCount.Cap);
            int space = (int)itemStackData.ItemCount.Space;
            int notAbleToUnload = math.clamp(stackUpdate.ItemCountChange - space, 0, stackUpdate.ItemCountChange);
            int change = stackUpdate.ItemCountChange - notAbleToUnload;

            return new ItemStackUpdateInfo()
            {
                ItemCountResult = new Range(itemStackData.ItemCount.SignedCap, newItemCount),
                ItemCountSpace = space,
                ItemCountResidue = notAbleToUnload,
                ItemCountChange = change,
            };
        }
    }
}