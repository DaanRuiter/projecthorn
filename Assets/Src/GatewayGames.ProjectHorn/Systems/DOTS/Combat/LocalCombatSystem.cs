

using System.Collections.Generic;
using Gateway.ProjectHorn.Components.UI.EntityHUD;
using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Systems.DOTS.Common;
using Gateway.ProjectHorn.Systems.Entities.Components.Actor;
using Gateway.ProjectHorn.Systems.Entities.Components.Combat;
using Gateway.ProjectHorn.Systems.UI;
using Gateway.ProjectHorn.Systems.UI.Factories;
using Gateway.ProjectHorn.Util;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Random = Unity.Mathematics.Random;

/**
 * 02/06/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.DOTS.Combat
{
    [AlwaysUpdateSystem]
    public class LocalCombatSystem : BaseDOTSSystem
    {
        private static readonly Dictionary<Entity, int2> m_moveCommands = new Dictionary<Entity, int2>();

        protected override float updateInterval => .2f;

        [InjectService] private static UISystem i_uiSystem;

        public static Entity TestCombatUnit;
        private static EntityQuery m_combatUnitQuery;
        private static CombatUIFactory _combatUIFactory;
        private EntityQuery m_solderQuery;

        protected override void StartSystem()
        {
            m_solderQuery = GetEntityQuery(typeof(ActorLocalMoveData),
                typeof(ActorLocalSoldierData),
                typeof(CombatUnitIndex));
            m_combatUnitQuery = GetEntityQuery(typeof(CombatUnitData), typeof(EntityIndex));
        }

        protected override JobHandle UpdateSystem(JobHandle inputDeps, SystemTimeInfo timeInfo)
        {
            var moveCommands = m_moveCommands.ToNativeHashMap(Allocator.TempJob);
            var soldierQuery = m_solderQuery;
            uint baseRandomSeed = (uint)UnityEngine.Random.Range(1, 999999);

            Entities
                .WithStructuralChanges()
                .ForEach((int entityInQueryIndex, ref CombatUnitData combatUnitData, in Entity entity) =>
                {
                    if (moveCommands.ContainsKey(entity))
                    {
                        combatUnitData.CurrentTileTarget = moveCommands[entity];

                        soldierQuery.SetSharedComponentFilter(new CombatUnitIndex()
                        {
                            CombatUnitEntity = entity
                        });

                        var soldiers = soldierQuery.ToEntityArray(Allocator.TempJob);
                        var moveData = soldierQuery.ToComponentDataArray<ActorLocalMoveData>(Allocator.TempJob);
                        var soldierData = soldierQuery.ToComponentDataArray<ActorLocalSoldierData>(Allocator.TempJob);

                        var random = Random.CreateFromIndex(baseRandomSeed + ((uint)entityInQueryIndex));
                        var randomFloat2s = random.GenerateRandomFloat2s(0f, 1f, soldiers.Length);

                        for (int i = 0; i < soldiers.Length; i++)
                        {
                            var soldier = soldiers[i];

                            var actorMoveData = moveData[i];
                            float2 targetPos = combatUnitData.CurrentTileTarget.AddFloat2(randomFloat2s[i]);
                            actorMoveData.SetTarget(targetPos);
                            entityManager.SetComponentData(soldier, actorMoveData);

                            var actorSoldierData = soldierData[i];
                            actorSoldierData.State = ActorSoldierState.Moving;
                            entityManager.SetComponentData(soldier, actorSoldierData);
                        }

                        soldiers.Dispose();
                        moveData.Dispose();
                        soldierData.Dispose();
                        randomFloat2s.Dispose();
                    }
                }).Run();

            m_moveCommands.Clear();
            moveCommands.Dispose();

            return inputDeps;
        }

        protected override void StopSystem()
        {
        }

        public static void SetMoveCommand(Entity combatUnitEntity, int2 targetTilePosition)
        {
            if (m_moveCommands.ContainsKey(combatUnitEntity))
            {
                m_moveCommands[combatUnitEntity] = targetTilePosition;
            }
            else
            {
                m_moveCommands.Add(combatUnitEntity, targetTilePosition);
            }
        }

        public static Entity GetCombatUnitForBuilding(Entity building)
        {
            m_combatUnitQuery.SetSharedComponentFilter(new EntityIndex() { Entity = building });

            if (m_combatUnitQuery.CalculateEntityCount() <= 0)
            {
                return CreateCombatUnit(building);
            }

            var units = m_combatUnitQuery.ToEntityArray(Allocator.Temp);
            var unit = units[0];
            units.Dispose();

            return unit;
        }

        public static Entity CreateCombatUnit(Entity building)
        {
            var unit = entityManager.CreateEntity();
            entityManager.AddComponentData(unit,
                new CombatUnitData()
                {
                    CombatUnitUID = UID.GetUniqueID(),
                    CurrentTileTarget = building.GetPosition().ToInt2()
                });
            entityManager.AddSharedComponentData(unit, new EntityIndex() { Entity = building });

            TestCombatUnit = unit;

            var container = i_uiSystem.GetElement<EntityHUDContainer>();
            var element = i_uiSystem.CreateUIElement<CombatUnitHUD>(new UIElementFactoryParams()
            {
                Entity = unit,
                Parent = container.Transform
            });
            container.SetEntityHUD(unit, element);

            return unit;
        }
    }
}