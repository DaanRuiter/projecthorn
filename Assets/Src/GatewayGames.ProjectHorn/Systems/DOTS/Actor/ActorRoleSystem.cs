using Gateway.ProjectHorn.Components.UI.EntityHUD;
using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Services.Serialization;
using Gateway.ProjectHorn.Services.Serialization.Definitions.Building;
using Gateway.ProjectHorn.Services.Serialization.Definitions.Item;
using Gateway.ProjectHorn.Systems.Actor;
using Gateway.ProjectHorn.Systems.Actor.TaskMaster;
using Gateway.ProjectHorn.Systems.Building;
using Gateway.ProjectHorn.Systems.DOTS.Combat;
using Gateway.ProjectHorn.Systems.DOTS.Common;
using Gateway.ProjectHorn.Systems.DOTS.Item;
using Gateway.ProjectHorn.Systems.Entities.Components.Actor;
using Gateway.ProjectHorn.Systems.Entities.Components.Building;
using Gateway.ProjectHorn.Systems.Entities.Components.Combat;
using Gateway.ProjectHorn.Systems.Terrain;
using Gateway.ProjectHorn.Systems.UI;
using Gateway.ProjectHorn.Util;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;

/**
 * 17/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.DOTS.Actor
{
    public class ActorRoleSystem : BaseDOTSSystem
    {
        protected override float updateInterval => .15f;

        [InjectService] private static DefinitionService i_definitionService;
        [InjectService] private static TerrainSystem i_terrainSystem;
        [InjectService] private static UISystem i_uiSystem;

        private static EntityQuery m_buildingQuery;

        protected override void StartSystem()
        {
            m_buildingQuery = GetEntityQuery(typeof(BuildingBaseData), typeof(FactionIndex));
        }

        protected override JobHandle UpdateSystem(JobHandle inputDeps, SystemTimeInfo timeInfo)
        {
            m_buildingQuery.SetSharedComponentFilter(new FactionIndex() { FactionUID = FactionIndex.PlayerFactionUID });
            var buildingEntities = m_buildingQuery.ToEntityArray(Allocator.TempJob);
            var buildingData = m_buildingQuery.ToComponentDataArray<BuildingBaseData>(Allocator.TempJob);

            int buildingCount = buildingData.Length;
            var buildingPositions = new NativeArray<float2>(buildingCount, Allocator.TempJob);
            var actorCounts = new NativeHashMap<int, int>(buildingCount, Allocator.TempJob);
            for (int i = 0; i < buildingPositions.Length; i++)
            {
                buildingPositions[i] = GetEntityPosition(buildingEntities[i]).ToFloat2();
            }

            Entities.WithSharedComponentFilter(new ActorRoleData()
                {
                    Role = ActorRole.None
                })
                .WithStructuralChanges()
                .ForEach((ActorRoleData roleData, in Entity entity) =>
                {
                    switch (roleData.Role)
                    {
                        case ActorRole.None:
                            for (int i = 0; i < buildingData.Length; i++)
                            {
                                var building = buildingData[i];
                                var role = GetBuildingActorRole(building);
                                int actorCount = building.ActorCount;

                                if (!actorCounts.ContainsKey(i))
                                {
                                    actorCounts.Add(i, actorCount);
                                }
                                else
                                {
                                    actorCount = actorCounts[i];
                                }

                                if (actorCount < building.ActorLimit)
                                {
                                    switch (role)
                                    {
                                        case ActorRole.Worker:
                                            MakeActorWorker(entity, building, buildingPositions[i]);
                                            break;
                                        case ActorRole.Producer:
                                            MakeActorProducer(entity, building, buildingPositions[i]);
                                            break;
                                        case ActorRole.Soldier:
                                            MakeActorSoldier(entity, building, buildingPositions[i]);
                                            break;
                                        case ActorRole.Builder:
                                            MakeActorBuilder(entity, building, buildingPositions[i]);
                                            break;
                                    }

                                    actorCounts[i] += 1;
                                    return;
                                }
                            }

                            break;
                    }
                }).Run();

            int j = 0;
            foreach (var workerCount in actorCounts)
            {
                var building = buildingData[j];
                var buildingEntity = buildingEntities[j];

                if (building.ActorCount < workerCount.Value)
                {
                    building.ActorCount = workerCount.Value;
                    entityManager.SetComponentData(buildingEntity, building);
                }

                j++;
            }

            buildingEntities.Dispose();
            buildingData.Dispose();
            buildingPositions.Dispose();
            actorCounts.Dispose();

            return inputDeps;
        }

        protected override void StopSystem()
        {
        }

        public static ActorRole GetBuildingActorRole(BuildingBaseData building)
        {
            return i_definitionService.GetDefinitionByUID<BuildingDef>(building.DefinitionUID).ActorRole
                .AsEnum<ActorRole>();
        }

        // public static bool BuildingNeedsWorker(BuildingBase building)
        // {
        //     var buildingDef = i_definitionService.GetDefinitionByUID<BuildingDef>(building.DefinitionUID);
        //     return i_definitionService.GetDefinition<ItemDef>(buildingDef.Produces) != null;
        // }

        public static void MakeActorWorker(Entity entity, BuildingBaseData building, float2 buildingPosition)
        {
            var buildingDef = i_definitionService.GetDefinitionByUID<BuildingDef>(building.DefinitionUID);
            var producingItem = i_definitionService.GetDefinition<ItemDef>(buildingDef.Produces);

            entityManager.SetSharedComponentData(entity,
                new ActorRoleData()
                {
                    Role = ActorRole.Worker
                });

            entityManager.AddComponentData(entity,
                new ActorLocalWorkData()
                {
                    State = ActorWorkState.Waiting,
                    WorkStationPosition = buildingPosition,
                    District = building.District,
                    NextWorkTimeStamp = cachedRealTime,
                    WorkYieldItemUID = producingItem.UID
                });

            // Create worker HUD
            var hudContainer = i_uiSystem.GetElement<EntityHUDContainer>();
            var display = hudContainer.CreateItemStackHUDElement(entity);
            display.SetItem(producingItem, ItemStackData.MaxCarryItemCount, 0);
            hudContainer.SetEntityHUD(entity, display);
        }

        public static void MakeActorProducer(Entity entity, BuildingBaseData building, float2 buildingPosition)
        {
            var buildingDef = i_definitionService.GetDefinitionByUID<BuildingDef>(building.DefinitionUID);
            var productions = buildingDef.Production;
            var intake = productions.Intake[0];
            var intakeDef = intake.GetItemDef(i_definitionService);
            var product = productions.Product[0];
            var productDef = product.GetItemDef(i_definitionService);

            entityManager.SetSharedComponentData(entity,
                new ActorRoleData()
                {
                    Role = ActorRole.Producer
                });

            entityManager.AddComponentData(entity,
                new ActorLocalProduceData()
                {
                    State = ActorProductionState.Waiting,
                    WorkStationPosition = buildingPosition,
                    District = building.District,
                    NextProduceTimeStamp = cachedRealTime,
                    IntakeItemUID = intakeDef.UID,
                    IntakeItemCount = intake.Amount,
                    ProductionItemUID = productDef.UID,
                    ProductionItemCount = product.Amount,
                    ProductionDuration = productions.Duration
                });
        }

        public static void MakeActorSoldier(Entity actor, BuildingBaseData building, float2 buildingPosition)
        {
            var actorData = entityManager.GetComponentData<ActorBaseData>(actor).ActorData;
            actorData.Workplace = building.Entity;
            actorData.WorkplacePosition = building.Entity.GetPosition();

            var combatUnit = LocalCombatSystem.GetCombatUnitForBuilding(building.Entity);

            entityManager.SetSharedComponentData(actor,
                new ActorRoleData()
                {
                    Role = ActorRole.Worker
                });

            entityManager.AddComponentData(actor,
                new ActorLocalSoldierData()
                {
                    ActorData = actorData,
                    Damage = 5,
                    State = ActorSoldierState.Recruiting,
                    HitPoints = 100,
                    HitPointsCap = 100
                });

            entityManager.AddSharedComponentData(actor,
                new CombatUnitIndex()
                {
                    CombatUnitEntity = combatUnit
                });
        }

        public static void MakeActorBuilder(Entity actor, BuildingBaseData building, float2 buildingPosition)
        {
            var actorData = entityManager.GetComponentData<ActorBaseData>(actor);
            actorData.ActorData.Workplace = building.Entity;
            actorData.ActorData.WorkplacePosition = building.Entity.GetPosition();
            entityManager.SetComponentData(actor, actorData);

            entityManager.SetSharedComponentData(actor,
                new ActorRoleData()
                {
                    Role = ActorRole.Builder
                });

            entityManager.AddComponentData(actor, new ActorLocalBuilderData());

            var taskSystem = Main.GetOrCreateSystem<ActorTaskSystem>();
            var taskMaster = taskSystem.GetTaskMaster<ActorMoveTaskMaster>();
            float halfNodeSize = District.HalfNodeLocalSize;

            taskSystem.EnqueueTask(taskMaster.CreateMoveTask(actor, buildingPosition.Add(halfNodeSize)));
        }
    }
}