﻿using Gateway.ProjectHorn.Systems.Entities.Components.Actor;
using Gateway.ProjectHorn.Util;
using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Random = UnityEngine.Random;

/**
 * 31/01/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.DOTS.Actor
{
    [BurstCompile]
    public class ActorLocalMoveSystem : BaseDOTSSystem
    {
        private EntityQuery spawnerQuery;

        protected override void StartSystem()
        {
            spawnerQuery = GetEntityQuery(typeof(ActorLocalMoveData));
        }

        protected override JobHandle UpdateSystem(JobHandle inputDeps, SystemTimeInfo timeInfo)
        {
            uint baseRandomSeed = (uint)Random.Range(1, 500);

            var moveActors = Entities
                .WithStoreEntityQueryInField(ref spawnerQuery)
                .ForEach((int entityInQueryIndex, ref Translation translation, ref ActorLocalMoveData moveData) =>
                {
                    bool reachedTarget = moveData.IsAtTarget(translation);
                    bool canSeek = moveData.CanSeek(timeInfo.CurrentTime);
                    var random = Unity.Mathematics.Random.CreateFromIndex(baseRandomSeed + (uint)entityInQueryIndex);

                    moveData.SetReachedTarget(reachedTarget);

                    if (reachedTarget)
                    {
                        if (canSeek)
                        {
                            moveData.SetTarget(moveData.TilePosition.RandomLocalTilePos(random));
                            moveData.NextSeekTimestamp = timeInfo.CurrentTime + 2f;
                        }
                    }
                    else
                    {
                        float3 targetPos = Vector3.MoveTowards(translation.Value,
                            moveData.CurrentTarget.ToVector3(),
                            moveData.MoveSpeed * timeInfo.DeltaTime * timeInfo.TimeScale);
                        targetPos.z = translation.Value.z;

                        translation.Value = targetPos;
                    }
                }).Schedule(inputDeps);

            moveActors.Complete();

            return moveActors;
        }

        protected override void StopSystem()
        {
        }
    }
}