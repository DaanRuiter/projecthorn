﻿using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Systems.DOTS.Common;
using Gateway.ProjectHorn.Systems.Entities.Components;
using Gateway.ProjectHorn.Systems.Entities.Components.Actor;
using Gateway.ProjectHorn.Systems.Terrain;
using Gateway.ProjectHorn.Systems.UI;
using Gateway.ProjectHorn.Util;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;

/**
 * 03/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.DOTS.Actor
{
    // [BurstCompile]
    public class ActorLocalSoldierSystem : BaseDOTSSystem
    {
        protected override float updateInterval => .1f;

        private EntityQuery m_soldierQuery;
        private EntityQuery m_tileQuery;

        [InjectService] private TerrainSystem i_terrainSystem;
        [InjectService] private UISystem i_uiSystem;

        protected override void StartSystem()
        {
            m_soldierQuery = GetEntityQuery(typeof(ActorLocalSoldierData),
                typeof(ActorLocalWorkData),
                typeof(ActorRoleData));
            m_tileQuery = GetEntityQuery(typeof(TileData), typeof(FactionIndex));
        }

        protected override JobHandle UpdateSystem(JobHandle inputDeps, SystemTimeInfo timeInfo)
        {
            m_tileQuery.SetSharedComponentFilter(new FactionIndex() { FactionUID = FactionIndex.PlayerFactionUID });
            m_soldierQuery.SetSharedComponentFilter(new ActorRoleData()
            {
                Role = ActorRole.Soldier
            });

            var tileEntities = m_tileQuery.ToEntityArray(Allocator.TempJob);
            var tilePositions = new NativeArray<int2>(tileEntities.Length, Allocator.TempJob);

            for (int i = 0; i < tilePositions.Length; i++)
            {
                // cache beforehand
                tilePositions[i] = entityManager.GetComponentData<TileData>(tileEntities[i]).GetPosition();
            }

            // Randomize order of tiles to check // todo: use distance to find closest
            tilePositions.Randomize();

            Entities
                .WithoutBurst()
                .ForEach((int entityInQueryIndex,
                    ref ActorLocalSoldierData soldierData,
                    ref ActorLocalMoveData moveData,
                    in Entity entity) =>
                {
                    switch (soldierData.State)
                    {
                        case ActorSoldierState.Recruiting:
                            soldierData.State = ActorSoldierState.Moving;
                            moveData.SetTarget(soldierData.ActorData.WorkplacePosition);
                            break;
                        case ActorSoldierState.Moving:
                            if (moveData.ReachedTarget)
                            {
                                soldierData.State = ActorSoldierState.Waiting;
                            }

                            break;
                        case ActorSoldierState.Waiting:
                            break;
                    }
                }).Run();

            // workJob.Complete();

            tileEntities.Dispose();
            tilePositions.Dispose();

            return inputDeps;
        }

        protected override void StopSystem()
        {
        }
    }
}