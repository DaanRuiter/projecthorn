

using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Systems.DOTS.Item;
using Gateway.ProjectHorn.Systems.Entities.Components.Actor;
using Gateway.ProjectHorn.Systems.Terrain;
using Gateway.ProjectHorn.Util;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;

/**
 * 24/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.DOTS.Actor
{
    public class ActorLocalProductionSystem : BaseDOTSSystem
    {
        private struct ItemCount
        {
            public uint UID;
            public int Count;

            public override string ToString()
            {
                return string.Format("ItemCount [{0}] {1}x", UID, Count);
            }
        }

        protected override float updateInterval => .1f;

        [InjectService] private TerrainSystem i_terrainSystem;

        private EntityQuery m_workerQuery;

        protected override void StartSystem()
        {
            m_workerQuery = GetEntityQuery(typeof(ActorLocalMoveData),
                typeof(ActorLocalProduceData),
                typeof(ActorRoleData));
        }

        private JobHandle _jobHandle;

        protected override JobHandle UpdateSystem(JobHandle inputDeps, SystemTimeInfo timeInfo)
        {
            if (!LocalItemSystem.Ready) return inputDeps;

            float currentTime = UnityEngine.Time.realtimeSinceStartup;
            m_workerQuery.SetSharedComponentFilter(new ActorRoleData()
            {
                Role = ActorRole.Producer
            });

            var workers = m_workerQuery.ToEntityArray(Allocator.TempJob);
            var districtInventoryData =
                new NativeMultiHashMap<Entity, ItemCount>(workers.Length, Allocator.TempJob);
            var workerCarryData = LocalItemSystem.GetActorCarryData(workers, Allocator.TempJob);

            for (int i = 0; i < workers.Length; i++)
            {
                var districtPos = entityManager.GetComponentData<ActorLocalProduceData>(workers[i]).WorkStationPosition;
                var inventory = i_terrainSystem.Tiles.GetTile(districtPos).District.GetInventoryData();

                foreach (var itemCount in inventory)
                {
                    if (itemCount.Value > 0)
                    {
                        districtInventoryData.Add(workers[i],
                            new ItemCount()
                            {
                                UID = itemCount.Key,
                                Count = itemCount.Value
                            });
                    }
                }
            }

            // if (_jobHandle.IsCompleted)
            // {
            Entities.WithStructuralChanges().ForEach((int entityInQueryIndex,
                ref ActorLocalProduceData produceData,
                ref ActorLocalMoveData moveData,
                in Entity entity) =>
            {
                switch (produceData.State)
                {
                    case ActorProductionState.Waiting:
                        moveData.SetTarget(produceData.WorkStationPosition);
                        produceData.State = ActorProductionState.Moving;
                        break;
                    case ActorProductionState.Moving:
                        if (moveData.ReachedTarget)
                        {
                            produceData.State = ActorProductionState.Producing;
                            produceData.NextProduceTimeStamp = currentTime + produceData.ProductionDuration;
                        }

                        break;
                    case ActorProductionState.Producing:

                        if (currentTime >= produceData.NextProduceTimeStamp)
                        {
                            bool intakePresent = produceData.IntakeItemCount == 0;

                            if (!intakePresent) // Try find intake
                            {
                                if (!districtInventoryData.TryGetFirstValue(entity, out var count, out var iterator))
                                {
                                    return;
                                }

                                do
                                {
                                    if (count.UID == produceData.IntakeItemUID &&
                                        count.Count >= produceData.IntakeItemCount)
                                    {
                                        intakePresent = true;

                                        LocalItemSystem.TransferItems(produceData.WorkDistrict,
                                            entity,
                                            produceData.IntakeItemUID,
                                            produceData.IntakeItemCount);

                                        // Update inventory data
                                        districtInventoryData.SetValue(new ItemCount()
                                            {
                                                Count = count.Count -= produceData.IntakeItemCount,
                                                UID = count.UID
                                            },
                                            iterator);
                                        break;
                                    }
                                } while (districtInventoryData.TryGetNextValue(out count, ref iterator));
                            }

                            if (intakePresent) // Found intake, produce
                            {
                                var carryData = workerCarryData.GetCarryData(entity, produceData.ProductionItemUID);

                                if (carryData.ItemCount.CanTake(produceData.ProductionItemCount))
                                {
                                    produceData.NextProduceTimeStamp = currentTime + produceData.ProductionDuration;
                                    produceData.PerformProduction(entity);
                                }
                                else
                                {
                                    produceData.State = ActorProductionState.Unloading;
                                }
                            }
                        }

                        break;
                    case ActorProductionState.Unloading:
                        var producerResultData = workerCarryData.GetCarryData(entity, produceData.ProductionItemUID);

                        LocalItemSystem.TransferItems(entity,
                            produceData.District,
                            producerResultData.ItemUID,
                            producerResultData.ItemCount.SignedCount);

                        produceData.State = ActorProductionState.Waiting;
                        produceData.NextProduceTimeStamp = currentTime + 2f;
                        break;
                }
            }).Run();

            // }

            workerCarryData.Dispose();
            workers.Dispose();
            districtInventoryData.Dispose();

            return inputDeps;
        }

        protected override void StopSystem()
        {
        }
    }
}