﻿using Gateway.ProjectHorn.Components.UI.EntityHUD;
using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Systems.Building;
using Gateway.ProjectHorn.Systems.DOTS.Common;
using Gateway.ProjectHorn.Systems.DOTS.Item;
using Gateway.ProjectHorn.Systems.Entities.Components;
using Gateway.ProjectHorn.Systems.Entities.Components.Actor;
using Gateway.ProjectHorn.Systems.Terrain;
using Gateway.ProjectHorn.Systems.UI;
using Gateway.ProjectHorn.Util;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;

/**
 * 03/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.DOTS.Actor
{
    // [BurstCompile]
    public class ActorLocalWorkSystem : BaseDOTSSystem
    {
        protected override float updateInterval => .1f;

        private EntityQuery m_workerQuery;
        private EntityQuery m_tileQuery;

        [InjectService] private TerrainSystem i_terrainSystem;
        [InjectService] private UISystem i_uiSystem;

        protected override void StartSystem()
        {
            m_workerQuery = GetEntityQuery(typeof(ActorLocalMoveData),
                typeof(ActorLocalWorkData),
                typeof(ActorRoleData));
            m_tileQuery = GetEntityQuery(typeof(TileData), typeof(FactionIndex));
        }

        protected override JobHandle UpdateSystem(JobHandle inputDeps, SystemTimeInfo timeInfo)
        {
            if (!LocalItemSystem.Ready) return inputDeps;

            float currentTime = UnityEngine.Time.realtimeSinceStartup;

            m_tileQuery.SetSharedComponentFilter(new FactionIndex() { FactionUID = FactionIndex.PlayerFactionUID });
            m_workerQuery.SetSharedComponentFilter(new ActorRoleData()
            {
                Role = ActorRole.Worker
            });

            var tileEntities = m_tileQuery.ToEntityArray(Allocator.TempJob);
            var tilePositions = new NativeArray<int2>(tileEntities.Length, Allocator.TempJob);
            var nodeProductions =
                new NativeMultiHashMap<int2, District.NodeItemYield>(tileEntities.Length, Allocator.TempJob);
            var workers = m_workerQuery.ToEntityArray(Allocator.TempJob);
            var workerCarryData = LocalItemSystem.GetActorCarryData(workers, Allocator.TempJob);
            var actorItemCounts = new NativeHashMap<Entity, int2>(workers.Length, Allocator.TempJob);

            for (int i = 0; i < tilePositions.Length; i++)
            {
                // cache beforehand
                tilePositions[i] = entityManager.GetComponentData<TileData>(tileEntities[i]).GetPosition();
            }

            // Randomize order of tiles to check // todo: use distance to find closest
            tilePositions.Randomize();

            for (int i = 0; i < tilePositions.Length; i++)
            {
                var tilePos = tilePositions[i] = tilePositions[i];
                var tile = i_terrainSystem.Tiles.GetTile(tilePos);

                if (tile.HasDistrict)
                {
                    var yields = tile.District.GetNodeYieldData();
                    if (yields != null)
                    {
                        foreach (var yield in yields)
                        {
                            nodeProductions.Add(tilePos, yield);
                        }
                    }
                }
            }

            workers.Dispose();

            Entities
                .WithStructuralChanges()
                .WithoutBurst()
                .ForEach((int entityInQueryIndex,
                    ref ActorLocalWorkData workData,
                    ref ActorLocalMoveData moveData,
                    in Entity entity) =>
                {
                    switch (workData.State)
                    {
                        case ActorWorkState.Waiting:
                            if (currentTime >= workData.NextWorkTimeStamp)
                            {
                                workData.State = ActorWorkState.Seeking;
                            }

                            break;
                        case ActorWorkState.Seeking:

                            // Search for tile that produces preferred item
                            for (int i = 0; i < tilePositions.Length; i++)
                            {
                                var tileYields = nodeProductions.GetValuesForKey(tilePositions[i]);
                                while (tileYields.MoveNext())
                                {
                                    if (tileYields.Current.YieldData.ItemUID == workData.WorkYieldItemUID)
                                    {
                                        moveData.SetTarget(tileYields.Current.WorldPosition);
                                        moveData.TilePosition = tilePositions[i];
                                        workData.State = ActorWorkState.Moving;
                                        return;
                                    }
                                }
                            }

                            // if (!foundTile)
                            // {
                            //     var random = new Unity.Mathematics.Random(baseRandomSeed + (uint)entityInQueryIndex);
                            //     int randomIndex = random.NextInt(0, tilePositions.Length);
                            //     moveData.CurrentTarget = tilePositions[randomIndex];
                            //     workData.WorkState = ActorWorkState.Moving;
                            // }

                            break;
                        case ActorWorkState.Moving:
                            if (moveData.ReachedTarget)
                            {
                                if (workerCarryData.CarriesFullStack(entity, workData.WorkYieldItemUID))
                                {
                                    workData.State = ActorWorkState.Unloading;
                                }
                                else
                                {
                                    var yields = nodeProductions.GetValuesForKey(moveData.TilePosition);

                                    while (yields.MoveNext())
                                    {
                                        if (yields.Current.YieldData.ItemUID == workData.WorkYieldItemUID)
                                        {
                                            workData.State = ActorWorkState.Working;
                                            workData.WorkDuration = yields.Current.YieldData.WorkCost;
                                            workData.WorkYieldCount = yields.Current.YieldData.WorkYieldCount;
                                            workData.NextWorkTimeStamp =
                                                currentTime + (workData.WorkDuration / timeInfo.TimeScale);
                                            return;
                                        }
                                    }

                                    //found no yield
                                }
                            }

                            break;

                        case ActorWorkState.Working:
                            if (workData.NextWorkTimeStamp <= currentTime)
                            {
                                int stackSpace = workerCarryData.GetStackSpaceLeft(entity, workData.WorkYieldItemUID);

                                if (stackSpace >= workData.WorkYieldCount || stackSpace == -1) // Space left or no stack
                                {
                                    var carryData =
                                        workerCarryData.GetCarryData(entity, workData.WorkYieldItemUID);
                                    int yield = carryData.ItemCount.SignedCount + workData.WorkYieldCount;

                                    workData.NextWorkTimeStamp =
                                        currentTime + (workData.WorkDuration / timeInfo.TimeScale);

                                    actorItemCounts[entity] = new int2(yield, carryData.ItemCount.SignedCap);

                                    LocalItemSystem.CreateItemStackUpdate(workData.WorkYieldItemUID,
                                        workData.WorkYieldCount,
                                        entity);
                                }
                                else // Full stack
                                {
                                    moveData.SetTarget(workData.WorkStationPosition);
                                    workData.State = ActorWorkState.Moving;
                                }
                            }

                            break;
                        case ActorWorkState.Unloading:
                            var workResultData = workerCarryData.GetCarryData(entity, workData.WorkYieldItemUID);
                            LocalItemSystem.TransferItems(entity,
                                workData.District,
                                workResultData.ItemUID,
                                workResultData.ItemCount.SignedCount);
                            workData.State = ActorWorkState.Waiting;
                            workData.NextWorkTimeStamp = currentTime + 2f;
                            actorItemCounts[entity] = new int2(0, workResultData.ItemCount.SignedCap);
                            break;
                        case ActorWorkState.Loading:

                            break;
                    }
                }).Run();

            // workJob.Complete();

            var entityHUDContainer = i_uiSystem.GetElement<EntityHUDContainer>();
            if (entityHUDContainer != null)
            {
                foreach (var itemCount in actorItemCounts)
                {
                    entityHUDContainer.GetEntityHUD<ItemStackHUDElement>(itemCount.Key)
                        .SetItemCount(itemCount.Value.x, itemCount.Value.y);
                }
            }

            workerCarryData.Dispose();
            tileEntities.Dispose();
            tilePositions.Dispose();
            nodeProductions.Dispose();
            actorItemCounts.Dispose();

            return inputDeps;
        }

        protected override void StopSystem()
        {
        }
    }
}