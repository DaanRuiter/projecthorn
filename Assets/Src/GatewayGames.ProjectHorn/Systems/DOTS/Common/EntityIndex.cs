

using Unity.Entities;

/**
 * 08/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.DOTS.Common
{
    public struct EntityIndex : ISharedComponentData
    {
        public Entity Entity;
    }
}