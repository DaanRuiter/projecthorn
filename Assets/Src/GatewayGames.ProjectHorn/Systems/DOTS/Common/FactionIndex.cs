﻿using Unity.Entities;

/**
 * 03/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.DOTS.Common
{
    public struct FactionIndex : ISharedComponentData
    {
        public const ushort PlayerFactionUID = 0;
        public const ushort EnemyFactionUID = 1;

        public ushort FactionUID;
    }
}