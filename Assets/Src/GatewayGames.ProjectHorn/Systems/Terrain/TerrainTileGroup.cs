﻿using System.Collections.Generic;
using Gateway.ProjectHorn.Services.Serialization.Definitions;
using Gateway.ProjectHorn.Systems.Terrain.Tile;
using Gateway.ProjectHorn.Util;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Gateway.ProjectHorn.Systems.Terrain
{
    public class TerrainTileGroup
    {
        public Vector2Int Dimensions { get; private set; }

        public TerrainTile this[int x, int y] => GetTile(x, y);

        public TerrainTile this[int2 pos] => GetTile(pos);

        public TerrainTile this[Vector2Int pos] => GetTile(pos);

        public Tilemap Tilemap => _tilemap;

        public int Length => Dimensions.x * Dimensions.y;

        private TerrainTile[,] _tiles;
        private Tilemap _tilemap;

        public TerrainTileGroup(Tilemap tilemap, TerrainTile[,] tiles)
        {
            _tiles = tiles;
            _tilemap = tilemap;
            Dimensions = new Vector2Int(tiles.GetLength(0), tiles.GetLength(1));
        }

        public void Dispose()
        {
        }

        public TerrainTile GetTile(float2 pos, bool @unsafe = false) => GetTile(pos.ToInt2(), @unsafe);

        public TerrainTile GetTile(int2 pos, bool @unsafe = false) => GetTile(pos.x, pos.y, @unsafe);

        public TerrainTile GetTile(Vector2Int pos, bool @unsafe = false) => GetTile(pos.x, pos.y, @unsafe);

        public TerrainTile GetTile(int x, int y, bool @unsafe = false)
        {
            if (!@unsafe && !IsInRange(x, y))
            {
                return null;
            }

            return _tiles[x, y];
        }

        public void SetTile(Vector2Int position, TileDef definition) => SetTile(position.x, position.y, definition);

        public void SetTile(int x, int y, TileDef definition)
        {
            if (!IsInRange(x, y))
            {
                return;
            }

            SetTile(_tiles[x, y], definition);
        }

        public void SetTile(TerrainTile tile, TileDef definition, bool refreshTileMap = true)
        {
            var tilemapPos = tile.GetPosition().ToVector3Int();
            _tilemap.SetTile(tilemapPos, definition.Tile);

            tile.OnTileChange(definition);
            UpdateTileOccupation(tile);

            if (refreshTileMap)
            {
                _tilemap.RefreshTile(tilemapPos);
            }
        }

        public void SetTile(int x, int y, TileDef definition, bool refreshTileMap = true)
        {
            var tilemapPos = new Vector3Int(x, y, 0);
            var tile = GetTile(x, y);
            _tilemap.SetTile(tilemapPos, definition.Tile);

            tile.OnTileChange(definition);
            UpdateTileOccupation(tile);

            if (refreshTileMap)
            {
                _tilemap.RefreshTile(tilemapPos);
            }
        }

        public void RefreshAllTiles()
        {
            _tilemap.RefreshAllTiles();
        }

        public void UpdateTileOccupation(TerrainTile tile)
        {
            // Vector3Int tilemapPos = (Vector3Int)tile.GetPosition();
            // int traversableIndex = tilemapPos.y * TerrainSystem.Tiles.Dimensions.x + tilemapPos.x;
            // _tileWalkCosts[traversableIndex] = tile.Occupation.IsOccupied ? byte.MaxValue : byte.MinValue; 
        }

        public void SetAllTiles(TileDef definition)
        {
            for (uint x = 0; x < Dimensions.x; x++)
            {
                for (uint y = 0; y < Dimensions.y; y++)
                {
                    SetTile(_tiles[x, y], definition, false);
                }
            }

            _tilemap.RefreshAllTiles();
        }

        public TerrainTile[] GetRange(int2 center, int radius) => GetRange(center, radius, radius);

        public TerrainTile[] GetRange(int2 center, int width, int height)
        {
            float2 halfSize = new float2(width, height) / 2f;
            return GetRange((center - halfSize).ToInt2(), (center + halfSize).ToInt2());
        }

        public TerrainTile[] GetRange(int2 from, int2 to)
        {
            List<TerrainTile> result = new List<TerrainTile>();

            int width = math.abs(to.x - from.x) + 1;
            int height = math.abs(to.y - from.y) + 1;

            for (int x = 1; x < width; x++)
            {
                for (int y = 1; y < height; y++)
                {
                    int tileX = from.x + x;
                    int tileY = from.y + y;
                    if (IsInRange(tileX, tileY))
                    {
                        result.Add(GetTile(tileX, tileY));
                    }
                }
            }

            return result.ToArray();
        }

        // public NativeArray<byte> GetTileWalkCosts()
        // {
        //     return _tileWalkCosts;
        // }

        public bool IsInRange(int x, int y) => (x >= 0 && x < Dimensions.x) && (y >= 0 && y < Dimensions.y);

        public bool IsInRange(Vector2Int position) => IsInRange(position.x, position.y);
    }
}