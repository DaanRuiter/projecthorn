

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Services.Graphics;
using Gateway.ProjectHorn.Systems.DOTS.Common;
using Gateway.ProjectHorn.Systems.Entities.Components;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

/**
 * 20/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Terrain.Tile
{
    public class TileGroup
    {
        private Dictionary<int2, TerrainTile> m_tiles;
        private Dictionary<int2, int2[]> m_neighbors; //todo: use single struct

        private ushort m_factionUID;
        private MeshFilter m_territoryView;

        private static EntityManager entityManager => World.DefaultGameObjectInjectionWorld.EntityManager;

        public TileGroup()
        {
            m_tiles = new Dictionary<int2, TerrainTile>();
            m_neighbors = new Dictionary<int2, int2[]>();
        }

        public TileGroup(ushort FactionUID) : this()
        {
            m_territoryView = Object.Instantiate(Resources.Load<GameObject>("prefabs/RealmRangePreview"))
                .GetComponent<MeshFilter>();
            SetFaction(FactionUID);
        }

        public void AddTile(int2 position, TerrainTile tile, bool updateTerritoryMesh = true)
        {
            if (!m_tiles.ContainsKey(position))
            {
                AddTileUnsafe(position, tile, updateTerritoryMesh);
            }
        }

        public void AddTileUnsafe(int2 position, TerrainTile tile, bool updateTerritoryMesh = true)
        {
            m_tiles.Add(position, tile);

            if (updateTerritoryMesh)
            {
                UpdateTerritoryView();
            }
        }

        public void SetFaction(ushort factionUID)
        {
            m_factionUID = factionUID;
            m_tiles.Clear();

            var terrainSystem = ServiceInjector.GetService<TerrainSystem>();
            var tileQuery = entityManager.CreateEntityQuery(typeof(TileData), typeof(FactionIndex));
            tileQuery.SetSharedComponentFilter(new FactionIndex() { FactionUID = m_factionUID });
            var tileEntities = tileQuery.ToEntityArray(Allocator.Temp);

            for (int i = 0; i < tileEntities.Length; i++)
            {
                var tilePos = entityManager.GetComponentData<TileData>(tileEntities[i]).GetPosition();
                AddTileUnsafe(tilePos, terrainSystem.Tiles.GetTile(tilePos), false);
            }

            tileEntities.Dispose();

            UpdateTerritoryView();
        }

        public ReadOnlyDictionary<int2, TerrainTile> GetTiles() => new ReadOnlyDictionary<int2, TerrainTile>(m_tiles);

        private void UpdateTerritoryView()
        {
            var mesh = m_territoryView.mesh;
            if (mesh != null)
            {
                Object.Destroy(mesh);
            }

            mesh = ServiceInjector.GetService<GraphicsService>().CreateSingleMeshForTiles(m_tiles.Values.ToArray());
            m_territoryView.mesh = mesh;
        }
    }
}