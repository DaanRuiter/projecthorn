﻿using Gateway.ProjectHorn.Services.Serialization.Definitions;
using Gateway.ProjectHorn.Systems.Building;
using Gateway.ProjectHorn.Systems.Entities.Components;
using Unity.Entities;
using UnityEngine;

namespace Gateway.ProjectHorn.Systems.Terrain.Tile
{
    public class TerrainTile
    {
        private static EntityManager entityManager => World.DefaultGameObjectInjectionWorld.EntityManager;

        public Entity Entity { get; }

        public District District { get; internal set; }

        public TileDef Definition { get; private set; }

        public TileOccupation Occupation => Definition.Occupation;

        public bool HasDistrict => District != null;

        public bool IsOccupied;

        public TerrainTile(Entity entity, TileDef definition)
        {
            Entity = entity;
            Definition = definition;
        }

        public Vector2Int GetPosition()
        {
            TileData tileData = entityManager.GetComponentData<TileData>(Entity);
            return new Vector2Int(tileData.X, tileData.Y);
        }

        public virtual void OnTileChange(TileDef definition)
        {
            Definition = definition;
        }
    }
}