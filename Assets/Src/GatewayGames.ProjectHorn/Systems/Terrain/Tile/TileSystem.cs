﻿

//public class TileSystem : JobComponentSystem
//{
//    protected override JobHandle OnUpdate(JobHandle inputDeps)
//    {
//        TileRotationJob rotationJob = new TileRotationJob { ElapsedTime = (float)Time.ElapsedTime };
//        return rotationJob.Schedule(this, inputDeps);
//    }

//    struct TileRotationJob : IJobForEach<Rotation, TileData>
//    {
//        public float ElapsedTime;

//        public void Execute(ref Rotation rotation, ref TileData tileData)
//        {
//            tileData.Ticks += 1;
//            rotation.Value = new Quaternion(Mathf.Sin(ElapsedTime), Mathf.Tan(ElapsedTime), 0, 1);
//        }
//    }
//}
