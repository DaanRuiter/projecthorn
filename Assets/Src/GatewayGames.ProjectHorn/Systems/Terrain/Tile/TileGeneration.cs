﻿using Gateway.ProjectHorn.Services.Serialization.Definitions;
using UnityEngine;
using Unity.Entities;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;
using TileData = Gateway.ProjectHorn.Systems.Entities.Components.TileData;

namespace Gateway.ProjectHorn.Systems.Terrain.Tile
{
    public static class TileGeneration
    {
        private static EntityManager m_entityManager;
        private static EntityArchetype m_tileArchType;

        public static TerrainTileGroup Generate(TerrainDef terrainDef,
            Tilemap tilemap,
            TileDef defaultTile,
            TileDef floraTile,
            TileDef mountainTile)
        {
            m_entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            m_tileArchType = m_entityManager.CreateArchetype(typeof(TileData));
            tilemap.origin = Vector3Int.zero;
            tilemap.size = terrainDef.WorldSize;

            var tiles = SpawnTileEntities(terrainDef.WorldSize.x, terrainDef.WorldSize.y, defaultTile);
            int width = tiles.GetLength(0);
            int height = tiles.GetLength(1);
            var perlinNoise = GeneratePerlinMap(width,
                height,
                Random.Range(0f, 999999f), //Seed
                new[] { 15.5f, 5.5f, 36.5f, 150f }, //scale
                new[] { 0f, 250f, 125f, 0f }, //seed addition
                new[] { 1f, 2.8f, 0.25f, 0.25f }); //strengths

            var tileGroup = new TerrainTileGroup(tilemap, tiles);

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    float perlin = perlinNoise[x, y];
                    var tile = perlin > .35f ? perlin < .55f ? defaultTile : mountainTile : floraTile;
                    tileGroup.SetTile(tileGroup.GetTile(x, y), tile, false);
                }
            }

            tileGroup.RefreshAllTiles();

            return tileGroup;
        }

        public static TerrainTile[,] SpawnTileEntities(int width, int height, TileDef defaultTileDefinition)
        {
            TerrainTile[,] tiles = new TerrainTile[width, height];
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    Entity tile = m_entityManager.CreateEntity(m_tileArchType);
                    m_entityManager.AddComponentData(tile,
                        new TileData()
                        {
                            X = x,
                            Y = y
                        });
                    tiles[x, y] = new TerrainTile(tile, defaultTileDefinition);
                }
            }

            return tiles;
        }

        public static float[,] GeneratePerlinMap(int width,
            int height,
            float seed,
            float[] scales,
            float[] additions,
            float[] strengths)
        {
            float[,] dataMap = new float[width, height];
            float totalStrength = 0f;
            for (int i = 0; i < strengths.Length; i++)
            {
                totalStrength += strengths[i];
            }

            for (int i = 0; i < scales.Length; i++)
            {
                seed += additions[i];

                float y = 0.0F;
                while (y < height)
                {
                    float x = 0.0F;
                    while (x < width)
                    {
                        float xCoord = seed + x / width * scales[i];
                        float yCoord = seed + y / height * scales[i];
                        float sample = Mathf.PerlinNoise(xCoord, yCoord);
                        float final = sample * strengths[i];
                        dataMap[(int)x, (int)y] += final;
                        x++;
                    }

                    y++;
                }
            }

            //bring it back to 0 - 1
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    dataMap[x, y] /= totalStrength;
                }
            }

            return dataMap;
        }
    }
}