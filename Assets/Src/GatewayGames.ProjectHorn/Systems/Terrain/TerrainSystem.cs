﻿using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Services.Serialization;
using Gateway.ProjectHorn.Services.Serialization.Definitions;
using Gateway.ProjectHorn.Systems.Building;
using Gateway.ProjectHorn.Systems.Terrain.Tile;
using Gateway.ProjectHorn.Util;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Gateway.ProjectHorn.Systems.Terrain
{
    public class TerrainSystem : SystemBase
    {
        public Tilemap MainTileMap => Tiles.Tilemap;

        public TerrainTileGroup Tiles { get; private set; }

        public TileDef[] TilesDefs { get; private set; }

        public TerrainDef Definition { get; private set; }

        [InjectService] private DefinitionService _definitions;

        internal override void OnStart()
        {
            if (!Main.IsAwake)
            {
                return;
            }

            TilesDefs = _definitions.GetDefinitions<TileDef>();
            Definition = _definitions.GetDefinition<TerrainDef>("TestTerrain");
            var defaultTile = _definitions.GetDefinition<TileDef>("TestTile");
            var floraTile = _definitions.GetDefinition<TileDef>("TestTilePlant");
            var mountainTile = _definitions.GetDefinition<TileDef>("TestTileOre");

            Tiles = TileGeneration.Generate(Definition,
                CreateTileMapGameObject(),
                defaultTile,
                floraTile,
                mountainTile);
        }

        internal override void OnUpdate()
        {
        }

        internal override void OnStop()
        {
            Tiles?.Dispose();
        }

        private Tilemap CreateTileMapGameObject()
        {
            var terrainGameObject = new GameObject("Terrain");
            terrainGameObject.AddComponent<Grid>().cellSize = new Vector3(1, 1, 0);

            var tileMapGameObject = new GameObject("Tilemap");
            tileMapGameObject.transform.SetParent(terrainGameObject.transform);

            var tileMapComponent = tileMapGameObject.AddComponent<Tilemap>();
            tileMapComponent.tileAnchor = Vector3.zero;

            var tileMapRendererComponent = tileMapGameObject.AddComponent<TilemapRenderer>();
            tileMapRendererComponent.detectChunkCullingBounds = TilemapRenderer.DetectChunkCullingBounds.Manual;

            return tileMapComponent;
        }

        // Helpers
        public TerrainTile GetTileUnderCursor()
        {
            if (!Main.EventSystem.IsPointerOverGameObject())
            {
                Vector3 mp = Input.mousePosition;
                mp.z = Mathf.Abs(Main.Camera.transform.position.z);

                Vector2Int pos = Main.Camera.ScreenToWorldPoint(mp).ToVector2Int();
                return Tiles.GetTile(pos);
            }

            return null;
        }

        public int2 GetNodePosUnderCursor()
        {
            if (!Main.EventSystem.IsPointerOverGameObject())
            {
                Vector3 mp = Input.mousePosition;
                mp.z = Mathf.Abs(Main.Camera.transform.position.z);

                var pos = Main.Camera.ScreenToWorldPoint(mp);
                var floored = new int2((int)Mathf.Floor(pos.x), (int)Mathf.Floor(pos.y));
                var local = new float2(pos.x - floored.x, pos.y - floored.y);
                return new int2((int)(local.x * District.NodeGridSize), (int)(local.y * District.NodeGridSize));
            }

            return default;
        }
    }
}