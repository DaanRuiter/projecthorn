﻿using System;
using System.Collections.Generic;
using Gateway.ProjectHorn.Services.Serialization.Definitions.District;
using Gateway.ProjectHorn.Services.Serialization.Definitions.DistrictNode;
using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Services.Serialization.Definitions;
using Gateway.ProjectHorn.Systems.DOTS.Building;
using Gateway.ProjectHorn.Systems.Terrain;
using Gateway.ProjectHorn.Systems.Terrain.Tile;
using Gateway.ProjectHorn.Util;
using Gateway.ProjectHorn.Services.Serialization;
using Gateway.ProjectHorn.Services.Serialization.Definitions.Building;
using Gateway.ProjectHorn.Services.Serialization.Definitions.DistrictNodeOverlay;
using Gateway.ProjectHorn.Systems.Building;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Rendering;
using UnityEngine;

namespace Gateway.ProjectHorn.Systems.Entities
{
    public class EntitySystem : SystemBase
    {
        public event Action<District> DistrictPlacedEvent;

        public event Action<DistrictNodeDef, DistrictNode> NodePlacedEvent;

        public event Action<BuildingDef, DistrictNode> BuildingConstructionStartedEvent;

        public event Action<BuildingDef, DistrictNode> BuildingConstructionFinishedEvent;

        [InjectService] private DefinitionService i_definitionService;
        [InjectService] private TerrainSystem i_terrainSystem;

        private Dictionary<Entity, EntityDef> m_entities;
        private EntityManager m_entityManager;
        private EntityArchetype m_entityArchType;
        private EntityArchetype m_actorArchType;

        //Std definitions
        private DistrictNodeOverlayDef _scaffoldDef;

        // Districts
        public Entity BuildDistrict(DistrictDef district, Vector2Int tilePosition) =>
            BuildDistrict(district, i_terrainSystem.Tiles.GetTile(tilePosition));

        public Entity BuildDistrict(DistrictDef district, TerrainTile tile)
        {
            var tilePos = tile.GetPosition();

            if (tile.District == null)
            {
                tile.IsOccupied = true;
                tile.District = new District();
                tile.District.Entity = PlaceEntity(district, tilePos, tilePos);

                DistrictPlacedEvent?.Invoke(tile.District);
            }

            return tile.District.Entity;
        }

        // Node - District

        public DistrictNode PlaceNodeInDistrict(DistrictNodeDef building,
            Vector2Int tilePosition,
            int2 localNodePosition) =>
            PlaceNodeInDistrict(building, i_terrainSystem.Tiles.GetTile(tilePosition), localNodePosition);

        private DistrictNode PlaceNodeInDistrict(DistrictNodeDef building, TerrainTile tile, int2 localNodePosition)
        {
            if (CanBuildAt(tile, localNodePosition))
            {
                var node = BuildEntity(building, tile, localNodePosition, false);

                building.OnEntityCreated(m_entityManager, node.Entity, node.WorldPosition, tile.GetPosition());

                NodePlacedEvent?.Invoke(building, node);

                return node;
            }

            return null;
        }

        // Building - District
        public DistrictNode PlaceBuildingInDistrict(BuildingDef building,
            Vector2Int tilePosition,
            int2 localNodePosition) =>
            PlaceBuildingInDistrict(building, i_terrainSystem.Tiles.GetTile(tilePosition), localNodePosition);

        public DistrictNode PlaceBuildingInDistrict(BuildingDef building, TerrainTile tile, int2 localNodePosition)
        {
            if (CanBuildAt(tile, localNodePosition))
            {
                var node = PlaceNodeInDistrict(building, tile, localNodePosition);

                if (building.NeedsToBeBuilt)
                {
                    PlaceConstruction(building, tile, localNodePosition);
                }
                else
                {
                    building.OnBuildingConstructionCompleted(m_entityManager,
                        node.Entity,
                        node.WorldPosition,
                        tile.GetPosition());
                }

                return node;
            }

            return null;
        }

        // Construction - Building
        private bool CanBuildAt(TerrainTile tile, int2 localNodePosition)
        {
            return tile.HasDistrict && tile.District.IsNodeFree(localNodePosition);
        }

        private void PlaceConstruction(BuildingDef building, TerrainTile tile, int2 localNodePosition)
        {
            var node = tile.District[localNodePosition];

            building.OnBuildingConstructionStarted(m_entityManager,
                node.Entity,
                node.WorldPosition,
                tile.GetPosition());

            //place scaffolding
            var scaffold = PlaceEntity(_scaffoldDef, node.WorldPosition);

            node.ChildEntities.Add(_scaffoldDef.Name, scaffold);

            BuildingConstructionStartedEvent?.Invoke(building, node);
        }
        
        public DistrictNode FinishBuildingInDistrict(BuildingDef building, float2 worldPosition)
        {
            var tile = i_terrainSystem.Tiles.GetTile(worldPosition);

            return FinishBuildingInDistrict(building, tile, worldPosition.ToNodePos());
        }

        public DistrictNode FinishBuildingInDistrict(BuildingDef building, TerrainTile tile, int2 localNodePosition)
        {
            var node = tile.District[localNodePosition];

            building.OnBuildingConstructionCompleted(m_entityManager,
                node.Entity,
                node.WorldPosition,
                tile.GetPosition());

            //Remove scaffold
            var scaffold = node.ChildEntities[_scaffoldDef.Name];
            DestroyEntity(scaffold);
            node.ChildEntities.Remove(_scaffoldDef.Name);

            BuildingConstructionFinishedEvent?.Invoke(building, node);

            return node;
        }

        // Buildings
        public Entity PlaceEntity(EntityDef entity, float2 worldPosition)
        {
            var tilePos = worldPosition.ToTilePos().ToVector2Int();

            return PlaceEntity(entity, worldPosition, tilePos);
        }

        // Actors
        public Entity SpawnActor(ActorSpawnerSystem.SpawnActorCommand spawnActorCommand)
        {
            var actorDef = i_definitionService.GetDefinitionByUID<ActorDef>(spawnActorCommand.UID);

            return SpawnActor(actorDef, spawnActorCommand.Pos);
        }

        public Entity SpawnActor(ActorDef actorDef, float2 position)
        {
            var tilePosition = position.ToVector2Int();
            var tile = i_terrainSystem.Tiles.GetTile(tilePosition);

            //todo-optimise: remove null checks
            if (tile == null || actorDef == null)
            {
                Error("Could not spawn " + actorDef + " at tile position " + tilePosition);
                return Entity.Null;
            }

            var entity = PlaceEntity(actorDef, position, tilePosition);

            // LogF("Spawned {0} at {1}", actorDef.Name, position);

            return entity;
        }

        public EntityDef GetDefinitionForEntity(Entity entity)
        {
            return m_entities[entity];
        }

        public T GetDefinitionForEntity<T>(Entity entity) where T : EntityDef
        {
            return (T)m_entities[entity];
        }

        internal override void OnStart()
        {
            m_entities = new Dictionary<Entity, EntityDef>();
            m_entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            m_entityArchType = m_entityManager.CreateArchetype(typeof(Translation),
                typeof(Rotation),
                typeof(LocalToWorld),
                typeof(RenderMesh),
                typeof(RenderBounds));

            _scaffoldDef = i_definitionService.GetDefinition<DistrictNodeOverlayDef>("scaffolding");
        }

        internal override void OnUpdate()
        {
        }

        internal override void OnStop()
        {
        }

        // Building
        private DistrictNode BuildEntity(DistrictNodeDef buildingDef,
            TerrainTile tile,
            int2 nodePosition,
            bool runOnEntityCreated = true)
        {
            var tilePos = tile.GetPosition();
            Vector2 worldPosition = tilePos;
            worldPosition.x += nodePosition.x / (float)District.NodeGridSize;
            worldPosition.y += nodePosition.y / (float)District.NodeGridSize;

            var building = PlaceEntity(buildingDef, worldPosition, tilePos, runOnEntityCreated);
            var districtNode = new DistrictNode(building);
            districtNode.WorldPosition = worldPosition;
            tile.District.SetNode(nodePosition, districtNode);

            return tile.District.GetNode(nodePosition);
        }

        // Entity
        private Entity PlaceEntity(EntityDef entityDef,
            Vector2 worldPosition,
            Vector2Int tilePosition,
            bool runOnEntityCreated = true)
        {
            Entity entity = SpawnEntity(worldPosition, entityDef, tilePosition, runOnEntityCreated);

            m_entities.Add(entity, entityDef);

            return entity;
        }

        private Entity SpawnEntity(Vector2 worldPosition,
            EntityDef entityDef,
            Vector2Int tilePosition,
            bool runOnEntityCreated = true)
        {
            Entity entity = m_entityManager.CreateEntity(m_entityArchType);

            if (runOnEntityCreated)
            {
                entityDef.OnEntityCreated(m_entityManager, entity, worldPosition, tilePosition);
            }

            return entity;
        }

        private void DestroyEntity(Entity entity)
        {
            m_entityManager.DestroyEntity(entity);
        }
    }
}