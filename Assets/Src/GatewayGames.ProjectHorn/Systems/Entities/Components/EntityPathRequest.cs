﻿using Unity.Entities;
using Unity.Mathematics;

namespace Gateway.ProjectHorn.Systems.Entities.Components
{
    public struct EntityPathRequest : IComponentData
    {
        public int2 Start;
        public int2 Target;
    }
}