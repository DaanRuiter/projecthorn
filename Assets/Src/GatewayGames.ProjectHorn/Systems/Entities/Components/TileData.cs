﻿using Unity.Entities;
using Unity.Mathematics;

namespace Gateway.ProjectHorn.Systems.Entities.Components
{
    public struct TileData : IComponentData
    {
        public int X;
        public int Y;

        public int2 GetPosition() => new int2(X, Y);
    }
}