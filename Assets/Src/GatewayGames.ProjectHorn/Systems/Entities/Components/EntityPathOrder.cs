﻿using Unity.Entities;

namespace Gateway.ProjectHorn.Systems.Entities.Components
{
    public struct EntityPathOrder : IComponentData
    {
        public int PathIndex;
        public int CurrentWaypointIndex;
    }
}