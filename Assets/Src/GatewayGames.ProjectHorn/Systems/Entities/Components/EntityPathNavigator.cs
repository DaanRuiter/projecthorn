﻿using Unity.Entities;

/**
 * 07/11/2020
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Entities.Components
{
    public struct EntityPathNavigator : IComponentData
    {
        public int PathID;
        public int CurrentPathIndex;
    }
}