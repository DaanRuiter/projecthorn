﻿using Gateway.ProjectHorn.Systems.Entities.Components.Common;
using Unity.Entities;
using Unity.Mathematics;

/**
 * 03/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Entities.Components.Actor
{
    public enum ActorWorkState
    {
        Waiting,
        Seeking,
        Moving,
        Working,
        Loading,
        Unloading
    }

    public struct ActorLocalWorkData : IComponentData
    {
        public Entity District;
        public ActorWorkState State;
        public float2 WorkStationPosition;
        public float NextWorkTimeStamp;
        public float WorkDuration;

        public int WorkYieldCount; //todo: create data struct for item info
        public uint WorkYieldItemUID;

        // public ItemStackCarryData Carry;

        public Entity WorkDistrict => District;
    }

    public struct ItemStackCarryData
    {
        public uint ItemUID;
        public URange ItemCount;

        public bool HasItems => ItemCount.Count > 0;

        public bool IsNull => ItemUID == 0;
    }
}