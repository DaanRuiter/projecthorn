

using Gateway.ProjectHorn.Systems.DOTS.Item;
using Unity.Entities;
using Unity.Mathematics;

/**
 * 24/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Entities.Components.Actor
{
    public enum ActorProductionState
    {
        Waiting,
        Moving,
        Producing,
        Loading,
        Unloading
    }

    public struct ActorLocalProduceData : IComponentData
    {
        public Entity District;
        public ActorProductionState State;
        public float2 WorkStationPosition;
        public float NextProduceTimeStamp;
        public float ProductionDuration;

        // In
        public uint IntakeItemUID;
        public int IntakeItemCount;

        // Out
        public uint ProductionItemUID;
        public int ProductionItemCount;

        // public ItemStackCarryData Carry;

        public Entity WorkDistrict => District;

        public void PerformProduction(Entity actor)
        {
            LocalItemSystem.TakeItemsFrom(actor, IntakeItemUID, IntakeItemCount);
            LocalItemSystem.GiveItemsTo(actor, ProductionItemUID, ProductionItemCount);
        }
    }
}