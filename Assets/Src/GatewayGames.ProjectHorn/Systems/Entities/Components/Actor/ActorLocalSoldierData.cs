

using Unity.Entities;

/**
 * 26/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Entities.Components.Actor
{
    public enum ActorSoldierState
    {
        Recruiting,
        Waiting,
        Moving,
        Fighting
    }

    public struct ActorLocalSoldierData : IComponentData
    {
        public ActorData ActorData;
        public ActorSoldierState State;
        public int Damage;
        public int HitPoints;
        public int HitPointsCap;
    }
}