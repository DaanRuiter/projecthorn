using Unity.Entities;

/**
 * 17/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Entities.Components.Actor
{
    public enum ActorRole
    {
        None,
        Worker,
        Producer,
        Soldier,
        Builder,
        Transporter
    }

    public struct ActorRoleData : ISharedComponentData
    {
        public ActorRole Role;
    }
}