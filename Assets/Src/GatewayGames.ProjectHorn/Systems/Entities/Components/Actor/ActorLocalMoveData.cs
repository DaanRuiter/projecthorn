﻿using Gateway.ProjectHorn.Util;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

/**
 * 31/01/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Entities.Components.Actor
{
    public struct ActorLocalMoveData : IComponentData
    {
        public int2 TilePosition;
        public float2 CurrentTarget;
        public float NextSeekTimestamp;
        public float MoveSpeed;

        public bool ReachedTarget => m_reachedTarget;

        public bool Wonders; //todo: use workerRef data
        private bool m_reachedTarget;

        public bool IsAtTarget(Translation translation) => translation.Value.Distance(CurrentTarget) <= .1f;

        public bool CanSeek(float currentTime) => Wonders && currentTime >= NextSeekTimestamp;

        public void SetTarget(float2 currentTarget)
        {
            CurrentTarget = currentTarget;
            m_reachedTarget = false;
        }

        internal void SetReachedTarget(bool reached)
        {
            m_reachedTarget = reached;
        }
    }
}