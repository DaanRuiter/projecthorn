
using Unity.Entities;

/**
 * 09/07/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Entities.Components.Actor
{
    public struct ActorLocalBuilderData : IComponentData
    {
        // public Entity TargetBuilding;
        // public uint BuildPower;
        // public float NextBuildTimeStamp;
    }
}