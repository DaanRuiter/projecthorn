using Gateway.ProjectHorn.Systems.Actor;
using Unity.Entities;
using Unity.Mathematics;

/**
 * 17/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Entities.Components.Actor
{
    public struct ActorBaseData : IComponentData
    {
        public uint ActorDefUID;
        public ActorData ActorData;
        public ActorTaskState TaskState;

        // public uint DefinitionUID;
        // public Entity Home;
        // public Entity Workplace;
        // public Entity District;
        // public int2 DistrictTilePosition;
    }

    public struct ActorData
    {
        public Entity Home;
        public Entity District;
        public Entity Workplace;
        public float2 HomeTilePosition;
        public float2 WorkplacePosition;
        public int2 DistrictTilePosition;
    }
}