﻿using Unity.Entities;

namespace Gateway.ProjectHorn.Systems.Entities.Components
{
    public struct EntityBase : IComponentData
    {
        public uint ID;
    }
}