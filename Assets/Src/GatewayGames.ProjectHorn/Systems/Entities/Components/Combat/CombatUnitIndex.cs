

using Unity.Entities;

/**
 * 02/06/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Entities.Components.Combat
{
    public struct CombatUnitIndex : ISharedComponentData
    {
        public Entity CombatUnitEntity;
    }
}