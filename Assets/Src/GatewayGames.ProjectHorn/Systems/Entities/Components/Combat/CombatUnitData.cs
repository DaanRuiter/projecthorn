

using Unity.Entities;
using Unity.Mathematics;

/**
 * 20/05/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Entities.Components.Combat
{
    public struct CombatUnitData : IComponentData
    {
        public ulong CombatUnitUID;
        public int2 CurrentTileTarget;
    }
}