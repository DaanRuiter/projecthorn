using Unity.Mathematics;

/**
 * 12/06/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Entities.Components.Common
{
    public struct Range
    {
        public int Cap { get; private set; }

        public int Min { get; private set; }

        public int Count { get; private set; }

        public void SetCount(int count) => Count = math.clamp(count, Min, Cap);

        public void SetCap(int cap)
        {
            Cap = cap;
            SetCount(Count);
        }

        public void SetMin(int min)
        {
            Min = math.clamp(min, min, Cap);
            SetCount(Count);
        }

        public Range(int cap, int count)
        {
            Min = 0;
            Cap = cap;
            Count = count;
        }

        public Range(int cap, int count, int min)
        {
            Min = min;
            Cap = cap;
            Count = count;
        }

        public URange ToURange() => new URange((uint)Cap, (uint)Count);
    }

    public struct URange
    {
        public uint Cap;

        public uint Count;

        public int SignedCap => (int)Cap;

        public int SignedCount => (int)Count;

        public bool CapReached => Count >= Cap;

        public uint Space => Cap - Count;

        public float NormalizedSpace => (float)Count / Cap;

        public bool CanTake(int amount) => CanTake((uint)amount);

        public bool CanTake(uint amount) => Count + amount <= Cap;

        public void SetCount(uint count) => Count = math.clamp(count, 0, Cap);

        public void Add(uint count) => SetCount(Count + count);

        public void SetCap(uint cap)
        {
            Cap = cap;
            SetCount(Count);
        }

        public void SetMin(uint min)
        {
            SetCount(Count);
        }

        public URange(uint cap, uint count)
        {
            Cap = cap;
            Count = count;
        }

        public Range ToRange() => new Range((int)Cap, (int)Count, 0);
    }
}