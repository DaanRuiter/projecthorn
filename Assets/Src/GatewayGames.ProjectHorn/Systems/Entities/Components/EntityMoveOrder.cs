﻿using UnityEngine;

namespace Gateway.ProjectHorn.Systems.Entities.Components
{
    public class EntityMoveorder
    {
        public Vector2Int TargetPosition { get; private set; }

        public EntityMoveorder(Vector2Int position)
        {
            TargetPosition = position;
        }
    }
}