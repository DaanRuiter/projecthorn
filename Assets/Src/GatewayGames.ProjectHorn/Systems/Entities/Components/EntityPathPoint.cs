﻿using Unity.Entities;
using Unity.Mathematics;

/**
 * 07/11/2020
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Entities.Components
{
    public struct EntityPathPoint : IComponentData
    {
        public int PathID;
        public int PathIndex;
        public int2 Position;
    }
}