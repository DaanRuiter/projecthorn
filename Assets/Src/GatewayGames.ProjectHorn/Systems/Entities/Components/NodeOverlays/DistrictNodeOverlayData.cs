using Gateway.ProjectHorn.Systems.Entities.Components.Common;
using Gateway.ProjectHorn.Services.Serialization.Definitions.DistrictNodeOverlay;
using Unity.Entities;

/**
 * 03/07/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Entities.Components.NodeOverlays
{
    public struct DistrictNodeOverlayData : IComponentData
    {
        public DistrictNodeOverlayType OverlayType;
        public URange OverlayFillProgress;
    }
}