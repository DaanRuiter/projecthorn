﻿using Unity.Entities;

namespace Gateway.ProjectHorn.Systems.Entities.Components.Building
{
    public struct BuildingBaseData : IComponentData
    {
        public uint DefinitionUID;
        public Entity Entity;
        public Entity District;
        public int ActorLimit;
        public int ActorCount;
    }
}