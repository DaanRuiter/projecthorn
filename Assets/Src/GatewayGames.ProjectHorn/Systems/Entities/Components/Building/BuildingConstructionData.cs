using Gateway.ProjectHorn.Systems.Entities.Components.Common;
using Unity.Entities;

/**
 * 12/06/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Entities.Components.Building
{
    public struct BuildingConstructionData : IComponentData
    {
        public URange Progress;
        public URange BuildersAssigned;
    }
}