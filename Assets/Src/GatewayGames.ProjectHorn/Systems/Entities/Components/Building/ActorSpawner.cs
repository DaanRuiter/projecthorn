﻿using Unity.Entities;

/**
 * 25/01/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Entities.Components.Building
{
    public struct ActorSpawner : IComponentData
    {
        public uint ActorUID;
        public float LastSpawnTimestamp;
        public float SpawnInterval;
        public int SpawnRange;
        public int SpawnLimit;
        public int SpawnCount;
    }
}