
/**
 * 12/07/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Actor
{
    public enum ActorTaskState
    {
        None,
        Performing
    }
}