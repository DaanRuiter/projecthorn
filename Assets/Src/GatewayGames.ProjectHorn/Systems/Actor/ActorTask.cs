
using Gateway.ProjectHorn.Systems.Entities.Components.Common;
using Gateway.ProjectHorn.Util;
using Unity.Entities;
using Unity.Mathematics;

/**
 * 10/07/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Actor
{
    public abstract class ActorTask : SharedObject
    {
        public ActorTaskExecutionInfo ExecutionInfo;

        public virtual ActorTaskExecutionInfo GetExecutionInfo()
        {
            return ExecutionInfo;
        }

        public abstract void OnTaskCompleted();

        public abstract void OnTaskStepCompleted(uint stepCount);
    }

    public struct ActorTaskExecutionInfo
    {
        public uint ID;
        public Entity Actor;
        public Entity TargetEntity;
        public float2 TargetPosition;
        public URange TaskExecuteStepCount;
        public float TaskExecuteInterval;
        public float NextExecuteTimeStamp;

        public bool IsFinished => TaskExecuteStepCount.CapReached;
    }
}