using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Services.Graphics;
using Gateway.ProjectHorn.Services.Serialization.Definitions.Building;
using Gateway.ProjectHorn.Systems.DOTS.Building;
using Gateway.ProjectHorn.Systems.Entities;
using Gateway.ProjectHorn.Systems.Entities.Components.Building;
using Unity.Mathematics;

/**
 * 12/07/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Actor.Tasks
{
    public class ActorConstructionTask : ActorTask
    {
        [InjectService] private EntitySystem i_entitySystem;

        public override void OnTaskCompleted()
        {
            var buildingDef = i_entitySystem.GetDefinitionForEntity<BuildingDef>(ExecutionInfo.TargetEntity);

            i_entitySystem.FinishBuildingInDistrict(buildingDef, ExecutionInfo.TargetPosition);

            Main.EntityManager.RemoveComponent<BuildingConstructionData>(ExecutionInfo.TargetEntity);
        }

        public override void OnTaskStepCompleted(uint stepCount)
        {
            float normalizedSpace = ExecutionInfo.TaskExecuteStepCount.NormalizedSpace;
            var scale = new float2(1f, normalizedSpace);
            var scaffold = ConstructionSystem.GetScaffoldOverlayEntity(ExecutionInfo.TargetPosition);

            GraphicsService.SetEntityScale(scaffold, scale);
        }
    }
}