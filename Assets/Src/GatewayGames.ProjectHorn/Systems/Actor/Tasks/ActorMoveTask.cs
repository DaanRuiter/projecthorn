/**
 * 10/07/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Actor.Tasks
{
    public class ActorMoveTask : ActorTask
    {
        public override void OnTaskCompleted()
        {
        }

        public override void OnTaskStepCompleted(uint stepCount)
        {
        }
    }
}