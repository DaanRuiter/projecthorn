using Gateway.ProjectHorn.Systems.DOTS.Item;
using NodeItemYield = Gateway.ProjectHorn.Systems.Building.District.NodeItemYield;

/**
 * 31/07/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Actor.Tasks
{
    public class ActorWorkTask : ActorTask
    {
        private NodeItemYield[] m_yields;

        public ActorWorkTask(NodeItemYield[] yields)
        {
            m_yields = yields;
        }

        public override void OnTaskCompleted()
        {
        }

        public override void OnTaskStepCompleted(uint stepCount)
        {
            foreach (var yield in m_yields)
            {
                LocalItemSystem.CreateItemStackUpdate((uint)yield.YieldData.ItemUID,
                    yield.YieldData.WorkYieldCount,
                    ExecutionInfo.Actor);
            }

            //todo: check inventory space and complete task early if no space
        }
    }
}