using System;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

/**
 * 12/07/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Actor
{
    [NativeContainer]
    public struct ActorTaskBuffer : INativeList<ActorTaskExecutionInfo>, IDisposable
    {
        private NativeList<ActorTaskExecutionInfo> m_list;

        public ActorTaskBuffer(Allocator allocator)
        {
            m_list = new NativeList<ActorTaskExecutionInfo>(allocator);
        }

        public void Dispose()
        {
            m_list.Dispose();
        }

        public void Add(ActorTaskExecutionInfo task)
        {
            m_list.Add(task);
        }

        public ref ActorTaskExecutionInfo ElementAt(int index) => ref m_list.ElementAt(index);

        public void Clear() => m_list.Clear();

        public int Length
        {
            get => m_list.Length;
            set => m_list.Length = value;
        }

        public int Capacity
        {
            get => m_list.Capacity;
            set => m_list.Capacity = value;
        }

        public bool IsEmpty => m_list.IsEmpty;

        public ActorTaskExecutionInfo this[int index]
        {
            get => m_list[index];
            set => m_list[index] = value;
        }
    }
}