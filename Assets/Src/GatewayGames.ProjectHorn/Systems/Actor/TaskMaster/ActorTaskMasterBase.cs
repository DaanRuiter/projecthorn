
using System;

/**
 * 10/07/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Actor.TaskMaster
{
    public interface IActorTaskMaster
    {
        Type GetActorTaskType();

        void SetActorTaskSystem(ActorTaskSystem system);
    }

    public abstract class ActorTaskMasterBase<T> : IActorTaskMaster where T : ActorTask
    {
        private ActorTaskSystem _actorTaskSystem;

        public Type GetActorTaskType() => typeof(T);

        public void SetActorTaskSystem(ActorTaskSystem system)
        {
            _actorTaskSystem = system;
        }

        protected void EnqueueTask(T task)
        {
            _actorTaskSystem.EnqueueTask(task);
        }
    }
}