
using Gateway.ProjectHorn.Systems.Actor.Tasks;

/**
 * 12/07/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Actor.TaskMaster
{
    public class ActorConstructionTaskMaster : ActorTaskMasterBase<ActorConstructionTask>
    {
        public ActorConstructionTask CreateTask(ActorTaskExecutionInfo taskExecutionInfo)
        {
            return new ActorConstructionTask()
            {
                ExecutionInfo = taskExecutionInfo
            };
        }
    }
}