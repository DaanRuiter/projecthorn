
using Gateway.ProjectHorn.Systems.Actor.Tasks;
using Gateway.ProjectHorn.Systems.Entities.Components.Common;
using Unity.Entities;
using Unity.Mathematics;

/**
 * 10/07/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Actor.TaskMaster
{
    public class ActorMoveTaskMaster : ActorTaskMasterBase<ActorMoveTask>
    {
        public ActorMoveTask CreateMoveTask(Entity actor, float2 targetPosition)
        {
            return new ActorMoveTask()
            {
                ExecutionInfo = new ActorTaskExecutionInfo()
                {
                    Actor = actor,
                    TargetPosition = targetPosition,
                    TaskExecuteStepCount = new URange(1, 0)
                }
            };
        }
    }
}