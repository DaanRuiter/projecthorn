
using Gateway.ProjectHorn.Systems.Actor.Tasks;

/**
 * 31/07/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Actor.TaskMaster
{
    public class ActorWorkTaskMaster : ActorTaskMasterBase<ActorWorkTask>
    {
    }
}