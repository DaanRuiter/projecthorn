
using System.Collections.Generic;
using Gateway.ProjectHorn.Systems.Actor.TaskMaster;
using Gateway.ProjectHorn.Systems.DOTS;
using Gateway.ProjectHorn.Systems.DOTS.Actor;
using Gateway.ProjectHorn.Systems.Entities.Components.Actor;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;

/**
 * 10/07/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Actor
{
    [UpdateAfter(typeof(ActorLocalMoveSystem))]
    public class ActorTaskSystem : BaseDOTSSystem
    {
        protected override float updateInterval => .1f;

        private ActorTaskDictionary m_actorTasks;
        private List<IActorTaskMaster> m_actorTaskMasters;

        public void EnqueueTask(ActorTask actorTask)
        {
            m_actorTasks.EnqueueSafe(actorTask.ExecutionInfo.Actor, actorTask);
        }

        public void AddTaskMaster(IActorTaskMaster taskMaster)
        {
            m_actorTaskMasters.Add(taskMaster);
            taskMaster.SetActorTaskSystem(this);
        }

        public IActorTaskMaster GetMasterForTask<T>() where T : ActorTask
        {
            foreach (var taskMaster in m_actorTaskMasters)
            {
                if (taskMaster.GetActorTaskType() == typeof(T))
                {
                    return taskMaster;
                }
            }

            return null;
        }

        public T GetTaskMaster<T>() where T : IActorTaskMaster
        {
            foreach (var taskMaster in m_actorTaskMasters)
            {
                if (taskMaster.GetType() == typeof(T))
                {
                    return (T)taskMaster;
                }
            }

            return default;
        }

        protected override void StartSystem()
        {
            m_actorTasks = new ActorTaskDictionary();
            m_actorTaskMasters = new List<IActorTaskMaster>();
        }

        protected override JobHandle UpdateSystem(JobHandle inputDeps, SystemTimeInfo timeInfo)
        {
            var executionInfoMap = m_actorTasks.GetExecutionInfoMap(Allocator.TempJob);

            Entities.ForEach((ref ActorLocalMoveData moveData,
                ref ActorTaskComponentData actorTaskData,
                ref ActorBaseData actorBaseData,
                in Entity entity) =>
            {
                bool hasTask = executionInfoMap.ContainsKey(entity);
                actorBaseData.TaskState = hasTask ? ActorTaskState.Performing : ActorTaskState.None;

                if (!hasTask)
                {
                    return;
                }

                var task = executionInfoMap[entity];

                if (!moveData.CurrentTarget.Equals(task.TargetPosition))
                {
                    moveData.SetTarget(task.TargetPosition);
                }

                if (moveData.ReachedTarget)
                {
                    if (timeInfo.CurrentTime >= task.NextExecuteTimeStamp)
                    {
                        ExecuteTaskStep(ref task, actorTaskData, timeInfo);
                    }
                }

                executionInfoMap[entity] = task;
            }).Run();

            m_actorTasks.UpdateTasks(executionInfoMap);

            executionInfoMap.Dispose();

            return inputDeps;
        }

        protected override void StopSystem()
        {
        }

        private static void ExecuteTaskStep(ref ActorTaskExecutionInfo taskExecutionInfo,
            ActorTaskComponentData taskComponentData,
            SystemTimeInfo timeInfo)
        {
            float bonus = 1f - taskComponentData.WorkSpeed;
            float duration = (taskExecutionInfo.TaskExecuteInterval - bonus) / timeInfo.TimeScale;
            taskExecutionInfo.NextExecuteTimeStamp = timeInfo.CurrentTime + duration;

            //todo: allow multiple steps per executions

            taskExecutionInfo.TaskExecuteStepCount.Add(1);
        }
    }
}