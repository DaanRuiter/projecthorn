
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;

/**
 * 10/07/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Actor
{
    public class ActorTaskDictionary : Dictionary<Entity, List<ActorTask>>
    {
        public void EnqueueSafe(Entity actor, ActorTask task)
        {
            if (ContainsKey(actor))
            {
                this[actor].Add(task);
            }
            else
            {
                var list = new List<ActorTask>();
                list.Add(task);
                Add(actor, list);
            }
        }

        public NativeHashMap<Entity, ActorTaskExecutionInfo> GetExecutionInfoMap(Allocator allocator)
        {
            var map = new NativeHashMap<Entity, ActorTaskExecutionInfo>(Count, allocator);

            foreach (var keyValue in this)
            {
                map.Add(keyValue.Key, keyValue.Value[0].GetExecutionInfo());
            }

            return map;
        }

        public void UpdateTasks(NativeHashMap<Entity, ActorTaskExecutionInfo> executionInfoMap)
        {
            foreach (var executionInfo in executionInfoMap)
            {
                if (executionInfo.Value.IsFinished)
                {
                    var taskList = this[executionInfo.Key];
                    var completedTask = taskList[0];

                    completedTask.OnTaskStepCompleted(executionInfo.Value.TaskExecuteStepCount.Cap);
                    completedTask.OnTaskCompleted();

                    taskList.RemoveAt(0);

                    if (taskList.Count == 0) //No more tasks queued
                    {
                        Remove(executionInfo.Key);
                    }
                }
                else
                {
                    uint stepsCompleted = executionInfo.Value.TaskExecuteStepCount.Count;
                    this[executionInfo.Key][0].ExecutionInfo = executionInfo.Value;

                    if (stepsCompleted > 0)
                    {
                        this[executionInfo.Key][0].OnTaskStepCompleted(stepsCompleted);
                    }
                }
            }
        }
    }
}