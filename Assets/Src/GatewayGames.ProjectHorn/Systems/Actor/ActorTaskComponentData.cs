
using Unity.Entities;

/**
 * 10/07/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.Actor
{
    public struct ActorTaskComponentData : IComponentData
    {
        public float WorkSpeed;
    }
}