﻿/**
 * 25/01/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

using Gateway.ProjectHorn.Services;

namespace Gateway.ProjectHorn.Systems
{
    public abstract class SystemBase : ServiceBase
    {
    }
}