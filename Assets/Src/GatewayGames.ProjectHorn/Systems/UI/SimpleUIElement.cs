﻿using System;
using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Systems.UI.Elements;
using Gateway.ProjectHorn.Util;
using Unity.Entities;
using UnityEngine;

/**
 * 07/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.UI
{
    public abstract class SimpleUIElement : SharedMonoBehaviour, IUIElement
    {
        public event Action ShowEvent;

        public event Action HideEvent;

        public Vector2 Position => GetRectTransform().position;

        public Vector2 Size => GetRectTransform().rect.size;

        public RectTransform Transform => GetRectTransform();

        protected EntityManager entityManager => World.DefaultGameObjectInjectionWorld.EntityManager;

        [InjectService] protected UISystem _uiSystem;

        private RectTransform m_rectTransform;

        protected override void Awake()
        {
            base.Awake();

            _uiSystem.AddElement(this);
            OnStart();
        }

        protected virtual void OnDestroy() => OnDispose();

        private void Update() => OnUpdate();

        protected abstract void OnStart();

        protected abstract void OnUpdate();

        protected abstract void OnDispose();

        protected bool IsCursorOnUI => Main.EventSystem.IsPointerOverGameObject();

        protected void ToggleGameObject(GameObject _gameObject, bool state)
        {
            if (_gameObject.activeInHierarchy != state)
            {
                _gameObject.SetActive(state);
            }
        }

        public virtual void Show()
        {
            gameObject.SetActive(true);
            ShowEvent?.Invoke();
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
            HideEvent?.Invoke();
        }

        private RectTransform GetRectTransform() =>
            m_rectTransform ? m_rectTransform : m_rectTransform = GetComponent<RectTransform>();
    }
}