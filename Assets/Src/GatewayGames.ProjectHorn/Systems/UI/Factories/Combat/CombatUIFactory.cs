

using Gateway.ProjectHorn.Engine;
using Gateway.ProjectHorn.Services;
using UnityEngine;

/**
 * 09/06/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.UI.Factories
{
    [RegisterUIElementFactory]
    public class CombatUIFactory : UIElementFactory<CombatUnitHUD>
    {
        private const string CombatUnitPrefabName = "CombatUnitHUD";

        public override CombatUnitHUD CreateElement(UIElementFactoryParams @params = null)
        {
            var prefab = PrefabCollection.StandardGameAssets.GetPrefab(CombatUnitPrefabName);
            var view = Object.Instantiate(prefab, @params?.Parent).GetComponent<CombatUnitHUD>();

            view.Init(100, 100, 1, 0);

            return view;
        }
    }
}