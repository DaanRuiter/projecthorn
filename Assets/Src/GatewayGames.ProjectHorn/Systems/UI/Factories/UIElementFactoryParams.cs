

using Unity.Entities;
using UnityEngine;

/**
 * 09/06/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.UI.Factories
{
    public class UIElementFactoryParams
    {
        public RectTransform Parent;
        public Entity Entity;
    }
}