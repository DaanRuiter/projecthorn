

using Gateway.ProjectHorn.Systems.UI.Elements;

/**
 * 09/06/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.UI.Factories
{
    public abstract class UIElementFactory<T> : IUIElementFactory where T : IUIElement
    {
        public abstract T CreateElement(UIElementFactoryParams @params = null);
    }
}