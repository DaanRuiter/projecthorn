

using Gateway.ProjectHorn.Systems.UI.Elements;
using Gateway.ProjectHorn.Util;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;

/**
 * 05/06/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.UI
{
    public class EntityUIElement : SimpleUIElement, IEntityUIElement
    {
        protected override void OnStart()
        {
        }

        protected override void OnUpdate()
        {
        }

        protected override void OnDispose()
        {
        }

        public virtual float3 GetEntityWorldPosition(Entity entity)
        {
            var translationPos = entityManager.GetComponentData<Translation>(entity).Value;
            var meshData = entityManager.GetSharedComponentData<RenderMesh>(entity);
            var meshCenter = meshData.mesh.bounds.center;
            return translationPos.ToVector3() + meshCenter;
        }
    }
}