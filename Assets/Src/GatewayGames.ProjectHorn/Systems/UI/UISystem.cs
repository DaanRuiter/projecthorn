﻿/**
 * 07/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

using System;
using System.Collections.Generic;
using Gateway.ProjectHorn.Services;
using Gateway.ProjectHorn.Systems.UI.Elements;
using Gateway.ProjectHorn.Systems.UI.Factories;

namespace Gateway.ProjectHorn.Systems.UI
{
    public class UISystem : SystemBase
    {
        public bool IsPlacingEntity;

        private Dictionary<Type, IUIElementFactory> m_factories;
        private List<IUIElement> m_elements;

        public void AddElement(IUIElement element)
        {
            if (!m_elements.Contains(element))
            {
                m_elements.Add(element);
            }
        }

        public T GetElement<T>() where T : IUIElement
        {
            for (int i = 0; i < m_elements.Count; i++)
            {
                if (m_elements[i].GetType() == typeof(T))
                {
                    return (T)m_elements[i];
                }
            }

            return default;
        }

        public void RemoveElement(IUIElement element)
        {
            if (m_elements.Contains(element))
            {
                m_elements.Remove(element);
            }
        }

        public UIElementFactory<T> GetFactoryForElementType<T>() where T : IUIElement
        {
            if (m_factories.ContainsKey(typeof(T)))
            {
                return m_factories[typeof(T)] as UIElementFactory<T>;
            }

            return null;
        }

        public T CreateUIElement<T>(UIElementFactoryParams @params = null, bool registerElement = true)
            where T : IUIElement
        {
            var factory = GetFactoryForElementType<T>();

            if (factory != null)
            {
                var element = factory.CreateElement(@params);

                if (registerElement)
                {
                    AddElement(element);
                }

                return element;
            }

            return default;
        }

        internal override void OnStart()
        {
            m_elements = new List<IUIElement>();
            m_factories = new Dictionary<Type, IUIElementFactory>();

            var factoryTypes = ServiceInjector.GetTypesWithAttribute(typeof(RegisterUIElementFactory));

            for (int i = 0; i < factoryTypes.Length; i++)
            {
                var baseType = factoryTypes[i].BaseType;

                if (baseType == null) continue;

                var targetUIElementType = baseType.GetGenericArguments();

                if (typeof(IUIElementFactory).IsAssignableFrom(factoryTypes[i]) && targetUIElementType.Length > 0)
                {
                    for (int j = 0; j < targetUIElementType.Length; j++)
                    {
                        if (typeof(IUIElement).IsAssignableFrom(targetUIElementType[j]))
                        {
                            RegisterFactoryForElementType(targetUIElementType[j],
                                Activator.CreateInstance(factoryTypes[i]) as IUIElementFactory);
                        }
                    }
                }
            }
        }

        internal override void OnUpdate()
        {
        }

        internal override void OnStop()
        {
        }

        public void RegisterFactoryForElementType(Type UIElementType, IUIElementFactory factory)
        {
            if (m_factories.ContainsKey(UIElementType))
            {
                Error($"UI element factory for ui type {UIElementType.Name} already registered");
                return;
            }

            m_factories.Add(UIElementType, factory);

            Log($"Registered {factory} ui factory for element type {UIElementType.Name}");
        }
    }
}