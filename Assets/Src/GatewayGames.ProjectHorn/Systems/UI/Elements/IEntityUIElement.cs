

using Unity.Entities;
using Unity.Mathematics;

/**
 * 05/06/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.UI.Elements
{
    public interface IEntityUIElement : IUIElement
    {
        float3 GetEntityWorldPosition(Entity entity);
    }
}