using System;
using Unity.Entities;

/**
 * 11/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.UI.Elements
{
    public interface IListUIElement<T> : IUIElement where T : IUIElement
    {
        event Action<T> ElementAddedEvent;

        event Action<T, int> ElementInsertedEvent;

        event Action<T> ElementRemovedEvent;

        void AddElement(T element);

        void InsertElement(T element, int index);

        T GetElement(int index);

        T GetElement(Entity entity);

        void RemoveElement(T element);

        void RemoveElement(int index);
    }
}