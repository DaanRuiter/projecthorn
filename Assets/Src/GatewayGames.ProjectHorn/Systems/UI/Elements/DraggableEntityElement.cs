

using System;
using UnityEngine;
using UnityEngine.EventSystems;

/**
 * 05/06/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.UI.Elements
{
    public class DraggableEntityElement : EntityUIElement, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        public event Action DragBeginEvent;

        public event Action DragEvent;

        public event Action<GameObject[]> DragEndEvent;

        protected override void OnStart()
        {
        }

        protected override void OnUpdate()
        {
        }

        protected override void OnDispose()
        {
        }

        protected virtual void UpdatePosition()
        {
            Transform.position = Input.mousePosition;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            DragBeginEvent?.Invoke();
        }

        public void OnDrag(PointerEventData eventData)
        {
            UpdatePosition();
            DragEvent?.Invoke();
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            DragEndEvent?.Invoke(eventData.hovered.ToArray());
        }
    }
}