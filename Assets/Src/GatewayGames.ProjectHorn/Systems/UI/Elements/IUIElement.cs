﻿using UnityEngine;

namespace Gateway.ProjectHorn.Systems.UI.Elements
{
    public interface IUIElement
    {
        Vector2 Position { get; }

        Vector2 Size { get; }

        RectTransform Transform { get; }

        void Show();

        void Hide();
    }
}