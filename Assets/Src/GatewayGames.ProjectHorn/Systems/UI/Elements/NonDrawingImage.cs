using UnityEngine;
using UnityEngine.UI;

/**
 * 13/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Systems.UI.Elements
{
    [RequireComponent(typeof(CanvasRenderer))]
    public class NonDrawingImage : Graphic
    {
        public override void SetMaterialDirty()
        {
        }

        public override void SetVerticesDirty()
        {
        }

        protected override void OnPopulateMesh(VertexHelper vh)
        {
            vh.Clear();
        }
    }
}