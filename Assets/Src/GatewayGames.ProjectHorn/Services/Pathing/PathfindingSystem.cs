﻿// using Unity.Jobs;
// using Unity.Transforms;
// using Unity.Mathematics;
// using Unity.Collections;
// using Unity.Entities;
// using UnityEngine;
// using System.Xml.Serialization;
// using UnityEngine.PlayerLoop;
// using Unity.Burst;
// using Boo.Lang;
// using UnityEditor.ShaderGraph.Internal;
// using static Unity.Entities.EntityCommandBuffer;
//
// public struct Node
// {
//     public bool Walkable;
//     public int2 Position;
//     public int gCost;
//     public int hCost;
//     public int fCost { get { return gCost + hCost; } }
//     public int2 ParentIndex;
//
//     public bool Equals(Node other)
//     {
//         return Position.x == other.Position.x && Position.y == other.Position.y;
//     }
// }
//
// public class NewPathfindingSystem : SystemBase
// {
//     private EndSimulationEntityCommandBufferSystem m_entityCommandBuffer;
//     private EntityQuery m_requestQuery;
//     private bool m_initialized;
//     private List<FindEntityPathJob2> pathJobs = new List<FindEntityPathJob2>();
//     private List<JobHandle> pathJobHandles = new List<JobHandle>();
//
//     public void Initialize()
//     {
//         m_initialized = true;
//         m_requestQuery = GetEntityQuery(typeof(EntityPathRequest));
//         Terrain.NODE_DATA = new NativeHashMap<int2, Node>(Terrain.Tiles.Length, Allocator.Persistent);
//
//         for(int x = 0; x < Terrain.Tiles.Dimensions.x; x++)
//         {
//             for (int y = 0; y < Terrain.Tiles.Dimensions.y; y++)
//             {
//                 int2 index = new int2(x, y);
//                 Terrain.NODE_DATA[index] = new Node
//                 {
//                     Walkable = true,
//                     Position = index
//                 };
//             }
//         }
//     }
//
//     protected override void OnCreate()
//     {
//         base.OnCreate();
//         m_entityCommandBuffer = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
//     }
//
//     protected override void OnUpdate()
//     {
//         if (!m_initialized)
//         {
//             if (Terrain.Tiles == null)
//                 return;
//             else
//                 Initialize();
//         }
//
//         EntityManager entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
//         int2 dimensions = Terrain.Tiles.Dimensions.ToInt2();
//         NativeArray<Entity> entities = m_requestQuery.ToEntityArray(Allocator.TempJob);
//
//         foreach(Entity entity in entities)
//         {
//             var startTime = Time.ElapsedTime;
//             EntityPathRequest request = entityManager.GetComponentData<EntityPathRequest>(entity);
//
//
//             FindEntityPathJob2 job = new FindEntityPathJob2
//             {
//                 Nodes = Terrain.NODE_DATA,
//                 Dimensions = dimensions,
//                 Start = request.Start,
//                 Target = request.Target,
//                 Result = new NativeList<int2>(Allocator.TempJob),
//                 OpenList = new NativeList<int2>(Allocator.TempJob),
//                 ClosedList = new NativeList<int2>(Allocator.TempJob),
//                 x = new NativeArray<int>(1, Allocator.TempJob),
//                 Entity = entity,
//                 CommandBuffer = m_entityCommandBuffer.CreateCommandBuffer().ToConcurrent(),
//                 PathID = Pathing.Paths.Count
//             };
//             entityManager.RemoveComponent<EntityPathRequest>(entity);
//             JobHandle pathJobHandle = job.Schedule();
//             pathJobs.Add(job);
//             pathJobHandles.Add(pathJobHandle);
//             //m_entityCommandBuffer.AddJobHandleForProducer(pathJobHandle);
//
//             //Gateway.ProjectHorn.Entities.BuildEntity(Definitions.GetDefinition<BuildingDef>("Debug_Red"), new Vector2Int(request.Start.x, request.Start.y));
//             //Gateway.ProjectHorn.Entities.BuildEntity(Definitions.GetDefinition<BuildingDef>("Debug_Green"), new Vector2Int(request.Target.x, request.Target.y));
//
//             //UnityEngine.Debug.Log("Path from " + job.Start + " to " + job.Target);
//             //UnityEngine.Debug.Log(job.x[0] + " executions");
//             //UnityEngine.Debug.Log(job.Result.Length + " nodes in path");
//             //foreach (int2 point in job.Result)
//             //{
//             //    UnityEngine.Debug.Log(point);
//             //    //Gateway.ProjectHorn.Entities.BuildEntity(Definitions.GetDefinition<BuildingDef>("Debug_Blue"), new Vector2Int(point.x, point.y));
//             //}
//
//
//             //yield return new WaitUntil(() => handleCalculate.IsCompleted);
//
//             //if (job.Result.Length > 0)
//             //{
//             //    ConstructEntityPathJob constructEntityPathJob = new ConstructEntityPathJob
//             //    {
//             //        Entity = entity,
//             //        CommandBuffer = m_entityCommandBuffer.PostUpdateCommands,
//             //        PathID = Pathing.Paths.Count,
//             //        points = job.Result
//             //    };
//             //    constructEntityPathJob.Schedule(pathJobHandle);
//
//             //    int pathIndex = 0;
//             //    int pathID = Pathing.Paths.Count;
//             //    foreach(int2 point in job.Result)
//             //    {
//             //        entityManager.AddComponentData(entityManager.CreateEntity(), new EntityPathPoint
//             //        {
//             //            PathIndex = pathIndex,
//             //            Position = point,
//             //            PathID = pathID
//             //        });
//             //        pathIndex++;
//             //    }
//             //    entityManager.AddComponentData(entity, new EntityPathNavigator
//             //    {
//             //        PathID = pathID,
//             //        CurrentPathIndex = 0
//             //    });
//             //foreach(var point in Pathing.Paths[Pathing.Paths.Count - 1].Waypoints)
//             //{
//             //    UnityEngine.Debug.Log(point);
//             //}
//             //UnityEngine.Debug.Log(Pathing.Paths[Pathing.Paths.Count - 1].Waypoints.Length);
//
//             //}
//         }
//
//         entities.Dispose();
//
//         for (int i = pathJobHandles.Count - 1; i >= 0; i--)
//         {
//             if (pathJobHandles[i].IsCompleted)
//             {
//                 var job = pathJobs[i];
//                 job.Result.Dispose();
//                 job.OpenList.Dispose();
//                 job.Nodes.Dispose();
//                 job.ClosedList.Dispose();
//                 job.x.Dispose();
//
//                 pathJobs.RemoveAt(i);
//                 pathJobHandles.RemoveAt(i);
//             }
//         }
//     }
// }
//
// public struct ConstructEntityPathJob : IJob
// {
//     public Entity Entity;
//     public EntityCommandBuffer CommandBuffer;
//     public NativeArray<int2> points;
//     public int PathID;
//
//     public void Execute()
//     {
//         int pathIndex = 0;
//         foreach (int2 point in points)
//         {
//             CommandBuffer.AddComponent(CommandBuffer.CreateEntity(), new EntityPathPoint
//             {
//                 PathIndex = pathIndex,
//                 Position = point,
//                 PathID = PathID
//             });
//             pathIndex++;
//         }
//         CommandBuffer.AddComponent(Entity, new EntityPathNavigator
//         {
//             PathID = PathID,
//             CurrentPathIndex = 0
//         });
//     }
// }
//
// public struct FindEntityPathJob2 : IJob
// {
//     public NativeHashMap<int2, Node> Nodes;
//     public NativeList<int2> Result;
//     public NativeList<int2> OpenList;
//     public NativeList<int2> ClosedList;
//     public int2 Dimensions;
//     public int2 Start;
//     public int2 Target;
//     public NativeArray<int> x;
//
//     public Entity Entity;
//     public Concurrent CommandBuffer;
//     public int PathID;
//
//     public void Execute()
//     {
//         Node startNode = Nodes[Start];
//         Node targetNode = Nodes[Target];
//
//         OpenList.Add(startNode.Position);
//         int attempts = 0;
//         while(OpenList.Length > 0 && attempts < 20000)
//         {
//             
//             attempts++;
//             x[0] = attempts;
//             Node currentNode = GetBestNode(Nodes[OpenList[0]]);
//
//             //UnityEngine.Debug.Log("+ Current Node Position: " + currentNode.Position);
//
//             OpenList.Remove(currentNode.Position);
//             ClosedList.Add(currentNode.Position);
//
//             if (currentNode.Equals(targetNode))
//             {
//                 Complete(startNode, targetNode);
//                 return;
//             }
//
//             foreach(Node neighbor in GetNeighbors(currentNode))
//             {
//                 //UnityEngine.Debug.Log(neighbor.Position);
//                 if (!neighbor.Walkable || ClosedList.Contains(neighbor.Position))
//                 {
//                     continue;
//                 }
//
//                 int newCost = currentNode.gCost + GetDistance(currentNode, neighbor);
//                 if(newCost < neighbor.gCost || !OpenList.Contains(neighbor.Position))
//                 {
//                     Nodes[neighbor.Position] = new Node
//                     {
//                         gCost = newCost,
//                         hCost = GetDistance(neighbor, targetNode),
//                         ParentIndex = currentNode.Position,
//                         Position = neighbor.Position,
//                         Walkable = true
//                     };
//
//                     int totalCost = Nodes[neighbor.Position].hCost + newCost;
//                     //UnityEngine.Debug.Log(neighbor.Position + " hcost: " + Nodes[neighbor.Position].hCost + " / gCost: " + newCost + " = " + totalCost);
//
//                     if (!OpenList.Contains(neighbor.Position))
//                     {
//                         OpenList.Add(neighbor.Position);
//                     }
//                 }
//             }
//         }
//     }
//
//     private void Complete(Node startNode, Node targetNode)
//     {
//         //UnityEngine.Debug.Log("TargetNode has parent " + Nodes[targetNode.Position].ParentIndex);
//
//         int attempts = 0;
//         Node traceNode = Nodes[targetNode.Position];
//         while (!traceNode.Equals(startNode) && attempts < 100)
//         {
//             attempts++;
//             Result.Add(traceNode.Position);
//             traceNode = Nodes[traceNode.ParentIndex];
//             //UnityEngine.Debug.Log(traceNode.Position + " has parent " + Nodes[traceNode.ParentIndex].Position);
//         }
//         Result.Reverse();
//
//         int pathIndex = 0;
//         //foreach (int2 point in Result)
//         //{
//         //    CommandBuffer.AddComponent(0, CommandBuffer.CreateEntity(0), new EntityPathPoint
//         //    {
//         //        PathIndex = pathIndex,
//         //        Position = point,
//         //        PathID = PathID
//         //    });
//         //    pathIndex++;
//         //}
//         //CommandBuffer.AddComponent(0, Entity, new EntityPathNavigator
//         //{
//         //    PathID = PathID,
//         //    CurrentPathIndex = 0
//         //});
//         //UnityEngine.Debug.Log("- Path Finished with length: " + Result.Length);
//     }
//
//     private Node GetBestNode(Node current)
//     {
//         //UnityEngine.Debug.Log("? Get best Node()");
//         Node best = current;
//         foreach(int2 point in OpenList)
//         {
//             Node node = Nodes[point];
//             //UnityEngine.Debug.Log(node.Position + " cost: " + node.fCost);
//             if (node.fCost < best.fCost || node.fCost == best.fCost && node.hCost < best.hCost)
//             {
//                 best = node;
//             }
//         }
//         //UnityEngine.Debug.Log("! Best Node Found: " + best.Position + " with cost " + best.fCost);
//         return best;
//     }
//
//     int GetDistance(Node nodeA, Node nodeB)
//     {
//         int dstX = math.abs(nodeA.Position.x - nodeB.Position.x);
//         int dstY = math.abs(nodeA.Position.y - nodeB.Position.y);
//
//         //UnityEngine.Debug.Log("DistX: " + dstX);
//         //UnityEngine.Debug.Log("DistY: " + dstY);
//
//         return dstX + dstY;
//         //if (dstX > dstY)
//         //    return 14 * dstY + 10 * (dstX - dstY);
//         //return 14 * dstX + 10 * (dstY - dstX);
//     }
//
//     private Node[] GetNeighbors(Node node)
//     {
//         NativeList<Node> result = new NativeList<Node>(Allocator.Temp);
//
//         for (int x = -1; x <= 1; x++)
//         {
//             for (int y = -1; y <= 1; y++)
//             {
//                 if (x == 0 && y == 0)
//                     continue; // Skip self
//
//
//                 int checkX = node.Position.x + x;
//                 int checkY = node.Position.y + y;
//
//                 if (checkX >= 0 && checkX < Dimensions.x && checkY >= 0 && checkY < Dimensions.y)
//                 {
//                     result.Add(Nodes[new int2(checkX, checkY)]);
//                 }
//             }
//         }
//
//         return result.ToArray();
//     }
// }
//
// public struct FindEntityPathJob : IJob
// {
//     [ReadOnly]
//     public NativeArray<byte> TileWalkCosts;
//     public NativeList<int2> Result;
//     public int2 Dimensions;
//     public int2 Start;
//     public int2 Target;
//
//     public NativeList<int2> OpenList;
//     public NativeList<int2> ClosedList;
//     public NativeArray<int> GCosts;
//     public NativeArray<int> FCosts;
//
//     public void Execute()
//     {
//         var startIndex = PosToSetIndex(Start);
//         int attempts = 0;
//
//         //Set start tile
//         GCosts[startIndex] = 0;
//         FCosts[startIndex] = Heuristic(Start, Target);
//         OpenList.Add(Start);
//
//         while(OpenList.Length > 0 && attempts < 1000)
//         {
//             attempts++;
//             int2 current = NextBest();
//
//             if (current.x == Target.x && current.y == Target.y)
//             {
//                 //Done
//                 return;
//             }
//
//             OpenList.RemoveAt(OpenList.IndexOf(current));
//             ClosedList.Add(current);
//
//             foreach(int2 neighbor in GetNeighbors(current))
//             {
//                 if (ClosedList.Contains(neighbor))
//                     continue;
//
//                 int projectedCost = getGScore(current) + 1;
//
//                 if (!OpenList.Contains(neighbor))
//                 {
//                     OpenList.Add(neighbor);
//                 }
//                 else if(projectedCost >= getGScore(neighbor))
//                 {
//                     continue;
//                 }
//
//                 Result.Add(neighbor);
//                 int neighborIndex = PosToSetIndex(neighbor);
//                 GCosts[neighborIndex] = projectedCost;
//                 FCosts[neighborIndex] = projectedCost + Heuristic(neighbor, Target);
//             }
//         }
//     }
//
//     private int PosToSetIndex(int2 position)
//     {
//         return position.y * Dimensions.x + position.x;
//     }
//
//     private int Heuristic(int2 start, int2 target)
//     {
//         var dx = target.x - start.x;
//         var dy = target.y - start.y;
//         return math.abs(dx) + math.abs(dy);
//     }
//
//     private int getGScore(int2 point)
//     {
//         return GCosts[PosToSetIndex(point)];
//     }
//
//     private int getFScore(int2 point)
//     {
//         return FCosts[PosToSetIndex(point)];
//     }
//
//     private int2[] GetNeighbors(int2 position)
//     {
//         NativeList<int2> result = new NativeList<int2>(Allocator.Temp);
//
//         for (int x = -1; x <= 1; x++)
//         {
//             for (int y = -1; y <= 1; y++)
//             {
//                 if (x == 0 && y == 0)
//                     continue; // Skip self
//
//                 int checkX = position.x + x;
//                 int checkY = position.y + y;
//
//                 if (checkX >= 0 && checkX < Dimensions.x && checkY >= 0 && checkY < Dimensions.y)
//                 {
//                     result.Add(new int2(checkX, checkY));
//                 }
//             }
//         }
//
//         return result.ToArray();
//     }
//
//     private int2 NextBest()
//     {
//         int bestScore = int.MaxValue;
//         int2 bestPoint = OpenList[0];
//         foreach (var point in OpenList)
//         {
//             var score = getFScore(point);
//             if (score < bestScore)
//             {
//                 bestPoint = point;
//                 bestScore = score;
//             }
//         }
//
//
//         return bestPoint;
//     }
// }

