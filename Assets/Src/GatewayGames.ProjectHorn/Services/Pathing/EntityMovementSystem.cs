﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using System;
using Gateway.ProjectHorn.Systems.Entities.Components;

namespace Gateway.ProjectHorn.Services.Pathing
{
    public struct EntityPathIndex : IEquatable<EntityPathIndex>
    {
        public int ID;
        public int Index;

        public bool Equals(EntityPathIndex other)
        {
            return (ID == other.ID && Index == other.Index);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode() ^ Index.GetHashCode();
        }
    }

    public class EntityMovementSystem : JobComponentSystem
    {
        private EntityManager m_entityManager => World.DefaultGameObjectInjectionWorld.EntityManager;

        public float m_gridSpacing = 1.1f;
        public float m_gridScale = 1;

        private EntityQuery navigatorPointQuery;

        protected override void OnCreate()
        {
            base.OnCreate();
            navigatorPointQuery = GetEntityQuery(typeof(EntityPathPoint));
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            float
                spacing = (m_gridSpacing * m_gridScale); // Make node spacing relative to established in inspector scale
            float3 offset = new float3(spacing * .5f, spacing * .5f, 0); // Ground path at the center of nodes
            NativeList<Entity> removeList = new NativeList<Entity>(Allocator.Temp);
            NativeHashMap<EntityPathIndex, int2> paths = new NativeHashMap<EntityPathIndex, int2>(10, Allocator.Temp);
            NativeArray<EntityPathPoint> allWaypoints =
                navigatorPointQuery.ToComponentDataArray<EntityPathPoint>(Allocator.Temp);
            foreach (var waypoint in allWaypoints)
            {
                var index = new EntityPathIndex { ID = waypoint.PathID, Index = waypoint.PathIndex };
                if (!paths.ContainsKey(index))
                {
                    paths.Add(index, waypoint.Position);
                }
            }

            Entities.ForEach((ref Translation translation, ref EntityPathNavigator navigator, ref Entity entity) =>
            {
                var index = new EntityPathIndex { ID = navigator.PathID, Index = navigator.CurrentPathIndex };
                if (paths.ContainsKey(index))
                {
                    int2 waypoint = paths[index];
                    float3 currentPos = translation.Value;
                    float3 waypointPos = new float3(waypoint.x + 0.5f, waypoint.y + 0.5f, 0);
                    float3 targetPos = Vector3.Lerp(currentPos,
                        waypointPos,
                        UnityEngine.Time.deltaTime * 1.5f); // ((waypointPos * spacing) - offset) * 0.05f;
                    float distance = Vector3.Distance(currentPos, waypointPos);
                    targetPos.z = -0.1f;

                    translation.Value = targetPos;

                    //UnityEngine.Debug.Log(string.Format("{0}", distance));
                    if (distance <= 0.25f)
                    {
                        var nextIndex = new EntityPathIndex
                            { ID = navigator.PathID, Index = navigator.CurrentPathIndex + 1 };
                        if (!paths.ContainsKey(nextIndex))
                        {
                            removeList.Add(entity);
                        }
                        else
                        {
                            navigator.CurrentPathIndex++;
                        }
                    }
                }
            }).Run();

            foreach (var entity in removeList)
            {
                m_entityManager.RemoveComponent<EntityPathNavigator>(entity);
            }

            removeList.Dispose();
            paths.Dispose();

            return inputDeps;
        }
    }
}