﻿

/*public class EntityPathFindingSystem : SystemBase
{
    private EndSimulationEntityCommandBufferSystem m_commandBuffer;
    private bool m_created;

    protected override void OnCreate()
    {
        base.OnCreate();

        m_commandBuffer = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var currentCommandBuffer = m_commandBuffer.CreateCommandBuffer().ToConcurrent();
        EntityManager entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        int2 dimensions = Terrain.Tiles.Dimensions.ToInt2();
        int pathCount = Pathing.Paths.Count;
        var AllNodes = Terrain.NODE_DATA;

        
        Entities
            .WithName("Find_Entity_Path")
            .ForEach((int entityInQueryIndex, ref EntityPathRequest pathRequest, ref Entity entity) =>
        {
            NativeHashMap<int2, Node> nodes = new NativeHashMap<int2, Node>(AllNodes.Count(), Allocator.TempJob);
            for (int x = 0; x < dimensions.x; x++)
            {
                for (int y = 0; y < dimensions.y; y++)
                {
                    int2 key = new int2(x, y);
                    nodes[key] = AllNodes[key];
                }
            }

            Node startNode = nodes[pathRequest.Start];
            Node targetNode = nodes[pathRequest.Target];
            NativeList<int2> openList = new NativeList<int2>(Allocator.TempJob);
            NativeList<int2> closedList = new NativeList<int2>(Allocator.TempJob);

            openList.Add(startNode.Position);
            int attempts = 0;
            while (openList.Length > 0 && attempts < 20000)
            {
                attempts++;
                Node currentNode = GetBestNode(nodes[openList[0]], openList, nodes);

                //UnityEngine.Debug.Log("+ Current Node Position: " + currentNode.Position);

                openList.Remove(currentNode.Position);
                closedList.Add(currentNode.Position);

                if (currentNode.Equals(targetNode))
                {
                    NativeList<int2> result = Complete(startNode, targetNode, nodes);
                    if (result.Length > 0)
                    {
                        int pathIndex = 0;
                        int pathID = pathCount;
                        foreach (int2 point in result)
                        {
                            UnityEngine.Debug.Log(string.Format("{0}, {1}", point.x, point.y));
                            //var e = currentCommandBuffer.CreateEntity(entityInQueryIndex);
                            //currentCommandBuffer.AddComponent(entityInQueryIndex, e, new EntityPathPoint
                            //{
                            //    PathIndex = pathIndex,
                            //    Position = point,
                            //    PathID = pathID
                            //});
                            //pathIndex++;
                        }
                        //currentCommandBuffer.AddComponent(entityInQueryIndex, entity, new EntityPathNavigator
                        //{
                        //    PathID = pathID,
                        //    CurrentPathIndex = 0
                        //});
                        //currentCommandBuffer.RemoveComponent<EntityPathRequest>(entityInQueryIndex, entity);

                    }
                    result.Dispose();
                    nodes.Dispose();
                    return;
                }

                foreach (Node neighbor in GetNeighbors(currentNode, nodes, dimensions))
                {
                    //UnityEngine.Debug.Log(neighbor.Position);
                    if (!neighbor.Walkable || closedList.Contains(neighbor.Position))
                    {
                        continue;
                    }

                    int newCost = currentNode.gCost + GetDistance(currentNode, neighbor);
                    if (newCost < neighbor.gCost || !openList.Contains(neighbor.Position))
                    {
                        nodes[neighbor.Position] = new Node
                        {
                            gCost = newCost,
                            hCost = GetDistance(neighbor, targetNode),
                            ParentIndex = currentNode.Position,
                            Position = neighbor.Position,
                            Walkable = true
                        };

                        int totalCost = nodes[neighbor.Position].hCost + newCost;
                        //UnityEngine.Debug.Log(neighbor.Position + " hcost: " + Nodes[neighbor.Position].hCost + " / gCost: " + newCost + " = " + totalCost);

                        if (!openList.Contains(neighbor.Position))
                        {
                            openList.Add(neighbor.Position);
                        }
                    }
                }
            }
        }).Schedule();

    }

    public static NativeList<int2> Complete(Node startNode, Node targetNode, NativeHashMap<int2, Node> nodes)
    {
        //UnityEngine.Debug.Log("TargetNode has parent " + Nodes[targetNode.Position].ParentIndex);
        NativeList<int2> result = new NativeList<int2>();
        int attempts = 0;
        Node traceNode = nodes[targetNode.Position];
        while (!traceNode.Equals(startNode) && attempts < 100)
        {
            attempts++;
            result.Add(traceNode.Position);
            traceNode = nodes[traceNode.ParentIndex];
            //UnityEngine.Debug.Log(traceNode.Position + " has parent " + Nodes[traceNode.ParentIndex].Position);
        }
        result.Reverse();
        return result;
        //UnityEngine.Debug.Log("- Path Finished with length: " + Result.Length);
    }

    public static Node GetBestNode(Node current, NativeList<int2> openList, NativeHashMap<int2, Node> nodes)
    {
        //UnityEngine.Debug.Log("? Get best Node()");
        Node best = current;
        foreach (int2 point in openList)
        {
            Node node = nodes[point];
            //UnityEngine.Debug.Log(node.Position + " cost: " + node.fCost);
            if (node.fCost < best.fCost || node.fCost == best.fCost && node.hCost < best.hCost)
            {
                best = node;
            }
        }
        //UnityEngine.Debug.Log("! Best Node Found: " + best.Position + " with cost " + best.fCost);
        return best;
    }

    public static int GetDistance(Node nodeA, Node nodeB)
    {
        int dstX = math.abs(nodeA.Position.x - nodeB.Position.x);
        int dstY = math.abs(nodeA.Position.y - nodeB.Position.y);

        //UnityEngine.Debug.Log("DistX: " + dstX);
        //UnityEngine.Debug.Log("DistY: " + dstY);

        return dstX + dstY;
        //if (dstX > dstY)
        //    return 14 * dstY + 10 * (dstX - dstY);
        //return 14 * dstX + 10 * (dstY - dstX);
    }

    public static Node[] GetNeighbors(Node node, NativeHashMap<int2, Node> nodes, int2 dimensions)
    {
        NativeList<Node> result = new NativeList<Node>(Allocator.Temp);

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                    continue; // Skip self


                int checkX = node.Position.x + x;
                int checkY = node.Position.y + y;

                if (checkX >= 0 && checkX < dimensions.x && checkY >= 0 && checkY < dimensions.y)
                {
                    result.Add(nodes[new int2(checkX, checkY)]);
                }
            }
        }

        return result.ToArray();
    }
}*/