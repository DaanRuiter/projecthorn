﻿namespace Gateway.ProjectHorn.Services.Pathing
{
    // public struct EntityNavigationPath
    // {
    //     public int2[] Waypoints;
    // }
    //
    // public static class PathingService
    // {
    //     public static List<EntityNavigationPath> Paths = new List<EntityNavigationPath>();
    //     
    //     private static EntityManager m_entityManager => World.DefaultGameObjectInjectionWorld.EntityManager;
    //
    //     private static float m_gridSpacing = 1.1f;
    //     private static float m_gridScale = 1;
    //
    //     public static void UpdateWaypoints()
    //     {
    //         foreach (var waypoint in Waypoints)
    //         {
    //             float spacing = (m_gridSpacing * m_gridScale); // Make node spacing relative to established in inspector scale
    //             float3 offset = new float3(spacing * .5f, spacing * .5f, 0); // Ground path at the center of nodes
    //             Entity entity = waypoint.Entity;
    //             float3 currentPos = m_entityManager.GetComponentData<Translation>(entity).Value;
    //             float3 waypointPos = new float3(waypoint.Waypoints[0].x, waypoint.Waypoints[0].y, 0);
    //             float3 diff = waypointPos - currentPos * 0.001f;
    //             float3 targetPos = currentPos + new float3(0, 0.0001f, 0);
    //             targetPos.z = -0.01f;
    //
    //             m_entityManager.SetComponentData(entity, new Translation
    //             {
    //                 Value = targetPos
    //             });
    //
    //             float distance = Vector3.Distance(currentPos, targetPos);
    //             if (distance <= 0.05f)
    //             {
    //                 if (waypoint.Waypoints.Count <= 0)
    //                 {
    //                     waypoint.Waypoints.RemoveAt(0);
    //                 }
    //             }
    //         }
    //     }
    // }
}