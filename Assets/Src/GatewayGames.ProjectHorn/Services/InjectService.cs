﻿using System;
using JetBrains.Annotations;

/**
 * 08/11/2020
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Services
{
    [MeansImplicitUse(ImplicitUseKindFlags.Assign)]
    public class InjectService : Attribute
    {
    }

    public class RegisterUIElementFactory : Attribute
    {
    }
}