﻿using System.Collections.Generic;
using Gateway.ProjectHorn.Services.Serialization.Definitions;
using Gateway.ProjectHorn.Services.Serialization.Definitions.Graphics;
using Gateway.ProjectHorn.Systems.Building;
using Gateway.ProjectHorn.Systems.Terrain.Tile;
using Gateway.ProjectHorn.Util;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Rendering;
using Object = UnityEngine.Object;

/**
 * 06/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Services.Graphics
{
    public class GraphicsService : ServiceBase
    {
        private const string entityMaterialResourcePath = "materials/building";

        private Material m_entitySourceMaterial;
        private Dictionary<GraphicsTextureDef, Material> m_entityMaterials;
        private Dictionary<GraphicsTextureDef, Mesh> m_entityMeshes;
        private Dictionary<Entity, Material> m_entityInstancedMaterials;

        public Mesh CreateSingleMeshForTiles(TerrainTile[] tiles)
        {
            CombineInstance[] tileMeshes = new CombineInstance[tiles.Length];

            GameObject posTransform = new GameObject("localMeshTransform");
            for (int i = 0; i < tileMeshes.Length; i++)
            {
                var tilePos = tiles[i].GetPosition();
                posTransform.transform.position = tilePos.ToVector3();
                tileMeshes[i].mesh = BuildQuad(1, 1);
                tileMeshes[i].transform = posTransform.transform.localToWorldMatrix;
            }

            Object.Destroy(posTransform);

            var combinedMesh = new Mesh();
            combinedMesh.CombineMeshes(tileMeshes);
            return combinedMesh;
        }

        public Mesh CreateTileRangeMesh(SpawnerDef spawnerDef)
        {
            int range = spawnerDef.SpawnRange;
            range = Mathf.Clamp(range, 1, Mathf.Abs(range));
            int pos = -Mathf.Abs(range / 2);
            return BuildQuad(range + 1, range + 1, pos, pos);
        }

        // public RenderMeshDescription CreateRenderMeshDescription(GraphicsTextureDef graphicsDef)
        // {
        //     return new RenderMeshDescription(GetEntityMesh(graphicsDef), GetBuildingMaterial(graphicsDef));
        // }

        public RenderMesh CreateEntityRenderMesh(GraphicsTextureDef graphicsDef)
        {
            return new RenderMesh()
            {
                mesh = GetEntityMesh(graphicsDef),
                material = GetBuildingMaterial(graphicsDef),
                castShadows = ShadowCastingMode.On,
                receiveShadows = true,
                layer = graphicsDef.Layer
            };
        }

        public RenderMesh CreateEntityRenderMesh(GraphicsTextureDef graphicsDef, Mesh mesh)
        {
            return new RenderMesh()
            {
                mesh = mesh,
                material = GetBuildingMaterial(graphicsDef),
                castShadows = ShadowCastingMode.On,
                receiveShadows = true
            };
        }

        public Material GetInstancedMaterial(Entity entity, GraphicsTextureDef graphics)
        {
            if (!m_entityInstancedMaterials.ContainsKey(entity))
            {
                var material = new Material(GetBuildingMaterial(graphics));

                m_entityInstancedMaterials.Add(entity, material);

                return material;
            }

            return m_entityInstancedMaterials[entity];
        }

        /// <returns>True if the entity has an instanced material, false if using shared material</returns>
        public bool DestroyInstancedMaterial(Entity entity)
        {
            if (m_entityInstancedMaterials.ContainsKey(entity))
            {
                var material = m_entityInstancedMaterials[entity];
                Object.Destroy(material);
                m_entityInstancedMaterials.Remove(entity);

                return true;
            }

            return false;
        }

        public Material GetBuildingMaterial(GraphicsTextureDef def)
        {
            if (m_entityMaterials.ContainsKey(def))
            {
                return m_entityMaterials[def];
            }

            var material = new Material(m_entitySourceMaterial);
            material.mainTexture = def.Texture;
            material.mainTexture.name = def.Texture.name;
            material.renderQueue += def.Layer;
            m_entityMaterials.Add(def, material);
            return material;
        }

        public static void SetEntityScale(Entity entity, float2 scale)
        {
            var position = entity.GetPosition3D().ToVector3();
            var localTo = Main.EntityManager.GetComponentData<LocalToWorld>(entity);
            var matrix = Matrix4x4.TRS(position, Quaternion.identity, scale.ToVector3());
            matrix.m22 = 1f;
            localTo.Value = matrix;
            Main.EntityManager.SetComponentData(entity, localTo);
        }

        internal override void OnStart()
        {
            m_entitySourceMaterial = Resources.Load<Material>(entityMaterialResourcePath);
            m_entityMaterials = new Dictionary<GraphicsTextureDef, Material>();
            m_entityMeshes = new Dictionary<GraphicsTextureDef, Mesh>();
            m_entityInstancedMaterials = new Dictionary<Entity, Material>();
        }

        internal override void OnUpdate()
        {
        }

        internal override void OnStop()
        {
        }

        private Mesh BuildQuad(float width, float height, int x = 0, int y = 0)
        {
            var mesh = new Mesh();

            var vertices = new[]
            {
                new Vector3(x, y, 0),
                new Vector3(x + width, y, 0),
                new Vector3(x, y + height, 0),
                new Vector3(x + width, y + height, 0)
            };
            mesh.vertices = vertices;

            int[] tris = new[]
            {
                0, 2, 1,
                2, 3, 1
            };
            mesh.triangles = tris;

            var normals = new[]
            {
                Vector3.back,
                Vector3.back,
                Vector3.back,
                Vector3.back
            };
            mesh.normals = normals;

            var uv = new[]
            {
                new Vector2(0, 0),
                new Vector2(1, 0),
                new Vector2(0, 1),
                new Vector2(1, 1)
            };
            mesh.uv = uv;

            return mesh;
        }

        private Mesh GetEntityMesh(GraphicsTextureDef def)
        {
            if (m_entityMeshes.ContainsKey(def))
            {
                return m_entityMeshes[def];
            }

            var nodeSize = def.NodeSize.x > 0f ? def.NodeSize : new float2(1, 1);
            var meshSize = nodeSize / District.NodeGridSize;
            var mesh = BuildQuad(meshSize.x, meshSize.y);
            m_entityMeshes.Add(def, mesh);
            return mesh;
        }
    }
}