﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

/**
 * 08/11/2020
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Services
{
    public static class ServiceInjector
    {
        private static Dictionary<int, ServiceBase> _services;

        static ServiceInjector()
        {
            _services = new Dictionary<int, ServiceBase>();
        }

        public static Type[] GetTypesWithAttribute(Type attributeType)
        {
            var assembly = Assembly.GetAssembly(typeof(Main));

            return assembly.GetTypes()
                .Where(m => m.GetCustomAttributes(attributeType, false).Length > 0)
                .ToArray();
        }

        public static void InjectServices(object target)
        {
            var injectionTargets = new[] { target.GetType() }
                .SelectMany(t =>
                    t.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance |
                                BindingFlags.Static))
                .Where(m => m.GetCustomAttributes(typeof(InjectService), false).Length > 0)
                .ToArray();

            for (int i = 0; i < injectionTargets.Length; i++)
            {
                if (injectionTargets[i].GetValue(target) == null)
                {
                    var service = GetService(injectionTargets[i].FieldType);

                    injectionTargets[i].SetValue(target, service);
                }
            }
        }

        public static ST LoadService<ST>() where ST : ServiceBase => GetService<ST>();

        public static ST GetService<ST>() where ST : ServiceBase
        {
            var type = typeof(ST);
            return (ST)GetService(type);
        }

        public static object GetService(Type serviceType)
        {
            int hashCode = serviceType.GetHashCode();

            if (!_services.ContainsKey(hashCode))
            {
                return AddServiceUntyped(hashCode, serviceType);
            }

            return _services[hashCode];
        }

        private static ST AddService<ST>(ST service) where ST : ServiceBase
        {
            int hash = typeof(ST).GetHashCode();

            _services.Add(hash, service);

            return service;
        }

        private static object AddServiceUntyped(int hashCode, Type serviceType)
        {
            Debug.Log("Creating service: " + serviceType.Name + "(hashCode:" + hashCode + ")");

            var service = ((ServiceBase)Activator.CreateInstance(serviceType)).Initialize();

            _services.Add(hashCode, service);

            InjectServices(service);

            service.OnStart();

            return service;
        }

        public static void UpdateServices()
        {
            foreach (var service in _services)
            {
                service.Value.OnUpdate();
            }
        }

        public static void DisposeServices()
        {
            foreach (var service in _services)
            {
                service.Value.OnStop();
            }
        }
    }
}