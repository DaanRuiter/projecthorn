﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Gateway.ProjectHorn.Services.Serialization.Definitions;
using Gateway.ProjectHorn.Services.Serialization.Definitions.Building;
using Gateway.ProjectHorn.Services.Serialization.Definitions.District;
using Gateway.ProjectHorn.Services.Serialization.Definitions.DistrictNode;
using Gateway.ProjectHorn.Services.Serialization.Definitions.Graphics;
using Gateway.ProjectHorn.Services.Serialization.Definitions.Item;
using UnityEngine;

namespace Gateway.ProjectHorn.Services.Serialization
{
    public class SerializationService : ServiceBase
    {
        private const string FolderUpMarker = "..";

        public static readonly string dataFolder = Application.dataPath + "/../Gamedata/";

        public SerializationService(bool injectServices) : base(injectServices)
        {
        }

        public SerializationService()
        {
        }

        public DT LoadDefinition<DT>(string pathToFile) where DT : BaseDef
        {
            pathToFile = CleanPath(pathToFile);

            DT def = JsonUtility.FromJson<DT>(File.ReadAllText(pathToFile));
            def.Create(pathToFile);

            return def;
        }

        public EntityDef LoadEntityDefinition(string pathToFile, Type definitionType)
        {
            pathToFile = CleanPath(pathToFile);

            EntityDef def = (EntityDef)JsonUtility.FromJson(File.ReadAllText(pathToFile), definitionType);
            def.Create(pathToFile);

            return def;
        }

        public DT[] LoadDefinitions<DT>(string localPath, bool includeSubFolders = true) where DT : BaseDef
        {
            List<DT> result = new List<DT>();
            List<string> filesToLoad = new List<string>(Directory.GetFiles(dataFolder + localPath, "*.json"));

            if (includeSubFolders)
            {
                string[] subfolders = Directory.GetDirectories(dataFolder + localPath);
                foreach (string folder in subfolders)
                {
                    filesToLoad.AddRange(Directory.GetFiles(folder, "*.json"));
                }
            }

            foreach (string dataFilename in filesToLoad)
            {
                result.Add(LoadDefinition<DT>(dataFilename));
            }

            return result.ToArray();
        }

        public EntityDef[] LoadEntityDefinitions(string localPath, Type definitionType, bool includeSubFolders = true)
        {
            List<EntityDef> result = new List<EntityDef>();
            string folderPath = dataFolder + localPath;

            if (!Directory.Exists(folderPath))
            {
                ErrorF("Could not load entity type {0}, folder does not exist: {1}", definitionType.Name, folderPath);
                return new EntityDef[0];
            }

            List<string> filesToLoad = new List<string>(Directory.GetFiles(folderPath, "*.json"));

            if (includeSubFolders)
            {
                string[] subfolders = Directory.GetDirectories(dataFolder + localPath);
                foreach (string folder in subfolders)
                {
                    filesToLoad.AddRange(Directory.GetFiles(folder, "*.json"));
                }
            }

            foreach (string dataFilename in filesToLoad)
            {
                result.Add(LoadEntityDefinition(dataFilename, definitionType));
            }

            return result.ToArray();
        }

        public Texture2D LoadTexture(Graphics2DDef graphicsDefinition)
        {
            try
            {
                var texture = new Texture2D(graphicsDefinition.GraphicDimensions.x,
                    graphicsDefinition.GraphicDimensions.y);
                texture.LoadImage(File.ReadAllBytes(graphicsDefinition.DataPath));
                texture.filterMode = FilterMode.Point;
                texture.name = GetFileName(graphicsDefinition.GraphicDataPath);
                return texture;
            }
            catch (FileNotFoundException exception)
            {
                Debug.LogError("Could not find graphics file at: " + graphicsDefinition.DataPath);
                Debug.LogError(exception);
            }

            return null;
        }

        public Sprite LoadSprite(Graphics2DDef graphicsDefinition)
        {
            var texture = LoadTexture(graphicsDefinition);
            var result = Sprite.Create(texture,
                new Rect(0, 0, graphicsDefinition.GraphicDimensions.x, graphicsDefinition.GraphicDimensions.y),
                Vector2.zero,
                graphicsDefinition.GraphicDimensions.x);
            result.name = result.texture.name;
            return result;
        }

        public Sprite[] LoadSprites(Graphics2DDef[] graphicsDefinitions)
        {
            Sprite[] result = new Sprite[graphicsDefinitions.Length];

            int i = 0;
            foreach (Graphics2DDef def in graphicsDefinitions)
            {
                result[i] = LoadSprite(def);
                i++;
            }

            return result;
        }

        public TileDef[] LoadTiles()
        {
            TileDef[] result = LoadDefinitions<TileDef>(TileDef.DefinitionPath);

            foreach (TileDef tile in result)
            {
                tile.Graphics.Sprite = LoadSprite(tile.Graphics);
                tile.Tile.sprite = tile.Graphics.Sprite;
            }

            return result;
        }

        public DistrictDef[] LoadDistricts()
        {
            var defaultDistrictDef = new DistrictDef();
            DistrictDef[] result = LoadDefinitions<DistrictDef>(defaultDistrictDef.DefinitionPath);

            foreach (DistrictDef district in result)
            {
                district.Graphics.Texture = LoadTexture(district.Graphics);
            }

            return result;
        }

        public DistrictNodeDef[] LoadDistrictsNodes()
        {
            var defaultDistrictDef = new DistrictNodeDef();
            DistrictNodeDef[] result = LoadDefinitions<DistrictNodeDef>(defaultDistrictDef.DefinitionPath);

            foreach (DistrictNodeDef district in result)
            {
                district.Graphics.Texture = LoadTexture(district.Graphics);
            }

            return result;
        }

        public BuildingDef[] LoadBuildings()
        {
            var defaultBuildingDef = new BuildingDef();
            BuildingDef[] result = LoadDefinitions<BuildingDef>(defaultBuildingDef.DefinitionPath);

            foreach (BuildingDef building in result)
            {
                building.Graphics.Texture = LoadTexture(building.Graphics);
            }

            return result;
        }

        //todo: move load code to definition (pass def service)
        public ItemDef[] LoadItems()
        {
            var defaultItemDef = new ItemDef();
            ItemDef[] result = LoadDefinitions<ItemDef>(defaultItemDef.DefinitionPath);

            foreach (ItemDef item in result)
            {
                item.Graphics.Texture = LoadTexture(item.Graphics);
            }

            return result;
        }

        public ActorDef[] LoadActors()
        {
            var defaultActorDef = new ActorDef();
            ActorDef[] result = LoadDefinitions<ActorDef>(defaultActorDef.DefinitionPath);

            foreach (ActorDef actor in result)
            {
                actor.Graphics.Texture = LoadTexture(actor.Graphics);
            }

            return result;
        }

        public TerrainDef[] LoadTerrain()
        {
            return LoadDefinitions<TerrainDef>(TerrainDef.DefinitionPath);
        }

        public void LoadAllEntityDefinitions(DefinitionService definitionService)
        {
            var type = typeof(EntityDef);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => p.IsSubclassOf(type)).ToArray();

            for (int i = 0; i < types.Length; i++)
            {
                var defaultDef = (EntityDef)Activator.CreateInstance(types[i]);
                EntityDef[] result = LoadEntityDefinitions(defaultDef.DefinitionPath, types[i]);

                foreach (EntityDef entity in result)
                {
                    entity.Graphics.Texture = LoadTexture(entity.Graphics);
                }

                definitionService.AddDefinitions(types[i], result);
            }
        }

        // Helpers

        public static string[] GetLocalFiles(string localPath, string searchPattern = null)
        {
            return Directory.GetFiles(ConvertLocalPath(localPath), searchPattern);
        }

        public static string ConvertLocalPath(string localPath)
        {
            return dataFolder + localPath;
        }

        public static string GetDirectory(string path)
        {
            return Path.GetDirectoryName(path);
        }

        public static string GetFileName(string path)
        {
            var split = path.Split(Path.DirectorySeparatorChar);
            return (split.Length > 0) ? split[split.Length - 1] : path;
        }

        public static string CleanPath(string path)
        {
            string[] split = path.Replace("\\", "/").Split('/');
            string result = string.Empty;
            int folderUpMarkerCount = 0;

            for (int i = split.Length - 1; i >= 0; i--)
            {
                if (split[i] == FolderUpMarker)
                {
                    folderUpMarkerCount++;
                }
                else if (folderUpMarkerCount == 0)
                {
                    string seperator = i - 1 >= 0 ? "" + Path.DirectorySeparatorChar : string.Empty;
                    result = seperator + split[i] + result;
                }
                else
                {
                    folderUpMarkerCount--;
                }
            }

            return result;
        }

        public static string ReplaceFileName(string path, string newFileName)
        {
            string result = Path.GetDirectoryName(path) ?? string.Empty;

            if (result[result.Length - 1] != Path.DirectorySeparatorChar)
            {
                result += Path.DirectorySeparatorChar;
            }

            return result + newFileName;
        }

        internal override void OnStart()
        {
        }

        internal override void OnUpdate()
        {
        }

        internal override void OnStop()
        {
        }
    }
}