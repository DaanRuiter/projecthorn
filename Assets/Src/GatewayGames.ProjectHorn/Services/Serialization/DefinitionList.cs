﻿using System.Collections.Generic;
using System;
using Gateway.ProjectHorn.Services.Serialization.Definitions;

namespace Gateway.ProjectHorn.Services.Serialization
{
    public class DefinitionList<DT> where DT : BaseDef
    {
        public Type DefinitionType
        {
            get { return typeof(DT); }
        }

        public List<DT> Items { get; }

        public DefinitionList()
        {
            Items = new List<DT>();
        }
    }
}