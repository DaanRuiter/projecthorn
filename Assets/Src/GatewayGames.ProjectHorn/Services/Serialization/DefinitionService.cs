﻿using System.Collections.Generic;
using System;
using Gateway.ProjectHorn.Services.Serialization.Definitions;
using UnityEngine;

namespace Gateway.ProjectHorn.Services.Serialization
{
    public class DefinitionService : ServiceBase
    {
        private static Dictionary<Type, Action> LoadCoreDefinitionsActionTable = new Dictionary<Type, Action>();

        private readonly Dictionary<Type, List<BaseDef>> m_definitions = new Dictionary<Type, List<BaseDef>>();

        private readonly Dictionary<uint, int> m_uidMap = new Dictionary<uint, int>();

        [InjectService] private SerializationService m_serializationService;

        private uint m_uidCounter = 1; // 0 = empty

        public DefinitionService(bool injectServices) : base(injectServices)
        {
        }

        public DefinitionService()
        {
        }

        public Dictionary<Type, List<BaseDef>> GetAllDefinitions()
        {
            return new Dictionary<Type, List<BaseDef>>(m_definitions);
        }

        public DT GetDefinition<DT>(string name, bool supressWarnings = false) where DT : BaseDef
        {
            var list = GetDefinitionList(typeof(DT));

            foreach (var baseDef in list)
            {
                var def = (DT)baseDef;
                if (def.Equals(name))
                {
                    return def;
                }
            }

            if (!supressWarnings)
            {
                Log("could not find definition with name " + name);
            }

            return null;
        }

        public DT GetDefinitionByUID<DT>(uint uid) where DT : BaseDef
        {
            if (m_uidMap.ContainsKey(uid))
            {
                int index = m_uidMap[uid];

                return (DT)m_definitions[typeof(DT)][index];
            }

            Debug.LogWarning("Failed to find definition with UID: " + uid);

            return null;
        }

        public DT[] GetDefinitions<DT>() where DT : BaseDef
        {
            var definitions = GetDefinitionList(typeof(DT));
            var result = new DT[definitions.Count];

            for (int i = 0; i < definitions.Count; i++)
            {
                result[i] = (DT)Convert.ChangeType(definitions[i], typeof(DT));
            }

            return result;
        }

        public void AddDefinitions(Type definitionType, EntityDef[] definitions, bool bypassLoadCheck = false)
        {
            for (int i = 0; i < definitions.Length; i++)
            {
                var definition = definitions[i];
                var list = GetDefinitionList(definitionType, bypassLoadCheck);
                if (!list.Contains(definition))
                {
                    list.Add(definition);

                    uint uid = m_uidCounter;
                    m_uidMap.Add(uid, list.Count - 1);
                    m_uidCounter++;

                    Log("Loaded definition: " + definition.Name);

                    definition.SetUID(uid);
                }
            }
        }

        public void AddDefinition<DT>(DT definition, bool bypassLoadCheck = false) where DT : BaseDef
        {
            if (GetDefinition<DT>(definition.Name, true) == null) //todo: optimise
            {
                var list = GetDefinitionList(typeof(DT), bypassLoadCheck);

                list.Add(definition);

                uint uid = m_uidCounter;
                m_uidMap.Add(uid, list.Count - 1);
                m_uidCounter++;

                Log("Loaded definition: " + definition.Name);

                definition.SetUID(uid);
            }
        }

        public void AddDefinitions<DT>(DT[] definitions, bool bypassLoadCheck = false) where DT : BaseDef
        {
            foreach (DT definition in definitions)
            {
                AddDefinition(definition, bypassLoadCheck);
            }
        }

        public void LoadDefinitionsOfType(Type defType)
        {
            if (!HasLoadedType(defType) && LoadCoreDefinitionsActionTable.ContainsKey(defType))
            {
                m_definitions.Add(defType, new List<BaseDef>());
                LoadCoreDefinitionsActionTable[defType].Invoke();
            }
        }

        // Internal

        internal override void OnStart()
        {
            LoadCoreDefinitionsActionTable = new Dictionary<Type, Action>()
            {
                { typeof(TileDef), () => AddDefinitions(m_serializationService.LoadTiles(), true) },
                { typeof(TerrainDef), () => AddDefinitions(m_serializationService.LoadTerrain(), true) },
            };

            m_serializationService.LoadAllEntityDefinitions(this);
        }

        internal void LoadCoreDefinitions()
        {
            foreach (var loadAction in LoadCoreDefinitionsActionTable)
            {
                LoadDefinitionsOfType(loadAction.Key);
            }
        }

        internal override void OnUpdate()
        {
        }

        internal override void OnStop()
        {
        }

        // Private
        private List<BaseDef> GetDefinitionList(Type defType, bool bypassLoadCheck = false)
        {
            if (!bypassLoadCheck && !HasLoadedType(defType) && LoadCoreDefinitionsActionTable.ContainsKey(defType))
            {
                LoadDefinitionsOfType(defType);
            }

            if (!m_definitions.ContainsKey(defType))
            {
                m_definitions.Add(defType, new List<BaseDef>());
            }

            return m_definitions[defType];
        }

        private bool HasLoadedType(Type type)
        {
            return m_definitions.ContainsKey(type);
        }
    }
}