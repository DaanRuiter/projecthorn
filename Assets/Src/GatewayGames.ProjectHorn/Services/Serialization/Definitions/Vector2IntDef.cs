﻿using UnityEngine;

/**
 * 07/11/2020
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Services.Serialization.Definitions
{
    [System.Serializable]
    public class Vector2IntDef : BaseDef
    {
        public int x;
        public int y;

        public Vector2IntDef(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public static implicit operator Vector2Int(Vector2IntDef v)
        {
            return new Vector2Int(v.x, v.y);
        }

        public static implicit operator Vector3Int(Vector2IntDef v)
        {
            return new Vector3Int(v.x, v.y, 0);
        }

        public static implicit operator Vector2(Vector2IntDef v)
        {
            return new Vector2(v.x, v.y);
        }

        public static implicit operator Vector3(Vector2IntDef v)
        {
            return new Vector3(v.x, v.y, 0);
        }

        public override string ToString()
        {
            return string.Format("({0}, {1})", x, y);
        }
    }
}