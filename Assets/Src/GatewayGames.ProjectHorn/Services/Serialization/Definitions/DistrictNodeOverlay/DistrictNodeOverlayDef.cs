using Gateway.ProjectHorn.Definitions;
using Gateway.ProjectHorn.Systems.Entities.Components.Common;
using Gateway.ProjectHorn.Systems.Entities.Components.NodeOverlays;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

/**
 * 03/07/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Services.Serialization.Definitions.DistrictNodeOverlay
{
    public enum DistrictNodeOverlayType
    {
        Single,
        Tiled,
        Particle
    }

    public class DistrictNodeOverlayDef : EntityDef
    {
        public override string DefinitionPath => "district_nodes_overlays/";

        public EnumDef DistrictNodeOverlayType;
        public uint MaxFillProgress;

        internal override void OnEntityCreated(EntityManager entityManager,
            Entity entity,
            float2 position,
            Vector2Int tilePosition)
        {
            base.OnEntityCreated(entityManager, entity, position, tilePosition);

            var overlayType = DistrictNodeOverlayType.AsEnum<DistrictNodeOverlayType>();
            var fillProgress = overlayType == DistrictNodeOverlay.DistrictNodeOverlayType.Single
                ? new URange(1, 0)
                : new URange(MaxFillProgress, 0);

            entityManager.AddComponentData(entity,
                new DistrictNodeOverlayData
                {
                    OverlayType = overlayType,
                    OverlayFillProgress = fillProgress
                });

            entityManager.AddComponentData(entity,
                new CompositeScale()
                {
                    Value = new float4x4(1f, 1f / fillProgress.Cap, 1f, 1f)
                });
        }
    }
}