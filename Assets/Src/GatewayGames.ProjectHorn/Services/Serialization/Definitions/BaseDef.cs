﻿using System.Linq;
using Gateway.ProjectHorn.Components.UI.Debug;
using Gateway.ProjectHorn.Util;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Gateway.ProjectHorn.Services.Serialization.Definitions
{
    [System.Serializable]
    public abstract class BaseDef : SharedObject
    {
        public string Name;
        public string[] Flags;

        public uint UID => m_uid;

        /// <summary>
        /// Returns true when this definition has been fully loaded & initialized
        /// </summary>
        public virtual bool IsDefined => m_created;

        public virtual string DataPath { get; protected set; }

        [System.NonSerialized] private bool m_created;
        [System.NonSerialized] private uint m_uid;

        public BaseDef(bool injectServices = true) : base(injectServices)
        {
        }

        public virtual void Create(string dataPath)
        {
            if (m_created) return;

            m_created = true;

            DataPath = dataPath;
        }

        public override string ToString() => Name;

        public bool HasFlag(string flag) => Flags == null ? false : Flags.Contains(flag);

        public bool Equals(BaseDef other) => Equals(other.Name);

        public bool Equals(string name) => Name?.ToLower() == name?.ToLower();

        public void InjectServices() => ServiceInjector.InjectServices(this);

        public void InspectWithPreview(EntityPreview preview)
        {
            preview.AddProperty(string.Empty, string.Format("<u>{0}</u>\n", Name));
            preview.AddProperty("UID", UID);
            preview.AddProperty("DataPath", DataPath);

            OnInspected(preview);
        }

        internal virtual Sprite GetSprite()
        {
            return null;
        }

        internal virtual Texture2D GetTexture()
        {
            return null;
        }

        internal virtual void OnInspected(EntityPreview preview)
        {
        }

        internal virtual void OnEntityCreated(EntityManager entityManager,
            Entity entity,
            float2 position,
            Vector2Int vector2Int)
        {
        }

        internal void SetUID(uint uid) => m_uid = uid;
    }
}