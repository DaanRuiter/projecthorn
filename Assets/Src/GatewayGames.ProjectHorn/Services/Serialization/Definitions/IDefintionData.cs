using Unity.Entities;

namespace GatewayGames.ProjectHorn.Services.Serialization.Definitions
{
    public interface IDefinitionData<T> where T : IComponentData
    {
        T AsData();

        void FromData();
    }
}