

using Gateway.ProjectHorn.Services.Serialization.Definitions.DistrictNode;
using Gateway.ProjectHorn.Systems.Entities;
using Gateway.ProjectHorn.Systems.Terrain;
using Gateway.ProjectHorn.Util;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

/**
 * 20/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Services.Serialization.Definitions.Prop
{
    [System.Serializable]
    public class PropNodeDef : DistrictNodeDef
    {
        public override DistrictNodeType NodeType => DistrictNodeType.Prop;

        public override string DefinitionPath => "district_prop_nodes/";

        public PropDef Prop => m_cachedPropDef;

        public string PropName;
        public int MinPropCount;
        public int MaxPropCount;

        private PropDef m_cachedPropDef;

        public override void Create(string dataPath)
        {
            base.Create(dataPath);

            m_cachedPropDef = ServiceInjector.GetService<DefinitionService>().GetDefinition<PropDef>(PropName);
        }

        internal override void OnEntityCreated(EntityManager entityManager,
            Entity entity,
            float2 position,
            Vector2Int tilePosition)
        {
            base.OnEntityCreated(entityManager, entity, position, tilePosition);

            // Spawn props
            var entitySystem = ServiceInjector.GetService<EntitySystem>();
            var propCount = Random.Range(MinPropCount, MaxPropCount);
            float halfNodeSize = Systems.Building.District.NodeLocalSize / 2f;

            for (int i = 0; i < propCount; i++)
            {
                var propPos = position.AddRandom(0, halfNodeSize);
                entitySystem.PlaceEntity(m_cachedPropDef, propPos);
            }

            // Update node
            var terrainSystem = ServiceInjector.GetService<TerrainSystem>();
            var district = terrainSystem.Tiles.GetTile(tilePosition).District;
            district.GetNodeClosestTo(position).Props = this;
        }
    }
}