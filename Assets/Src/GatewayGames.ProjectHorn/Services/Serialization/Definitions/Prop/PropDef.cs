

using Gateway.ProjectHorn.Services.Serialization.Definitions.Item;

/**
 * 20/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Services.Serialization.Definitions.Prop
{
    [System.Serializable]
    public class PropDef : EntityDef
    {
        public override string DefinitionPath => "props/";

        public ItemYieldDef ItemYield;
    }
}