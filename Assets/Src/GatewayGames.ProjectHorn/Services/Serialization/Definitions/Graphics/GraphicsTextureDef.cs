﻿using Gateway.ProjectHorn.Components.UI.Debug;
using Gateway.ProjectHorn.Services.Graphics;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using UnityEngine;

namespace Gateway.ProjectHorn.Services.Serialization.Definitions.Graphics
{
    [System.Serializable]
    public class GraphicsTextureDef : Graphics2DDef
    {
        public Texture2D Texture;
        public float2 NodeSize;
        public int Layer;

        [InjectService] private GraphicsService _graphicsService;

        internal override void OnInspected(EntityPreview preview)
        {
            preview.AddProperty("Texture", Texture.name);
            preview.AddProperty("NodeSize", NodeSize);
            preview.AddProperty("Layer", Layer);
            preview.SetTexture(Texture);
        }

        internal override void OnEntityCreated(EntityManager entityManager,
            Entity entity,
            float2 position,
            Vector2Int vector2Int)
        {
            base.OnEntityCreated(entityManager, entity, position, vector2Int);

            entityManager.AddSharedComponentData(entity, _graphicsService.CreateEntityRenderMesh(this));
            entityManager.SetComponentData(entity,
                new RenderBounds()
                {
                    Value = entityManager.GetSharedComponentData<RenderMesh>(entity).mesh.bounds.ToAABB()
                });
        }
    }
}