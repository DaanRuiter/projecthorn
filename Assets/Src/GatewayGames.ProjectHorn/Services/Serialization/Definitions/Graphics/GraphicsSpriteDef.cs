﻿using Gateway.ProjectHorn.Components.UI.Debug;
using UnityEngine;

namespace Gateway.ProjectHorn.Services.Serialization.Definitions.Graphics
{
    [System.Serializable]
    public class GraphicsSpriteDef : Graphics2DDef
    {
        public Sprite Sprite;

        internal override void OnInspected(EntityPreview preview)
        {
            preview.AddProperty("Sprite", Sprite.name);
            preview.SetImage(Sprite);
        }
    }
}