﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Gateway.ProjectHorn.Services.Serialization.Definitions.Graphics
{
    [System.Serializable]
    public class Graphics2DDef : BaseDef
    {
        public string GraphicDataPath;

        public Vector2IntDef GraphicDimensions;

        // public float Scale = 1f;

        public override void Create(string dataPath)
        {
            base.Create(dataPath);

            DataPath = SerializationService.ReplaceFileName(dataPath, GraphicDataPath);
        }

        internal override void OnEntityCreated(EntityManager entityManager,
            Entity entity,
            float2 position,
            Vector2Int vector2Int)
        {
        }
    }
}