

/**
 * 08/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Services.Serialization.Definitions.Item
{
    [System.Serializable]
    public class ItemDef : EntityDef
    {
        public override string DefinitionPath => "items/";

        public string ItemName;
    }
}