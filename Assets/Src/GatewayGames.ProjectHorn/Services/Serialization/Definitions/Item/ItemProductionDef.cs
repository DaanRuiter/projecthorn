

using System;
using Gateway.ProjectHorn.Components.UI.Debug;

/**
 * 24/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Services.Serialization.Definitions.Item
{
    [Serializable]
    public class ItemProductionDef : BaseDef
    {
        public float Duration;
        public ItemCountDef[] Intake;
        public ItemCountDef[] Product;

        internal override void OnInspected(EntityPreview preview)
        {
            base.OnInspected(preview);

            preview.AddProperty(nameof(Intake), Intake[0]);
            preview.AddProperty(nameof(Product), Product[0]);
        }
    }

    [Serializable]
    public class ItemCountDef : BaseDef
    {
        public string Item;
        public int Amount;

        public ItemDef GetItemDef(DefinitionService df) => df.GetDefinition<ItemDef>(Item);

        public override string ToString()
        {
            return string.Format("{0}x {1}", Amount, Item);
        }
    }
}