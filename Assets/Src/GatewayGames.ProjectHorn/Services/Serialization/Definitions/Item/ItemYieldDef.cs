

using Unity.Entities;

/**
 * 08/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Services.Serialization.Definitions.Item
{
    [System.Serializable]
    public class ItemYieldDef : BaseDef
    {
        public string ItemName;
        public int WorkCost;
        public int WorkYieldCount;

        [InjectService] private DefinitionService i_definitionService;

        public ItemYieldData AsData()
        {
            var itemDef = i_definitionService.GetDefinition<ItemDef>(ItemName);
            return new ItemYieldData()
            {
                ItemUID = (int)itemDef.UID,
                WorkCost = WorkCost,
                WorkYieldCount = WorkYieldCount
            };
        }
    }

    public struct ItemYieldData : IComponentData
    {
        public int ItemUID;
        public int WorkCost;
        public int WorkYieldCount;
    }
}