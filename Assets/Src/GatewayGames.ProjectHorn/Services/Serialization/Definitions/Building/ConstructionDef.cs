using System;

/**
 * 12/06/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Services.Serialization.Definitions.Building
{
    [Serializable]
    public class ConstructionDef : BaseDef
    {
        public uint WorkNeeded;
    }
}