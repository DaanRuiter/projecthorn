﻿using Gateway.ProjectHorn.Components.UI.Debug;
using Gateway.ProjectHorn.Definitions;
using Gateway.ProjectHorn.Services.Graphics;
using Gateway.ProjectHorn.Services.Serialization.Definitions.DistrictNode;
using Gateway.ProjectHorn.Services.Serialization.Definitions.Item;
using Gateway.ProjectHorn.Systems.DOTS.Building;
using Gateway.ProjectHorn.Systems.Entities.Components.Building;
using Gateway.ProjectHorn.Systems.Entities.Components.Common;
using Gateway.ProjectHorn.Systems.Terrain;
using Gateway.ProjectHorn.Util;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Gateway.ProjectHorn.Services.Serialization.Definitions.Building
{
    [System.Serializable]
    public class BuildingDef : DistrictNodeDef
    {
        public const float MinConstructionAlpha = .5f;

        public override string DefinitionPath => "buildings/";

        public override DistrictNodeType NodeType => DistrictNodeType.Building;

        public bool NeedsToBeBuilt => Construction == null ? false : Construction.WorkNeeded > 0;

        public SpawnerDef SpawnerDef;
        public EnumDef ActorRole;
        public int ActorLimit;
        public string Produces;
        public ConstructionDef Construction;
        public ItemProductionDef Production;

        [InjectService] private TerrainSystem i_terrainSystem;
        [InjectService] private GraphicsService i_graphicsService;

        public override void Create(string dataPath)
        {
            base.Create(dataPath);

            SpawnerDef.Create(dataPath);
        }

        internal override void OnInspected(EntityPreview preview)
        {
            base.OnInspected(preview);

            preview.AddProperty(nameof(ActorLimit), ActorLimit);

            if (SpawnerDef?.IsDefined ?? false)
            {
                SpawnerDef.OnInspected(preview);
            }

            if (Production?.IsDefined ?? false)
            {
                Production.OnInspected(preview);
            }
        }

        internal override void OnEntityCreated(EntityManager entityManager,
            Entity entity,
            float2 position,
            Vector2Int tilePosition)
        {
            base.OnEntityCreated(entityManager, entity, position, tilePosition);
        }

        internal virtual void OnBuildingConstructionStarted(EntityManager entityManager,
            Entity entity,
            float2 position,
            Vector2Int tilePosition)
        {
            Warning($"Started construction {Name}");

            // Set material
            var instanceMaterial = i_graphicsService.GetInstancedMaterial(entity, Graphics);
            var color = instanceMaterial.color;
            color.a = MinConstructionAlpha;
            instanceMaterial.color = color;
            entityManager.SetRenderMeshMaterial(entity, instanceMaterial);

            //Set constructionData
            entityManager.AddComponentData(entity,
                new BuildingConstructionData
                {
                    Progress = new URange(Construction.WorkNeeded, 0),
                    BuildersAssigned = new URange(ConstructionSystem.MaxBuildersPerBuilding, 0)
                });
        }

        internal virtual void OnBuildingConstructionCompleted(EntityManager entityManager,
            Entity entity,
            float2 position,
            Vector2Int tilePosition)
        {
            Warning($"Finished construction {Name}");

            if (i_graphicsService.DestroyInstancedMaterial(entity))
            {
                var sharedMaterial = i_graphicsService.GetBuildingMaterial(Graphics);

                entityManager.SetRenderMeshMaterial(entity, sharedMaterial);
            }

            entityManager.AddComponentData(entity,
                new BuildingBaseData()
                {
                    Entity = entity,
                    DefinitionUID = UID,
                    District = i_terrainSystem.Tiles[tilePosition].District.Entity,
                    ActorCount = 0,
                    ActorLimit = ActorLimit
                });

            if (SpawnerDef.IsDefined)
            {
                entityManager.AddComponentData(entity,
                    new ActorSpawner()
                    {
                        SpawnInterval = SpawnerDef.SpawnInterval,
                        LastSpawnTimestamp = Time.realtimeSinceStartup - SpawnerDef.SpawnInterval,
                        ActorUID = SpawnerDef.Spawnable.UID,
                        SpawnRange = 1, //todo: remove?
                        SpawnLimit = SpawnerDef.SpawnLimit,
                        SpawnCount = 0
                    });
            }
        }
    }
}