using System;
using Gateway.ProjectHorn.Components.UI.EntityHUD;
using Gateway.ProjectHorn.Components.UI.Item;
using Gateway.ProjectHorn.Systems.DOTS.Common;
using Gateway.ProjectHorn.Systems.Terrain;
using Gateway.ProjectHorn.Systems.UI;
using Gateway.ProjectHorn.Util;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

/**
 * 10/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Services.Serialization.Definitions.District
{
    [Serializable]
    public class DistrictDef : EntityDef
    {
        private static readonly int2 DistrictNodeSize =
            new int2(Systems.Building.District.NodeGridSize,
                Systems.Building.District.NodeGridSize);

        public override string DefinitionPath => "districts/";

        public int WorkRange;

        [InjectService] private TerrainSystem _terrainSystem;
        [InjectService] private UISystem i_uiSystem;

        public override void Create(string dataPath)
        {
            base.Create(dataPath);

            Graphics.NodeSize = DistrictNodeSize;
        }

        internal override void OnEntityCreated(EntityManager entityManager,
            Entity entity,
            float2 position,
            Vector2Int tilePosition)
        {
            base.OnEntityCreated(entityManager, entity, position, tilePosition);

            int ownerShipRadius = 1 + (WorkRange * 2);
            var tilesInRange = _terrainSystem.Tiles.GetRange(tilePosition.ToInt2(), ownerShipRadius);
            for (int i = 0; i < tilesInRange.Length; i++)
            {
                var tile = tilesInRange[i];
                entityManager.AddSharedComponentData(tile.Entity,
                    new FactionIndex()
                    {
                        FactionUID = FactionIndex.PlayerFactionUID
                    });

                Main.PlayerRealm.Tiles.AddTile(tile.GetPosition().ToInt2(), tile);
            }

            var entityHUDContainer = i_uiSystem.GetElement<DistrictItemHUDContainer>();
            var entityHUDElement = ItemStackListHUD.CreateItemStackListHUD(entityHUDContainer.transform);
            entityHUDElement.District = _terrainSystem.Tiles.GetTile(tilePosition).District;
            entityHUDElement.Hide();
            entityHUDContainer.SetEntityHUD(entity, entityHUDElement);
        }
    }
}