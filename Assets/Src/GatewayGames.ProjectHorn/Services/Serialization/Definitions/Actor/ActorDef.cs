﻿/**
 * 25/01/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

using Gateway.ProjectHorn.Components.UI.Debug;
using Gateway.ProjectHorn.Systems.Actor;
using Gateway.ProjectHorn.Systems.Entities.Components.Actor;
using Gateway.ProjectHorn.Systems.Terrain;
using Gateway.ProjectHorn.Util;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Gateway.ProjectHorn.Services.Serialization.Definitions
{
    public class ActorDef : EntityDef
    {
        public override string DefinitionPath => "actors/";

        public float MoveSpeed;

        [InjectService] private TerrainSystem i_terrainSystem;

        public override void Create(string dataPath)
        {
            base.Create(dataPath);

            Graphics.NodeSize = new float2(.35f, .35f);
        }

        internal override void OnEntityCreated(EntityManager entityManager,
            Entity entity,
            float2 position,
            Vector2Int tilePosition)
        {
            base.OnEntityCreated(entityManager, entity, position, tilePosition);

            var district = i_terrainSystem.Tiles.GetTile(tilePosition).District;
            var home = district.GetNodeClosestTo(position);

            entityManager.AddComponentData(entity,
                new ActorBaseData()
                {
                    ActorDefUID = UID,
                    ActorData = new ActorData()
                    {
                        Home = home.Entity,
                        Workplace = home.Entity,
                        District = district.Entity,
                        DistrictTilePosition = tilePosition.ToInt2(),
                        WorkplacePosition = position,
                        HomeTilePosition = position
                    }
                });

            entityManager.AddComponentData(entity,
                new ActorLocalMoveData()
                {
                    MoveSpeed = MoveSpeed,
                    CurrentTarget = position,
                    TilePosition = tilePosition.ToInt2(),
                    NextSeekTimestamp = Time.realtimeSinceStartup,
                    Wonders = false
                });

            entityManager.AddSharedComponentData(entity,
                new ActorRoleData()
                {
                    Role = ActorRole.None
                });

            entityManager.AddComponentData(entity,
                new ActorTaskComponentData
                {
                    WorkSpeed = 1
                });
        }

        internal override void OnInspected(EntityPreview preview)
        {
            base.OnInspected(preview);

            preview.AddProperty("MoveSpeed", MoveSpeed);
        }
    }
}