﻿using System;
using Gateway.ProjectHorn.Components.UI.Debug;

/**
 * 25/01/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */
namespace Gateway.ProjectHorn.Services.Serialization.Definitions
{
    [Serializable]
    public class SpawnerDef : BaseDef
    {
        public string SpawnableName;

        public float SpawnInterval;

        public int SpawnRange;

        public int SpawnLimit;

        public ActorDef Spawnable { get; private set; }

        public override bool IsDefined => Spawnable != null;

        [InjectService] private DefinitionService _definitionService;

        public override void Create(string dataPath)
        {
            if (SpawnableName != null)
            {
                Spawnable = _definitionService.GetDefinition<ActorDef>(SpawnableName);
                if (Spawnable != null)
                {
                    base.Create(dataPath);

                    InjectServices();
                }
            }
        }

        internal override void OnInspected(EntityPreview preview)
        {
            base.OnInspected(preview);

            preview.AddProperty("Spawns", SpawnableName);
        }
    }
}