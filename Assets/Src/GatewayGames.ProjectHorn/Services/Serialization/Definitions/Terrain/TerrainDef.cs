﻿namespace Gateway.ProjectHorn.Services.Serialization.Definitions
{
    [System.Serializable]
    public class TerrainDef : BaseDef
    {
        public static string DefinitionPath => "terrain/";

        public Vector2IntDef WorldSize;
    }
}