﻿using System.Collections.Generic;
using Gateway.ProjectHorn.Components.UI.Debug;
using Gateway.ProjectHorn.Services.Serialization.Definitions.Graphics;
using Gateway.ProjectHorn.Services.Serialization.Definitions.Item;
using Gateway.ProjectHorn.Services.Serialization.Definitions.Prop;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Gateway.ProjectHorn.Services.Serialization.Definitions
{
    [System.Serializable]
    public class TileDef : BaseDef
    {
        public static string DefinitionPath => "tiles/";

        public GraphicsSpriteDef Graphics;

        public TileOccupation Occupation;

        // public ItemYieldDef[] Yields;
        public string[] PossiblePropNodesNames;

        public PropNodeDef[] PossiblePropNodes { get; private set; }

        public Tile Tile { get; private set; }

        public override void Create(string dataPath)
        {
            base.Create(dataPath);

            Graphics.Create(dataPath);
            Tile = ScriptableObject.CreateInstance<Tile>();
            Tile.sprite = Graphics.Sprite;
            Tile.name = Name;

            if (PossiblePropNodesNames != null)
            {
                PossiblePropNodes = new PropNodeDef[PossiblePropNodesNames.Length];
                var definitionService = ServiceInjector.GetService<DefinitionService>();
                for (int i = 0; i < PossiblePropNodes.Length; i++)
                {
                    PossiblePropNodes[i] = definitionService.GetDefinition<PropNodeDef>(PossiblePropNodesNames[i]);
                }
            }
        }

        internal override void OnInspected(EntityPreview preview)
        {
            base.OnInspected(preview);

            preview.AddProperty("Occupation", Occupation);
            preview.AddProperty("Sprite", Graphics.Sprite.name);

            Graphics.OnInspected(preview);
        }

        internal override Sprite GetSprite()
        {
            return Graphics.Sprite;
        }

        public ItemYieldDef[] GetAllYields()
        {
            var yields = new List<ItemYieldDef>();

            if (PossiblePropNodes != null)
            {
                for (int i = 0; i < PossiblePropNodes.Length; i++)
                {
                    yields.Add(PossiblePropNodes[i].Prop.ItemYield);
                }
            }

            return yields.ToArray();
        }
    }
}