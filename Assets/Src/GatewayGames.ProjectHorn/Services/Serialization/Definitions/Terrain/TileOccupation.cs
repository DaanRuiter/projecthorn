﻿using System;

/**
 * 07/11/2020
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Services.Serialization.Definitions
{
    public enum TileOccupationMode
    {
        None,
        Partial,
        Full
    }

    [Serializable]
    public class TileOccupation : BaseDef
    {
        public Vector2IntDef Size;
        public string Mode;

        public bool IsOccupied => (Mode == "Full" && Size.x > 0 && Size.y > 0);

        public override string ToString() => string.Format("[{0}], {1}", Mode, Size);
    }
}