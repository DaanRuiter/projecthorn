using System;

/**
 * 20/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Services.Serialization.Definitions
{
    [Serializable]
    public class TerrainNoiseDef : BaseDef
    {
        public float Seed;
        public float Scale;
        public float Strength;
    }
}