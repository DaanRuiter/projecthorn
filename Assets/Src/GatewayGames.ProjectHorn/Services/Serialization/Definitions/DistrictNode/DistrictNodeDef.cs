

/**
 * 19/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Services.Serialization.Definitions.DistrictNode
{
    public enum DistrictNodeType
    {
        Generic,
        Building,
        Prop
    }

    public class DistrictNodeDef : EntityDef
    {
        public override string DefinitionPath => "district_nodes/";

        public virtual DistrictNodeType NodeType => DistrictNodeType.Generic;
    }
}