﻿using System;
using Gateway.ProjectHorn.Services.Serialization.Definitions;
using UnityEngine;

/**
 * 07/11/2020
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Definitions
{
    [Serializable]
    public class EnumDef : BaseDef
    {
        public string Value;

        [NonSerialized] private object m_parsedValue;

        public bool IsParsed { get; private set; }

        public EnumDef(Enum value)
        {
            SetValue(value);
        }

        public EnumDef()
        {
        }

        public void SetValue<E>(E value) where E : Enum
        {
            m_parsedValue = value;
        }

        public override string ToString()
        {
            if (IsParsed)
            {
                return m_parsedValue.ToString();
            }

            return string.Empty;
        }

        public E AsEnum<E>() where E : Enum
        {
            if (!IsParsed)
            {
                if (string.IsNullOrEmpty(Value))
                {
                    return default;
                }

                m_parsedValue = (E)Enum.Parse(typeof(E), Value);
                IsParsed = true;
            }

            return (E)m_parsedValue;
        }

        public bool Equals(Enum other)
        {
            try
            {
                var self = Enum.Parse(other.GetType(), Value);
                return self.ToString() == other.ToString();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                return false;
            }
        }
    }
}