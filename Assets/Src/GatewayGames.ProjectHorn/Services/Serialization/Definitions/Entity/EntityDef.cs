﻿using System;
using Gateway.ProjectHorn.Components.UI.Debug;
using Gateway.ProjectHorn.Services.Serialization.Definitions.Graphics;
using Gateway.ProjectHorn.Systems.DOTS.Common;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Gateway.ProjectHorn.Services.Serialization.Definitions
{
    [Serializable]
    public abstract class EntityDef : BaseDef
    {
        public abstract string DefinitionPath { get; }

        public GraphicsTextureDef Graphics;

        public float zPos => Graphics.Layer / 100f;

        public override void Create(string dataPath)
        {
            base.Create(dataPath);

            Graphics.Create(dataPath);
        }

        internal override void OnInspected(EntityPreview preview)
        {
            base.OnInspected(preview);

            if (Graphics.Texture)
            {
                preview.AddProperty("Texture", Graphics.Texture.name);
                Graphics.OnInspected(preview);
            }
        }

        internal override void OnEntityCreated(EntityManager entityManager,
            Entity entity,
            float2 position,
            Vector2Int tilePosition)
        {
            Graphics.OnEntityCreated(entityManager, entity, position, tilePosition);

            entityManager.AddSharedComponentData(entity,
                new FactionIndex()
                {
                    FactionUID = FactionIndex.PlayerFactionUID
                });
            entityManager.AddComponentData(entity,
                new Translation()
                {
                    Value = new float3(position.x, position.y, 0)
                });
        }
    }
}