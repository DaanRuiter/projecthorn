﻿/**
 * 08/11/2020
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

using Gateway.ProjectHorn.Util;

namespace Gateway.ProjectHorn.Services
{
    public abstract class ServiceBase : SharedObject
    {
        private string m_name = "uninitialized service";
        private bool m_started;

        public ServiceBase(bool injectServices) : base(injectServices)
        {
        }

        public ServiceBase() : base(false)
        {
        }

        internal abstract void OnStart();

        internal abstract void OnUpdate();

        internal abstract void OnStop();

        internal ServiceBase Initialize()
        {
            m_name = GetType().Name;
            return this;
        }

        protected override string OnFormatLogMessage(string severityLevelIcon, object message) =>
            string.Format("<{0}> [{1}] {2}", severityLevelIcon, m_name, message);
    }
}