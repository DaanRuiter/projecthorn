

/**
 * 09/07/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Util.HelperTypes
{
    public struct DataTuple<T1, T2> where T1 : struct where T2 : struct
    {
        public T1 Item1;
        public T2 Item2;
    }
}