﻿using Gateway.ProjectHorn.Services;
using UnityEngine;

namespace Gateway.ProjectHorn.Util
{
    public abstract class SharedObject
    {
        private const string logIcon = "?";
        private const string warningIcon = "!";
        private const string errorIcon = "X";

        protected SharedObject(bool injectServices = true)
        {
            if (injectServices)
            {
                ServiceInjector.InjectServices(this);
            }
        }

        protected void Log(object message) => Debug.Log(FormatLogMessage(logIcon, message));

        protected void LogF(string template, params object[] values) => Log(string.Format(template, values));

        protected void Warning(object message) => Debug.LogWarning(FormatLogMessage(warningIcon, message));

        protected void Error(object message) => Debug.LogError(FormatLogMessage(errorIcon, message));

        protected void ErrorF(string template, params object[] values) => Error(string.Format(template, values));

        protected string FormatLogMessage(string icon, object message) =>
            OnFormatLogMessage(icon, message);

        protected virtual string OnFormatLogMessage(string severityLevelIcon, object message) =>
            string.Format("[{0}] {1}", severityLevelIcon, message);
    }
}