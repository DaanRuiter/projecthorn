﻿using UnityEngine;

namespace Gateway.ProjectHorn.Util
{
    public static class Vector3Extensions
    {
        public static Vector3Int ToVector3Int(this Vector3 vector)
        {
            return new Vector3Int((int)vector.x, (int)vector.y, (int)vector.z);
        }

        public static Vector2Int ToVector2Int(this Vector3 vector)
        {
            return new Vector2Int((int)vector.x, (int)vector.y);
        }
    }
}