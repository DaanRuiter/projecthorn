using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

/**
 * 20/05/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Util
{
    public static class EntityUtil
    {
        public static float2 GetPosition(this Entity entity)
        {
            return World.DefaultGameObjectInjectionWorld.EntityManager.GetComponentData<Translation>(entity).Value
                .ToFloat2();
        }

        public static float3 GetPosition3D(this Entity entity)
        {
            return World.DefaultGameObjectInjectionWorld.EntityManager.GetComponentData<Translation>(entity).Value;
        }
    }
}