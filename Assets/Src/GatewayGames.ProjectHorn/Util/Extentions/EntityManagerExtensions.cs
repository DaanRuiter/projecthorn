

using Unity.Entities;
using Unity.Rendering;
using UnityEngine;

/**
 * 03/07/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Util
{
    public static class EntityManagerExtensions
    {
        public static void SetRenderMeshMaterial(this EntityManager entityManager, Entity entity, Material material)
        {
            var renderMesh = entityManager.GetSharedComponentData<RenderMesh>(entity);
            renderMesh.material = material;
            entityManager.SetSharedComponentData(entity, renderMesh);
        }
    }
}