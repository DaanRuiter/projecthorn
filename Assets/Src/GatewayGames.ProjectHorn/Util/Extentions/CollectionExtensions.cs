

using System;
using System.Collections.Generic;
using Sirenix.Utilities;
using Unity.Collections;

/**
 * 02/06/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Util
{
    public static class CollectionExtensions
    {
        public static V GetSafe<K, V>(this Dictionary<K, V> dictionary, K key)
        {
            return dictionary.ContainsKey(key) ? dictionary[key] : default;
        }

        public static NativeHashMap<K, V> ToNativeHashMap<K, V>(this Dictionary<K, V> dictionary, Allocator allocator)
            where V : struct where K : struct, IEquatable<K>
        {
            var hashMap = new NativeHashMap<K, V>(dictionary.Count, allocator);
            dictionary.ForEach(item => { hashMap.Add(item.Key, item.Value); });
            return hashMap;
        }
    }
}