﻿using Gateway.ProjectHorn.Systems.Building;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

/**
 * 31/01/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Util
{
    public static class Float2Extensions
    {
        public static Vector3 ToVector3(this float2 float2)
        {
            return new Vector3(float2.x, float2.y);
        }

        public static Vector2Int ToVector2Int(this float2 float2)
        {
            return new Vector2Int((int)float2.x, (int)float2.y);
        }

        public static int2 ToInt2(this float2 float2)
        {
            return new int2((int)float2.x, (int)float2.y);
        }

        public static int2 ToTilePos(this float2 float2)
        {
            return math.ceil(float2).ToInt2();
        }

        public static int2 ToNodePos(this float2 float2)
        {
            var floored = new int2((int)Mathf.Floor(float2.x), (int)Mathf.Floor(float2.y));
            var local = new float2(float2.x - floored.x, float2.y - floored.y);
            return new int2((int)(local.x * District.NodeGridSize), (int)(local.y * District.NodeGridSize));
        }

        public static float2 ToNodeCenterPos(this float2 float2)
        {
            float nodeSize = District.NodeLocalSize;
            var nodePos = ToNodePos(float2).ToFloat2();
            var floored = math.floor(float2);
            return floored + (nodeSize * (nodePos + 1));
        }

        public static float3 ToFloat3(this float2 float2)
        {
            return new float3(float2.x, float2.y, 0);
        }

        public static float2 Add(this float2 float2, float distance)
        {
            var newFloat2 = float2;
            newFloat2.x += distance;
            newFloat2.y += distance;
            return newFloat2;
        }

        public static float2 AddRandom(this float2 float2, float min, float max)
        {
            var newFloat2 = float2;
            newFloat2.x += Random.Range(min, max);
            newFloat2.y += Random.Range(min, max);
            return newFloat2;
        }
    }
}