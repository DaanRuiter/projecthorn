﻿using Unity.Mathematics;
using UnityEngine;
using URandom = UnityEngine.Random;
using Random = Unity.Mathematics.Random;

/**
 * 29/01/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Util
{
    public static class Int2Extensions
    {
        public static int2 AddRandom(this int2 int2, int min, int max)
        {
            int2.x += URandom.Range(min, max);
            int2.y += URandom.Range(min, max);

            return int2;
        }

        public static float2 AddFloat2(this int2 int2, float2 float2)
        {
            return new float2(int2.x + float2.x, int2.y + float2.y);
        }

        public static float2 AddRandom(this int2 int2, float min, float max, Random random)
        {
            float2 min2 = new float2(min, min);
            float2 max2 = new float2(max, max);

            //bug: Seems to favour lower half of min-max range
            return int2 + random.NextFloat2(min2, max2);
        }

        public static float2 RandomLocalTilePos(this int2 int2, Random random)
        {
            return int2.AddRandom(0f, 1f, random);
        }

        public static float2 ToFloat2(this int2 int2)
        {
            return new float2(int2.x, int2.y);
        }

        public static float3 ToFloat3(this int2 int2)
        {
            return new float3(int2.x, int2.y, 0);
        }

        public static Vector2 ToVector2(this int2 int2)
        {
            return new Vector2(int2.x, int2.y);
        }

        public static Vector2Int ToVector2Int(this int2 int2)
        {
            return new Vector2Int(int2.x, int2.y);
        }

        public static void Randomize(this int2[] array) => PrimitiveExtensions.Randomize(array);
    }
}