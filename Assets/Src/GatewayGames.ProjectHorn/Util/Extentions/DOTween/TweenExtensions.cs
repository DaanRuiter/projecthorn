

using System;
using DG.Tweening;

/**
 * 10/02/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Util.DOTween
{
    [Serializable]
    public class TweenSettings
    {
        public float Duration;
        public Ease Ease;
    }

    public static class TweenExtensions
    {
        // public static void SetSettings(this Tween tween)
        // {
        //     tween
        // }
    }
}