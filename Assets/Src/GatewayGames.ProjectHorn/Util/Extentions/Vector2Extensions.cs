﻿using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Gateway.ProjectHorn.Util
{
    public static class Vector2Extensions
    {
        public static Vector2 AddRandom(this Vector2 vector, float min, float max)
        {
            vector.x += Random.Range(min, max);
            vector.y += Random.Range(min, max);

            return vector;
        }

        public static float2 ToFloat2(this Vector2 vector2)
        {
            return new float2(vector2.x, vector2.y);
        }
    }
}