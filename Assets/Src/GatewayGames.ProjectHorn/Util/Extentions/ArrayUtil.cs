

using Gateway.ProjectHorn.Systems.DOTS.Item;
using Gateway.ProjectHorn.Systems.Entities.Components.Actor;
using Gateway.ProjectHorn.Systems.Entities.Components.Common;
using Unity.Collections;
using Unity.Entities;

/**
 * 02/05/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Util
{
    public static class ArrayUtil
    {
        public static float[] Scale(this float[] array, float multiplier)
        {
            var result = array;

            for (int i = 0; i < result.Length; i++)
            {
                result[i] *= multiplier;
            }

            return result;
        }

        //CarryData
        public static bool CarriesItem(this NativeMultiHashMap<Entity, ItemStackCarryData> carryData,
            Entity actor,
            uint itemUID)
        {
            var actorCarryData = carryData.GetValuesForKey(actor);

            while (actorCarryData.MoveNext())
            {
                var data = actorCarryData.Current;

                if (data.ItemUID == itemUID)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool CarriesFullStack(this NativeMultiHashMap<Entity, ItemStackCarryData> carryData,
            Entity actor,
            uint itemUID)
        {
            var actorCarryData = carryData.GetValuesForKey(actor);

            while (actorCarryData.MoveNext())
            {
                var data = actorCarryData.Current;

                if (data.ItemUID == itemUID && data.ItemCount.CapReached)
                {
                    return true;
                }
            }

            return false;
        }

        public static int GetStackSpaceLeft(this NativeMultiHashMap<Entity, ItemStackCarryData> carryData,
            Entity actor,
            uint itemUID)
        {
            var actorCarryData = carryData.GetValuesForKey(actor);

            while (actorCarryData.MoveNext())
            {
                var data = actorCarryData.Current;

                if (data.ItemUID == itemUID)
                {
                    return (int)data.ItemCount.Space;
                }
            }

            return -1;
        }

        public static ItemStackCarryData GetCarryData(this NativeMultiHashMap<Entity, ItemStackCarryData> carryData,
            Entity actor,
            uint itemUID)
        {
            var actorCarryData = carryData.GetValuesForKey(actor);

            while (actorCarryData.MoveNext())
            {
                var data = actorCarryData.Current;

                if (data.ItemUID == itemUID)
                {
                    return data;
                }
            }

            return new ItemStackCarryData()
            {
                ItemUID = itemUID,
                ItemCount = new URange(ItemStackData.MaxCarryItemCount, 0)
            };
        }
    }
}