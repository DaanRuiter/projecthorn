﻿using Unity.Mathematics;
using UnityEngine;

namespace Gateway.ProjectHorn.Util
{
    public static class Vector2IntExtensions
    {
        public static int2 ToInt2(this Vector2Int vector)
        {
            return new int2(vector.x, vector.y);
        }

        public static float2 ToFloat2(this Vector2Int vector)
        {
            return new float2(vector.x, vector.y);
        }

        public static Vector3 ToVector3(this Vector2Int vector)
        {
            return new Vector3(vector.x, vector.y);
        }

        public static Vector3Int ToVector3Int(this Vector2Int vector)
        {
            return new Vector3Int(vector.x, vector.y, 0);
        }
    }
}