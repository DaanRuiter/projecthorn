﻿using Unity.Mathematics;
using UnityEngine;

/**
 * 29/01/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Util
{
    public static class Float3Extensions
    {
        public static Vector3 ToVector3(this float3 float3)
        {
            return new Vector3(float3.x, float3.y, float3.z);
        }

        public static int2 ToInt2(this float3 float3)
        {
            return new int2((int)float3.x, (int)float3.y);
        }

        public static float2 ToFloat2(this float3 float3)
        {
            return new float2(float3.x, float3.y);
        }

        public static float Distance(this float3 float3, float3 float3Other)
        {
            return math.distance(float3, float3Other);
        }

        public static float Distance(this float3 float3, float2 float3Other)
        {
            return math.distance(float3, new float3(float3Other.x, float3Other.y, 0));
        }

        public static float3 Plus(this float3 float3, float distance)
        {
            var newFloat3 = float3;
            newFloat3.x += distance;
            newFloat3.y += distance;
            newFloat3.z += distance;
            return newFloat3;
        }
    }
}