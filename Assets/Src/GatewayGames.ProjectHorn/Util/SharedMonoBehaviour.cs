﻿using Gateway.ProjectHorn.Services;
using UnityEngine;

/**
 * 08/11/2020
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */
namespace Gateway.ProjectHorn.Util
{
    public class SharedMonoBehaviour : MonoBehaviour
    {
        protected virtual void Awake()
        {
            ServiceInjector.InjectServices(this);
        }
    }
}