



/**
 * 20/05/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Util
{
    public static class UID
    {
        private static ulong _counter;

        public static ulong GetUniqueID()
        {
            return _counter++;
        }
    }
}