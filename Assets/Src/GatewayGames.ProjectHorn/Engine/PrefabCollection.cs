

using System.Collections.Generic;
using Sirenix.Utilities;
using UnityEngine;

/**
 * 02/06/2021
 *
 * @author Daan Ruiter
 * A refactor a day keeps the debugger away
 */

namespace Gateway.ProjectHorn.Engine
{
    [CreateAssetMenu(menuName = "Gateway/Collection/Prefab", fileName = "PrefabCollection", order = 0)]
    public class PrefabCollection : ScriptableObject
    {
        public static PrefabCollection StandardGameAssets => m_StandardGameAssets;

        private static PrefabCollection m_StandardGameAssets;

        [SerializeField] private bool s_isStandardGameAssets;
        [SerializeField] private GameObject[] s_prefabs;

        private Dictionary<string, GameObject> m_prefabs;

        public void Load()
        {
            m_prefabs = new Dictionary<string, GameObject>();
            s_prefabs.ForEach(prefab => { m_prefabs.Add(prefab.name, prefab); });

            if (s_isStandardGameAssets)
            {
                m_StandardGameAssets = this;
            }
        }

        public GameObject GetPrefab(string prefabName)
        {
            return m_prefabs.ContainsKey(prefabName) ? m_prefabs[prefabName] : null;
        }
    }
}